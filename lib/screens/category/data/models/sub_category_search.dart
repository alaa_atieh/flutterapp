import 'package:json_annotation/json_annotation.dart';

part 'sub_category_search.g.dart';

@JsonSerializable()
class SubCategorySearch {
  String name;
  int id;

  SubCategorySearch({
    this.name,
    this.id,
  });

  factory SubCategorySearch.fromJson(Map<String, dynamic> json) =>
      _$SubCategorySearchFromJson(json);
  Map<String, dynamic> toJson() => _$SubCategorySearchToJson(this);

  @override
  String toString() => 'SubCategorySearch(name: $name, id: $id)';
}
