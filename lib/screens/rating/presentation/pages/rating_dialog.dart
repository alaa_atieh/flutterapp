import 'package:flutter/material.dart';

import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/product/domin/entities/temp_product.dart';
import 'package:australia_garden/screens/rating/presentation/widgets/rating_bar_form_field.dart';

import '../../data/datasources/rating_requests.dart';

class RatingDialog extends StatefulWidget {
  final Product product;
  final GlobalKey<ScaffoldState> scaffoldKey;

  RatingDialog({
    @required this.product,
    @required this.scaffoldKey,
  });

  @override
  _RatingDialogState createState() => _RatingDialogState();
}

class _RatingDialogState extends State<RatingDialog> {
  SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  TextEditingController _reviewTextController;
  TextEditingController _nameTextController;
  int _rating = 0;
  RatingRequests _ratingRequest = RatingRequests();

  Future<String> _postReview() async {
    String _name = _nameTextController.text;

    String response = await _ratingRequest.postReview(
      widget.product.productId,
      _name,
      _reviewTextController.text,
      _rating.toString(),
    );
    return response;
  }

  final _formKey = GlobalKey<FormState>();

  bool checkReviewText(String text) {
    return text.length >= 25;
  }

  bool checkReviewRating(int rating) {
    return rating > 0;
  }

  @override
  void initState() {
    super.initState();
    _reviewTextController = TextEditingController();
    _nameTextController = _sharedPreferencesManager.getBool('is_logged_in')
        ? TextEditingController(
            text: _sharedPreferencesManager.getString('firstname') +
                ' ' +
                _sharedPreferencesManager.getString('lastname'))
        : TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    bool isRateGiven;
    Widget payNow = Container(
      width: SizeConfig.screenWidth * 0.30,
      child: DefaultButton(
        textSize: 14.0,
        text: "Submit now",
        press: () async {
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();
            String _response = await _postReview();
            widget.scaffoldKey.currentState.showSnackBar(
              SnackBar(
                content: Container(
                  height: 30,
                  child: Text(
                    _response,
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ),
            );
            Navigator.of(context).pop();
          }
        },
      ),
    );

    Widget _nameTextFormField = TextFormField(
      controller: _nameTextController,
      decoration: InputDecoration(
        prefixIcon: Icon(
          Icons.person,
          color: kPrimaryColor,
          size: SizeConfig.screenWidth * 0.05,
        ),
        alignLabelWithHint: true,
        errorStyle: TextStyle(
          fontSize: 12,
          color: Colors.red,
        ),
        contentPadding: EdgeInsets.all(SizeConfig.screenWidth * 0.02),
        hintText: 'Your Name ',
      ),
      style: TextStyle(fontSize: 12, color: Colors.grey[600]),
      validator: (value) {
        if (value.length < 3 || value.length > 26) {
          return 'Name must be between 3 and 26 characters';
        }
        return null;
      },
    );

    Widget _reviewTextFormField = TextFormField(
      controller: _reviewTextController,
      decoration: InputDecoration(
        prefixIcon: Icon(
          Icons.comment,
          color: kPrimaryColor,
          size: SizeConfig.screenWidth * 0.05,
        ),
        //alignLabelWithHint: true,
        errorStyle: TextStyle(
          fontSize: 12,
          color: Colors.red,
        ),
        contentPadding: EdgeInsets.all(SizeConfig.screenWidth * 0.02),
        hintText: 'Say something about the product.',
      ),
      style: TextStyle(fontSize: 12, color: Colors.grey[600]),
      validator: (value) {
        if (value.length < 26 || isRateGiven == false) {
          String s =
              isRateGiven == false ? '* Please give us your rating ' : '';

          s += value.length < 26
              ? '\n* The review must be more than 26 character'
              : '';
          return s;
        }
        return null;
      },
    );

    Widget _ratingBarFormField = RatingBarFormField(
      validator: (value) {
        if (value == 0) {
          setState(() {
            isRateGiven = false;
          });
          return null;
        }
        setState(() {
          isRateGiven = true;
        });
        return null;
      },
      onSaved: (newValue) {
        setState(() {
          _rating = newValue;
        });
      },
    );

    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: Colors.grey[50]),
        padding: EdgeInsets.all(24.0),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          FittedBox(
            child: Text(
              'Give Us Your Opinion!',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          FittedBox(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.grey,
                  ),
                  children: [
                    TextSpan(
                      text: 'You are rating ',
                    ),
                    /* The Product Name is Set Here */
                    TextSpan(
                      text: widget.product.productName,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[600],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 16.0),
            //padding: EdgeInsets.all(16.0),
            decoration: BoxDecoration(
              color: Colors.grey[50],
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
            ),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  FittedBox(child: _ratingBarFormField),
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.04,
                  ),
                  _nameTextFormField,
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.02,
                  ),
                  _reviewTextFormField
                ],
              ),
            ),
          ),
          payNow
        ]),
      ),
    );
  }
}
