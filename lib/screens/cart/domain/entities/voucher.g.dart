// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voucher.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Voucher _$VoucherFromJson(Map<String, dynamic> json) {
  return Voucher(
    code: json['code'] as int,
    description: json['description'] as String,
    fromName: json['from_name'] as String,
    fromEmail: json['from_email'] as String,
    toName: json['to_name'] as String,
    toEmail: json['to_email'] as String,
    voucherThemeId: json['voucher_theme_id'] as String,
    message: json['message'] as String,
    price: json['price'] as String,
    amount: (json['amount'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$VoucherToJson(Voucher instance) => <String, dynamic>{
      'code': instance.code,
      'description': instance.description,
      'from_name': instance.fromName,
      'from_email': instance.fromEmail,
      'to_name': instance.toName,
      'to_email': instance.toEmail,
      'voucher_theme_id': instance.voucherThemeId,
      'message': instance.message,
      'price': instance.price,
      'amount': instance.amount,
    };
