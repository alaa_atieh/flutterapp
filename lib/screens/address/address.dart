class Address {
  String id;
  String firstName;
  String lastName;
  String company;
  String address1;
  String address2;
  String city;
  String postCode;
  String zone;
  String zoneCode;
  String country;
  String countryId;
  String zoneId;
  bool isDefault;

  Address({
    this.id,
    this.firstName,
    this.lastName,
    this.company,
    this.address1,
    this.address2,
    this.city,
    this.postCode,
    this.zone,
    this.zoneCode,
    this.country,
    this.countryId,
    this.zoneId,
    this.isDefault,
  });

  Address.fromJson(Map<String, dynamic> json) {
    id = json['address_id'];
    firstName = json['firstname'];
    lastName = json['lastname'];
    company = json['symbol_right'];
    company = json['symbol_right'];
    address1 = json['address_1'];
    address2 = json['address_2'];
    city = json['city'];
    postCode = json['postcode'];
    zone = json['zone'];
    zoneCode = json['zone_code'];
    country = json['country'];
    countryId = json['country_id'];
    zoneId = json['zone_id'];
    isDefault = json['default'];
  }
}
