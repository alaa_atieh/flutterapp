import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/screens/auth/confirm_otp_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:australia_garden/core/api_service.dart';
import 'package:australia_garden/core/components/custom_surfix_icon.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/auth/sign_up/components/sign_up_form.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';

class ChangeNumberPage extends StatefulWidget {
  static String routeName = '/change_number_page';
  @override
  _ChangeNumberPageState createState() => _ChangeNumberPageState();
}

class _ChangeNumberPageState extends State<ChangeNumberPage> {
  final _formKey = GlobalKey<FormState>();
  ApiService api = new ApiService();
  String pre = '+61';

  GlobalKey prefixKey = GlobalKey();
  double prefixWidth = 0;
  TextEditingController telephone = TextEditingController();

  String telephoneValidator = '';

  bool remember = false;
  bool agree = true;
  bool _isLoading;

  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  void initState() {
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/alaa-09.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: SafeArea(
            child: SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: SizeConfig.screenHeight * 0.04),
                      FittedBox(
                        child: Text("Change Phone NUmber", style: headingStyle),
                      ),
                      SizedBox(height: SizeConfig.screenHeight * 0.02),
                      Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: <Widget>[
                                Flexible(
                                  child: TextFormField(
                                    controller: telephone,
                                    style: TextStyle(fontSize: 16.0),
                                    textInputAction: TextInputAction.done,
                                    keyboardType: TextInputType.phone,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    decoration: InputDecoration(
                                      hintText: ' Telephone',
                                      prefixIcon: prefix(prefixKey, (value) {
                                        setState(() {
                                          pre = value;
                                        });
                                      }),
                                      // errorMaxLines: 2,

                                      suffixIcon: CustomSurffixIcon(
                                          svgIcon: "assets/icons/Phone.svg"),
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter a phone number';
                                      }
                                      if (int.tryParse(value) == null) {
                                        return 'Input needs to be digits only';
                                      }
                                      if (telephoneValidator != '') {
                                        return telephoneValidator;
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(15),
                      ),
                      !_isLoading
                          ? Padding(
                              padding: const EdgeInsets.only(bottom: 16.0),
                              child: DefaultButton(
                                text: "Submit",
                                press: () async {
                                  if (_formKey.currentState.validate()) {
                                    setState(() {
                                      _isLoading = true;
                                    });
                                    Response response = await api.changeNumber(
                                      pre + telephone.text,
                                    );
                                    setState(() {
                                      _isLoading = false;
                                    });
                                    if (response != null) {
                                      Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              ConfirmOtpPage(),
                                        ),
                                      );
                                    } else {
                                      Fluttertoast.showToast(
                                        msg: 'Something Went Wrong Try Again',
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        backgroundColor: Colors.black54,
                                        textColor: Colors.white,
                                      );
                                    }
                                  }
                                },
                              ),
                            )
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
    ;
  }
}
