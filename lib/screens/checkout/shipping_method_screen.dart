import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:im_stepper/stepper.dart';
import 'package:australia_garden/core/constants.dart';
import 'payment_method_screen.dart';
import 'shipping_method.dart';
import 'shipping_payment_api.dart';

class ShippingMethodScreen extends StatefulWidget {
  @override
  _ShippingMethodScreenState createState() => _ShippingMethodScreenState();
}

class _ShippingMethodScreenState extends State<ShippingMethodScreen> {
  final _formKey2 = GlobalKey<FormState>();
  ShippingMethodApi s = new ShippingMethodApi();
  List<ShippingMethod> shippingMethods = <ShippingMethod>[];
  ShippingMethod chosenMethod;
  TextEditingController commentController = TextEditingController(text: '');

  @override
  void initState() {
    super.initState();
    getShippingMethod(s);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Shipping method"),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/backgroundimage.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: SafeArea(
            child: SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: SingleChildScrollView(
                  child: Column(children: [
                    Text(
                      "Shipping Method",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getProportionateScreenWidth(28),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    IconStepper(
                      lineColor: Colors.black,
                      icons: [
                        SvgPicture.asset(
                          "assets/icons/iconfinder_address-billing_4192087.svg",
                          width: SizeConfig.screenWidth * 0.08,
                          height: SizeConfig.screenWidth * 0.08,
                          //FontAwesomeIcons.mapMarkerAlt,
                          // color: kPrimaryColor,
                          color: kPrimaryColor,
                          //  size: 8,
                        ),
                        SvgPicture.asset(
                          "assets/icons/iconfinder_address-shipping_4301321.svg",
                          width: SizeConfig.screenWidth * 0.08,
                          height: SizeConfig.screenWidth * 0.08,
                          //FontAwesomeIcons.shippingFast,
                          color: kPrimaryColor,
                          // size: 6,
                        ),
                        SvgPicture.asset(
                          "assets/icons/delivery.svg",
                          width: SizeConfig.screenWidth * 0.08,
                          height: SizeConfig.screenWidth * 0.08,
                          //  Icons.access_alarm
                        ),
                        SvgPicture.asset(
                          "assets/icons/paymentmethod.svg",
                          width: SizeConfig.screenWidth * 0.08,
                          height: SizeConfig.screenWidth * 0.08,
                          // Icons.supervised_user_circle,
                        ),
                      ],
                      enableNextPreviousButtons: false,
                      activeStep: 2,
                      activeStepColor: Colors.grey.shade100,
                      stepColor: Colors.grey.shade100,
                      activeStepBorderColor: Colors.yellow.shade700,
                      activeStepBorderWidth: 1.5,
                      enableStepTapping: false,
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    Column(
                      children: [
                        Form(
                          key: _formKey2,
                          child: Column(
                            children: [
                              SizedBox(height: SizeConfig.screenHeight * 0.03),
                              DropdownButtonFormField(
                                isExpanded: true,
                                validator: (value) {
                                  if (value == null) {
                                    return 'Please choose a shipping method';
                                  }
                                  return null;
                                },
                                onChanged: (value) {},
                                hint: Text('Choose shipping method'),
                                items: shippingMethods.map((val) {
                                  return new DropdownMenuItem(
                                    child: new Text(val.quote.title +
                                        "     " +
                                        val.quote.text),
                                    value: val.quote.code,
                                    onTap: () {
                                      chosenMethod = val;
                                    },
                                  );
                                }).toList(),
                              ),
                              SizedBox(height: SizeConfig.screenHeight * 0.06),
                              TextFormField(
                                maxLines: 3,
                                decoration: InputDecoration(
                                  hintText: "Comments",
                                ),
                                controller: commentController,
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: SizeConfig.screenHeight * 0.04),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: DefaultButton(
                                text: "Prev",
                                press: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            SizedBox(width: SizeConfig.screenWidth * 0.08),
                            Expanded(
                              child: DefaultButton(
                                text: "Next step ",
                                press: () async {
                                  if (_formKey2.currentState.validate()) {
                                    await s.saveShippingMethod(
                                        chosenMethod.quote.code,
                                        commentController.text);
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (_) => PaymentMethodScreen(
                                          hasProducts: true,
                                        ),
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    )
                  ]),
                ),
              ),
            ),
          ),
        ));
  }

  getShippingMethod(ShippingMethodApi s) async {
    List<ShippingMethod> sh;
    sh = await s.listShippingMethods();
    setState(
      () {
        shippingMethods = sh;
      },
    );
  }
}
