import 'dart:io';

import 'package:australia_garden/screens/checkout/order_success.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';

String paymentSuccessURL = Uri.decodeFull(
  Uri.https(
    authority,
    basicRoute,
    {
      'route': 'apiCustomize/success',
    },
  ).toString(),
);

class WebViewExample extends StatefulWidget {
  @override
  WebViewExampleState createState() => WebViewExampleState();
}

class WebViewExampleState extends State<WebViewExample> {
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    String _apiToken = _sharedPreferencesManager.getString('api_token');

    return Scaffold(
      appBar: AppBar(
        title: Text('Finishing Payment'),
      ),
      body: WebView(
        navigationDelegate: (NavigationRequest request) {
          if (request.url.contains(paymentSuccessURL)) {
            Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (_) => OrderSuccess(),
              ),
              (Route<dynamic> route) => false,
            );
          }
          return NavigationDecision.navigate;
        },
        initialUrl: Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/confirm',
              'api_token': _apiToken,
            },
          ).toString(),
        ),
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
