import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:australia_garden/core/api_service.dart';
import 'package:australia_garden/core/components/custom_surfix_icon.dart';
import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/components/form_error.dart';
import 'package:australia_garden/core/components/no_account_text.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/auth/sign_up/sign_up_screen.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.screenHeight * 0.04),
            Text(
              "Forgot Password",
              style: TextStyle(
                fontSize: getProportionateScreenWidth(28),
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            const Text(
              "Please enter your email and we will send \nyou a link to return to your account",
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.screenHeight * 0.1),
            ForgotPassForm(),
          ],
        ),
      ),
    );
  }
}

class ForgotPassForm extends StatefulWidget {
  @override
  _ForgotPassFormState createState() => _ForgotPassFormState();
}

class _ForgotPassFormState extends State<ForgotPassForm> {
  final _formKey = GlobalKey<FormState>();
  List<String> errors = [];
  ApiService _apiService;
  TextEditingController _textEditingController;
  bool _isLoading;

  @override
  void initState() {
    _apiService = ApiService();
    _textEditingController = TextEditingController();
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: AbsorbPointer(
        absorbing: _isLoading,
        child: Column(
          children: [
            TextFormField(
              controller: _textEditingController,
              keyboardType: TextInputType.emailAddress,
              onChanged: (value) {
                if (value.isNotEmpty && errors.contains(kEmailNullError)) {
                  setState(() {
                    errors.remove(kEmailNullError);
                  });
                } else if (emailValidatorRegExp.hasMatch(value) &&
                    errors.contains(kInvalidEmailError)) {
                  setState(() {
                    errors.remove(kInvalidEmailError);
                  });
                }
                return null;
              },
              validator: (value) {
                if (value.isEmpty && !errors.contains(kEmailNullError)) {
                  return kEmailNullError;
                } else if (!emailValidatorRegExp.hasMatch(value) &&
                    !errors.contains(kInvalidEmailError)) {
                  return kInvalidEmailError;
                }
                return null;
              },
              decoration: InputDecoration(
                labelText: "Email",
                hintText: "Enter your email",
                floatingLabelBehavior: FloatingLabelBehavior.always,
                suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
              ),
            ),
            SizedBox(height: getProportionateScreenHeight(30)),
            FormError(errors: errors),
            SizedBox(height: SizeConfig.screenHeight * 0.1),
            !_isLoading
                ? DefaultButton(
                    text: "Continue",
                    press: () async {
                      if (_formKey.currentState.validate()) {
                        setState(
                          () {
                            _isLoading = true;
                          },
                        );
                        try {
                          Map _response = await _apiService.forgetPassword(
                            _textEditingController.text.trim(),
                          );
                          String _success = _response['success'];
                          Map _error = _response['error'];

                          if (_success != null) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(_success),
                              ),
                            );
                            Navigator.of(context).pop();
                          } else {
                            setState(
                              () {
                                errors.clear();
                                errors.add(_error['warning']);
                                _isLoading = false;
                              },
                            );
                          }
                        } on Exception catch (e) {
                          Fluttertoast.showToast(
                            msg: e.toString(),
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Colors.black54,
                            textColor: Colors.white,
                          );
                        } finally {
                          setState(
                            () {
                              _isLoading = false;
                            },
                          );
                        }
                      }
                    },
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
            SizedBox(height: SizeConfig.screenHeight * 0.1),
            NoAccountText(
                text: "Don’t have an account? ",
                clickableText: "Sign Up",
                route: SignUpScreen.routeName),
          ],
        ),
      ),
    );
  }
}
