class PaymentMethod {
  String title;
  String code;
  String terms;
  String sortOrder;

  PaymentMethod({this.title, this.code, this.terms, this.sortOrder});
  PaymentMethod.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    code = json['code'];
    sortOrder = json['sort_order'];
    terms = json['terms'];
  }
}
