import 'quote.dart';

class ShippingMethod {
  String title;
  Quote quote;
  String sortOrder;
  bool error;

  ShippingMethod({this.title, this.quote, this.error, this.sortOrder});
  ShippingMethod.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    quote = Quote.fromJson(json['quote']);
    sortOrder = json['sort_order'];
    error = json['error'];
  }
}
