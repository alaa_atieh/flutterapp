import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:australia_garden/screens/address/add_address.dart';
import 'package:flutter/material.dart';

import 'address.dart';
import 'address_api.dart';
import 'address_card.dart';

class AddressList extends StatefulWidget {
  AddressApi a = new AddressApi();

  @override
  _AddressListState createState() => _AddressListState();
}

class _AddressListState extends State<AddressList> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,

      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => AddAddressScreen(
                _scaffoldKey,
                () {
                  setState(() {});
                },
              ),
            ),
          );
        },
        child: Icon(Icons.add_location),
        backgroundColor: kPrimaryColor,
      ),
      //Colors.red[300]),
      appBar: AppBar(),
      body: SafeArea(
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(5)),
          child: Column(
            children: [
              SizedBox(height: SizeConfig.screenHeight * 0.04), // 4%
              Text("Your addresses", style: headingStyle),
              SizedBox(height: SizeConfig.screenHeight * 0.02), // 4%
              FutureBuilder(
                future: widget.a.listAddresses(),
                builder: (BuildContext context,AsyncSnapshot<List<Address>> snapshot) {
                  if (snapshot.connectionState != ConnectionState.done) {
                    return Expanded(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  if (snapshot.hasError) {
                    return Expanded(
                      child: RetryButton(
                        action: () => setState(
                          () {},
                        ),
                      ),
                    );
                  }
                  if (snapshot.hasData) {
                    return Expanded(
                      child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.only(
                                top: SizeConfig.screenWidth * 0.03),
                            child: AddressCard(
                              address: snapshot.data[index],
                              deleteAddress: () {
                                setState(
                                  () {},
                                );
                              },
                              editAddress: () {
                                setState(
                                  () {},
                                );
                              },
                              scaffoldKey: _scaffoldKey,
                            ),
                          );
                        },
                      ),
                    );
                  }
                  return Expanded(
                    child: RetryButton(
                      action: () => setState(
                        () {},
                      ),
                    ),
                  );
                },
              ),
              Container(
                height: SizeConfig.screenWidth * 0.1,
                decoration: BoxDecoration(
                  border: Border(top: BorderSide.none),
                  color: Colors.transparent,
                ),
              )
            ],
          ),
        ),
      ),
      // bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }
}
