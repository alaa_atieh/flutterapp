import 'dart:core';

import 'package:australia_garden/screens/order/data/models/order_voucher_model.dart';

import 'order_product.dart';

class DetailedOrder {
  String id;
  String invoiceNumber;
  String dateAdded;
  String paymentAddress;
  String shippingAddress;
  String paymentMethod;
  String shippingMethod;
  List<OrderProduct> products;
  List<OrderVoucherModel> vouchers;
  DetailedOrder({
    this.id,
    this.invoiceNumber,
    this.dateAdded,
    this.paymentAddress,
    this.shippingAddress,
    this.paymentMethod,
    this.shippingMethod,
    this.products,
    this.vouchers,
  });

  DetailedOrder.fromJson(Map<String, dynamic> json) {
    id = json['order_id'];
    invoiceNumber = json['invoice_no'];
    paymentAddress = json['payment_address'];
    paymentMethod = json['payment_method'];
    shippingAddress = json['shipping_address'];
    shippingMethod = json['shipping_method'];
    dateAdded = json['date_added'];
    List pro = json['products'] as List;
    products = pro.map((e) => OrderProduct.fromJson(e)).toList();
    List vouchers = json['vouchers'] as List;
    this.vouchers =
        vouchers.map((voucher) => OrderVoucherModel.fromJson(voucher)).toList();
  }
}
