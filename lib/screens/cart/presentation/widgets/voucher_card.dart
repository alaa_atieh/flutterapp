import 'package:australia_garden/screens/cart/domain/entities/voucher.dart';
import 'package:flutter/material.dart';
import 'package:australia_garden/core/size_config.dart';

class VoucherCard extends StatefulWidget {
  final Voucher voucher;
  final Function onRemove;

  const VoucherCard({Key key, this.voucher, this.onRemove}) : super(key: key);

  @override
  _VoucherCardState createState() => _VoucherCardState();
}

class _VoucherCardState extends State<VoucherCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.screenHeight * 0.015,
        vertical: SizeConfig.screenWidth * 0.02,
      ),
      child: Container(
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                    text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'To :',
                      style: TextStyle(
                          color: Colors.black,

                          //AppColors.lightGray,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          fontFamily: 'Metropolis'),
                    ),
                    TextSpan(
                      text: widget.voucher.toEmail,
                      style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Metropolis')
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                  ],
                )),
                SizedBox(
                  height: 5,
                ),
                RichText(
                    text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Amount : ',
                      style: TextStyle(
                          color: Colors.black,

                          //AppColors.lightGray,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          fontFamily: 'Metropolis'),
                    ),
                    TextSpan(
                      text: widget.voucher.amount.toString(),
                      style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Metropolis')
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                  ],
                )),
                SizedBox(
                  height: 5,
                ),
                RichText(
                    text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Price : ',
                      style: TextStyle(
                          color: Colors.black,

                          //AppColors.lightGray,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          fontFamily: 'Metropolis'),
                    ),
                    TextSpan(
                      text: widget.voucher.price,
                      style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Metropolis')
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                  ],
                )),
                SizedBox(
                  height: 5,
                ),
                RichText(
                    text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Message : ',
                      style: TextStyle(
                          color: Colors.black,

                          //AppColors.lightGray,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          fontFamily: 'Metropolis'),
                    ),
                    TextSpan(
                      text: widget.voucher.message,
                      style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Metropolis')
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                  ],
                )),
                SizedBox(
                  height: 5,
                ),
                RichText(
                    text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Description : ',
                      style: TextStyle(
                          color: Colors.black,

                          //AppColors.lightGray,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          fontFamily: 'Metropolis'),
                    ),
                    TextSpan(
                      text: widget.voucher.description,
                      style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Metropolis')
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                  ],
                )),
                SizedBox(
                  height: 5,
                ),
              ],
            ),
          ),
          // isThreeLine: true,
        ),
      ),
    );
  }
}
