import 'package:flutter/material.dart';

import 'package:australia_garden/core/api_service.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/auth/sign_in/sign_in_screen.dart';
import 'package:australia_garden/screens/home/home_screen.dart';

//TODO change splash screen image
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
  static String routeName = "/splash";
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  Animation<double> opacity;
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();
  AnimationController controller;
  ApiService a = new ApiService();

  @override
  void initState() {
    super.initState();
    //
    if (_sharedPreferencesManager.getString('api_token') == null) {
      var result;
      a.logUser().then((value) => result = value['api_token']);
      _sharedPreferencesManager.putString('api_token', result);
    }
    controller = AnimationController(
        duration: Duration(milliseconds: 2500), vsync: this);
    opacity = Tween<double>(begin: 1.0, end: 0.0).animate(controller)
      ..addListener(() {
        setState(() {});
      });
    controller.forward().then((_) {
      navigationPage();
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void navigationPage() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (_) =>
            _sharedPreferencesManager.getBool('is_logged_in') != null &&
                    _sharedPreferencesManager.getBool('is_logged_in')
                ? HomeScreen()
                : SignInScreen(),
      ),
    );
  }

  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    SizeConfig().init(context);
    return Theme(
      data: theme.copyWith(scaffoldBackgroundColor: Colors.transparent),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/1.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          child: SafeArea(
            child: new Scaffold(
              body: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RichText(
                      text: TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(text: 'Powered by '),
                            TextSpan(
                                text: 'Noname group',
                                style: TextStyle(fontWeight: FontWeight.bold))
                          ]),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
