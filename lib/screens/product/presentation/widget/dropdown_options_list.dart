import 'package:australia_garden/screens/product/domin/entities/product_option_value.dart';
import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'package:flutter/material.dart';

class DropdownOptionList extends StatefulWidget {
  /* List of Options for the product  */
  final List<ProductOptionValue> productOptionValues;
  /* The name of the option */
  final String optionName;
  /* A function that will be executed whenever one of the check boxes is selected */
  final Callback onOptionSelected;
  /* The ID for the product option  */
  final String optionId;

  DropdownOptionList({
    @required this.productOptionValues,
    @required this.optionName,
    @required this.onOptionSelected,
    @required this.optionId,
  });

  @override
  _DropdownOptionListState createState() => _DropdownOptionListState();
}

class _DropdownOptionListState extends State<DropdownOptionList> {
  String _selectedName = '';

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: [
        DropdownButton<String>(
          hint: Text(_selectedName.isEmpty
              ? 'Select The ' + widget.optionName
              : _selectedName),
          isExpanded: true,
          items: widget.productOptionValues.map((value) {
            return new DropdownMenuItem<String>(
              value: value.productOptionValueId,
              child: new Row(
                children: [
                  Expanded(
                    child: Text(value.name),
                  ),
                  Text(value.price),
                ],
              ),
            );
          }).toList(),
          onChanged: (id) {
            setState(() {
              widget.onOptionSelected(widget.optionId, id);
              int _i = widget.productOptionValues
                  .indexWhere((element) => element.productOptionValueId == id);
              _selectedName = widget.productOptionValues[_i].name;
            });
          },
        ),
      ],
    );
  }
}
