import 'package:json_annotation/json_annotation.dart';

part 'added_to_cart_item.g.dart';

@JsonSerializable(
  explicitToJson: true,
)
class AddedToCartItem {
  @JsonKey(name: 'product_id')
  final String productId;

  @JsonKey(name: 'quantity')
  final String quantity;

  @JsonKey(name: 'option')
  final List<IdValue> option;

  AddedToCartItem({
    this.productId,
    this.quantity,
    this.option,
  });

  factory AddedToCartItem.fromJson(Map<String, dynamic> json) =>
      _$AddedToCartItemFromJson(json);

  Map<String, dynamic> toJson() => _$AddedToCartItemToJson(this);

  @override
  String toString() =>
      'CartItem(productId: $productId, quantity: $quantity, option: $option)';
}

@JsonSerializable(
  explicitToJson: true,
)
class IdValue {
  @JsonKey(name: 'key')
  String key;
  @JsonKey(name: 'value')
  List<String> value;

  IdValue({
    this.key,
    this.value,
  });

  factory IdValue.fromJson(Map<String, dynamic> json) =>
      _$IdValueFromJson(json);

  Map<String, dynamic> toJson() => _$IdValueToJson(this);

  @override
  String toString() => 'IdValue(key: $key, value: $value)';
}
