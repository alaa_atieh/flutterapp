// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_search.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategorySearch _$CategorySearchFromJson(Map<String, dynamic> json) {
  return CategorySearch(
    name: json['name'] as String,
    categoryId: json['category_id'] as int,
    children: (json['children'] as List)
        ?.map((e) => e == null
            ? null
            : SubCategorySearch.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CategorySearchToJson(CategorySearch instance) =>
    <String, dynamic>{
      'name': instance.name,
      'category_id': instance.categoryId,
      'children': instance.children,
    };
