import 'package:json_annotation/json_annotation.dart';

part 'voucher_gift_model.g.dart';

@JsonSerializable()
class VoucherGiftModel {
  @JsonKey(name: 'to_name')
  String toName;
  @JsonKey(name: 'to_email')
  String toEmail;
  @JsonKey(name: 'from_name')
  String fromName;
  @JsonKey(name: 'from_email')
  String fromEmail;
  @JsonKey(name: 'voucher_theme_id')
  String voucherThemeId;
  String message;
  String amount;

  VoucherGiftModel({
    this.toName,
    this.toEmail,
    this.fromName,
    this.fromEmail,
    this.voucherThemeId,
    this.message,
    this.amount,
  });
  factory VoucherGiftModel.fromJson(Map<String, dynamic> json) =>
      _$VoucherGiftModelFromJson(json);
  Map<String, dynamic> toJson() => _$VoucherGiftModelToJson(this);
}
