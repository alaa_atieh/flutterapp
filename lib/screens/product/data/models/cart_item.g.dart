// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartItem _$CartItemFromJson(Map<String, dynamic> json) {
  return CartItem(
    productId: json['product_id'] as String,
    quantity: json['quantity'] as String,
    option: (json['option'] as List)
        ?.map((e) =>
            e == null ? null : IdValue.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CartItemToJson(CartItem instance) => <String, dynamic>{
      'product_id': instance.productId,
      'quantity': instance.quantity,
      'option': instance.option?.map((e) => e?.toJson())?.toList(),
    };

IdValue _$IdValueFromJson(Map<String, dynamic> json) {
  return IdValue(
    key: json['key'] as String,
    value: (json['value'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$IdValueToJson(IdValue instance) => <String, dynamic>{
      'key': instance.key,
      'value': instance.value,
    };
