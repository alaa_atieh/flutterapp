import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/address/country.dart';
import 'package:australia_garden/screens/address/zone.dart';

import 'address.dart';

class AddressApi {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  Future<List<Address>> listAddresses() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    try {
      r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/address',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          List add = decoded['addresses'] as List;
          List<Address> addresses =
              add.map((e) => Address.fromJson(e)).toList();
          return addresses;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await listAddresses();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to List Addresses');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future deleteAddress(String addressId) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/address/delete/',
                  'address_id': addressId,
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        final data = json.decode(response.body);
        return data;
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await deleteAddress(addressId);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Delete Address');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<List<Country>> getCountries() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/address/add',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 500),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          List con = decoded['countries'] as List;

          List<Country> countries =
              con.map((e) => Country.fromJson(e)).toList();

          return countries;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getCountries();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Get Countries List');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
      return null;
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future addAddress(
      String firstName,
      String lastName,
      String city,
      String address1,
      String address2,
      String countryId,
      String zoneId,
      String postCode,
      String type,
      String id,
      bool isDefault) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    try {
      switch (type) {
        case "add":
          r = await http
              .post(
                Uri.decodeFull(
                  Uri.https(
                    authority,
                    basicRoute,
                    {
                      'route': 'apiCustomize/address/add',
                      'api_token': apiToken,
                    },
                  ).toString(),
                ),
                body: jsonEncode(
                  <String, dynamic>{
                    'firstname': firstName,
                    'lastname': lastName,
                    'city': city,
                    'address_1': address1,
                    'address_2': address2,
                    'country_id': countryId,
                    'zone_id': zoneId,
                    'postcode': postCode,
                    'default': isDefault ? 1 : 0
                  },
                ),
              )
              .timeout(
                Duration(seconds: 10),
              );

          break;
        case "paymentNew":
          r = await http
              .post(
                Uri.decodeFull(
                  Uri.https(
                    authority,
                    basicRoute,
                    {
                      'route': 'apiCustomize/payment/saveAddress',
                      'api_token': apiToken,
                    },
                  ).toString(),
                ),
                body: jsonEncode(<String, dynamic>{
                  'payment_address': "new",
                  'firstname': firstName,
                  'lastname': lastName,
                  'city': city,
                  'address_1': address1,
                  'address_2': address2,
                  'country_id': countryId,
                  'zone_id': zoneId,
                  'postcode': postCode,
                  'default': isDefault ? 1 : 0,
                }),
              )
              .timeout(
                Duration(seconds: 10),
              );
          break;
        case "paymentExist":
          r = await http
              .post(
                Uri.decodeFull(
                  Uri.https(
                    authority,
                    basicRoute,
                    {
                      'route': 'apiCustomize/payment/saveAddress',
                      'api_token': apiToken,
                    },
                  ).toString(),
                ),
                body: jsonEncode(
                  <String, dynamic>{
                    'payment_address': "existing",
                    'address_id': id
                  },
                ),
              )
              .timeout(
                Duration(seconds: 10),
              );
          break;

        case "shippingExist":
          r = await http
              .post(
                Uri.decodeFull(
                  Uri.https(
                    authority,
                    basicRoute,
                    {
                      'route': 'apiCustomize/shipping/saveAddress',
                      'api_token': apiToken,
                    },
                  ).toString(),
                ),
                body: jsonEncode(
                  <String, dynamic>{
                    'shipping_address': "existing",
                    'address_id': id
                  },
                ),
              )
              .timeout(
                Duration(seconds: 10),
              );
          break;

        case "shippingNew":
          r = await http
              .post(
                Uri.decodeFull(
                  Uri.https(
                    authority,
                    basicRoute,
                    {
                      'route': 'apiCustomize/shipping/saveAddress',
                      'api_token': apiToken,
                    },
                  ).toString(),
                ),
                body: jsonEncode(
                  <String, dynamic>{
                    'payment_address': "new",
                    'firstname': firstName,
                    'lastname': lastName,
                    'city': city,
                    'address_1': address1,
                    'address_2': address2,
                    'country_id': countryId,
                    'zone_id': zoneId,
                    'postcode': postCode,
                    "company": "company",
                    'default': isDefault ? 1 : 0,
                  },
                ),
              )
              .timeout(
                Duration(seconds: 10),
              );
          break;
      }

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        return decoded;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await addAddress(
          firstName,
          lastName,
          city,
          address1,
          address2,
          countryId,
          zoneId,
          postCode,
          type,
          id,
          isDefault,
        );
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Add Address');
      }

      //timeout(Duration(seconds: 10));
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<List<Zone>> getZonesByCountryId(zoneId) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/address/country',
                  'api_token': apiToken,
                  'country_id': zoneId,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          List zon = decoded['zones'] as List;

          List<Zone> zones = zon.map((e) => Zone.fromJson(e)).toList();

          return zones;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getZonesByCountryId(zoneId);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Get Cities');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future updateAddress(
      addressId,
      String firstName,
      String lastName,
      String city,
      String address1,
      String address2,
      String countryId,
      String zoneId,
      String postCode,
      bool isDefault) async {
    var r;
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      r = await http
          .post(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/address/edit',
                  'api_token': apiToken,
                  'address_id': addressId,
                },
              ).toString(),
            ),
            body: jsonEncode(
              <String, dynamic>{
                'firstname': firstName,
                'lastname': lastName,
                'city': city,
                'address_1': address1,
                'address_2': address2,
                'country_id': countryId,
                'zone_id': zoneId,
                'postcode': postCode,
                'default': isDefault ? 1 : 0,
              },
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        return decoded;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await updateAddress(addressId, firstName, lastName, city,
            address1, address2, countryId, zoneId, postCode, isDefault);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Update Address');
      }
    } //timeout(Duration(seconds: 10));
    on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(
        e.toString(),
      );
      return null;
    }
  }
}
