import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:flutter/material.dart';

import 'package:australia_garden/screens/category/apicalls/categories_api.dart';
import 'package:australia_garden/screens/category/category.dart';

import 'product_view.dart';
import 'size_changing_app_bar.dart';
import 'sort_rules.dart';

class ProductsListView extends StatefulWidget {
  //Category category;
  bool list;
  SortRules sortRules;
  String categoryName;
  String categoryId;

  ProductsListView({
    //this.category,
    this.list,
    this.sortRules,
    this.categoryName,
    this.categoryId,
  });

  @override
  _ProductsListViewState createState() => _ProductsListViewState();
}

class _ProductsListViewState extends State<ProductsListView> {
  CategoriesApi c = new CategoriesApi();
  Category category;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/backgroundimage.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: FutureBuilder(
          future: c.getCategory(
            widget.categoryId,
            widget.sortRules.typeToApi(),
            widget.sortRules.orderToApi,
            60,
          ),
          builder: (BuildContext context, AsyncSnapshot<Category> snapshot) {
            if (snapshot.connectionState != ConnectionState.done) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (snapshot.hasError) {
              return RetryButton(
                action: () => setState(
                  () {},
                ),
              );
            }
            if (snapshot.hasData) {
              return CustomScrollView(
                slivers: <Widget>[
                  SizeChangingAppBar(
                    categoryName: widget.categoryName,
                    sortRules: widget.sortRules,
                    categoryId: widget.categoryId,
                    isListView: true,
                    onViewChanged: () {
                      {
                        setState(() {
                          category = snapshot.data;
                        });
                        if (widget.list == false)
                          setState(() {
                            widget.list = true;
                          });
                        else
                          setState(() {
                            widget.list = false;
                          });
                      }
                    },
                    onSortChanged: (category, sortrules) {
                      setState(
                        () {
                          category = snapshot.data;
                          widget.sortRules = sortrules;
                        },
                      );
                    },
                  ),
                  widget.list
                      ? getListView(
                          context,
                          snapshot.data,
                        )
                      : getTileView(context, category),
                ],
              );
            }
            return RetryButton(
              action: () => setState(
                () {},
              ),
            );
          },
        ),
      ),
    );
  }
}
