// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_voucher_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderVoucherModel _$OrderVoucherModelFromJson(Map<String, dynamic> json) {
  return OrderVoucherModel(
    description: json['description'] as String,
    amount: json['amount'] as String,
  );
}

Map<String, dynamic> _$OrderVoucherModelToJson(OrderVoucherModel instance) =>
    <String, dynamic>{
      'description': instance.description,
      'amount': instance.amount,
    };
