import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:australia_garden/core/app_properties.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/address/address.dart';
import 'package:australia_garden/screens/address/address_api.dart';
import 'package:australia_garden/screens/address/edit_address_screen.dart';

class AddressCard extends StatefulWidget {
  Address address;
  Function deleteAddress;
  Function editAddress;
  GlobalKey<ScaffoldState> scaffoldKey;

  AddressApi a = new AddressApi();

  AddressCard(
      {this.address, this.deleteAddress, this.editAddress, this.scaffoldKey});

  @override
  _AddressCardState createState() => _AddressCardState();
}

class _AddressCardState extends State<AddressCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.screenWidth * 0.04,
          vertical: SizeConfig.screenWidth * 0.01),
      child: Container(
        decoration: BoxDecoration(
          boxShadow: shadow,
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 10,
              child: ListTile(
                contentPadding: EdgeInsets.only(left: 20),
                hoverColor: kSecondaryColor.withOpacity(0.1),
                // tileColorho: kSecondaryColor.withOpacity(0.1),
                title: Text(widget.address.address1),
                subtitle: Text(widget.address.address2),
                leading: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        FontAwesomeIcons.mapMarkerAlt,
                        color: Colors.black,
                      ),
                    ]),
                trailing: IconButton(
                  icon: Icon(
                    FontAwesomeIcons.pen,
                    color: kPrimaryColor, 
                    size: 17,
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              EditAddressScreen(widget.scaffoldKey, () {
                                widget.editAddress();
                              }, widget.address)),
                    );
                  },
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: IconButton(
                icon: Icon(
                  Icons.close, color: Colors.redAccent, //Colors.red[300],
                  size: 17,
                ),
                onPressed: () async {
                  var result = await widget.a.deleteAddress(widget.address.id);
                  if (result['success'] != null) {
                    widget.deleteAddress();
                  } else {
                    Fluttertoast.showToast(
                        msg: result['error']['warning'],
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        backgroundColor: Colors.black54,
                        textColor: Colors.white);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
