import 'package:australia_garden/screens/cart/data/models/added_to_cart_item.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Serialization', () {
    test(
      'Cart Item is serialized to json',
      () {
        final actual =
            AddedToCartItem(productId: '1', quantity: '1', option: []).toJson();

        final matcher = {
          {
            'product_id': '1',
            'quantity': '1',
            'option': [
              {
                'key': '1',
                'value': ['1']
              },
            ]
          }
        };

        expect(actual, matcher);
      },
    );

    test(
      'IdValue is serialized to json',
      () {
        final actual = IdValue(
          key: '1',
          value: ['1'],
        ).toJson();

        final matcher = {
          'key': '1',
          'value': ['1']
        };

        expect(actual, matcher);
      },
    );
  });
}
