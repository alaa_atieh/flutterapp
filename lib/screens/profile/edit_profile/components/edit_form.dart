import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/components/form_error.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/profile/profile_api.dart';
import 'package:flutter/material.dart';

class EditForm extends StatefulWidget {
  GlobalKey<ScaffoldState> _scaffoldKey;

  EditForm(this._scaffoldKey);

  @override
  _EditFormState createState() => _EditFormState();
}

class _EditFormState extends State<EditForm> {
  final _formKey = GlobalKey<FormState>();

  SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  ProfileApi api = new ProfileApi();

  String firstName;
  String lastName;
  String email;
  String telephone;
  TextEditingController firstNameTextEditingController,
      lastNameTextEditingController,
      emailTextEditingController,
      telephoneTextEditingController;

  GlobalKey prefixKey = GlobalKey();

  double prefixWidth = 0;

  String telephoneValidator = '';
  String emailValidator = '';
  String firstNameValidator = '';
  String lastNameValidator = '';

  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  void initState() {
    super.initState();
    String firstName = _sharedPreferencesManager.getString('firstname');
    String lastName = _sharedPreferencesManager.getString('lastname');
    String email = _sharedPreferencesManager.getString('email');
    String telephone = _sharedPreferencesManager.getString('telephone');

    firstNameTextEditingController = TextEditingController(text: firstName);
    lastNameTextEditingController = TextEditingController(text: lastName);
    emailTextEditingController = TextEditingController(text: email);
    telephoneTextEditingController = TextEditingController(text: telephone);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Input(
              controller: firstNameTextEditingController,
              hintText: ' First name',
              icon: Icons.person_outline,
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }
                if (firstNameValidator != '') {
                  return firstNameValidator;
                }
                return null;
              },
              secure: false),

          // buildEmailFormField(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          Input(
            controller: lastNameTextEditingController,
            hintText: 'Last name',
            icon: Icons.person_outline,
            keyboardType: TextInputType.text,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              if (lastNameValidator != '') {
                return lastNameValidator;
              }
              return null;
            },
            secure: false,
          ),

          //buildPasswordFormField(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          Row(children: <Widget>[
            Flexible(
              child: TextFormField(
                controller: telephoneTextEditingController,

                style: TextStyle(fontSize: 16.0),
                keyboardType: TextInputType.phone,
                // inputFormatters: <TextInputFormatter>[ FilteringTextInputFormatter.allow('123456789/+')],

                decoration: InputDecoration(
                  hintText: ' Telephone',
                  errorMaxLines: 2,
                  prefixIcon: Icon(
                    Icons.phone_android_outlined,
                    color: kPrimaryColor,
                  ),
                  border: InputBorder.none,
                ),
                validator: (value) {
                  if (int.tryParse(value) == null) {
                    return 'Input needs to be digits only';
                  }

                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  if (telephoneValidator != '') {
                    return telephoneValidator;
                  }
                  return null;
                },
              ),
            ),
          ]),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),
          Input(
            controller: emailTextEditingController,
            hintText: 'Email',
            icon: Icons.mail_outline_rounded,
            keyboardType: TextInputType.emailAddress,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              if (emailValidator != '') {
                return emailValidator;
              }
              return null;
            },
            secure: false,
          ),

          //buildConformPassFormField(),
          SizedBox(
            height: getProportionateScreenHeight(30),
          ),

          FormError(errors: errors),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          Row(children: <Widget>[
            Expanded(
              child: DefaultButton(
                  text: "Cancel",
                  press: () {
                    Navigator.of(context).pop();
                  }),
            ),
            SizedBox(width: 20),
            Expanded(
              child: DefaultButton(
                  text: "Save",
                  press: () async {
                    if (_formKey.currentState.validate()) {
                      dynamic decoded = await api.editProfile(
                        firstNameTextEditingController.text,
                        lastNameTextEditingController.text,
                        emailTextEditingController.text,
                        telephoneTextEditingController.text,
                      );
                      if (decoded['error'] != null) {
                        setState(() {
                          this.emailValidator = decoded['error']['email'];
                          this.firstNameValidator =
                              decoded['error']['firstname'];
                          this.telephoneValidator =
                              decoded['error']['telephone'];
                          this.lastNameValidator = decoded['error']['lastname'];
                        });
                      }
                      final formv = _formKey.currentState;
                      if (formv.validate()) {
                        _sharedPreferencesManager.putString(
                          'firstname',
                          firstNameTextEditingController.text,
                        );
                        _sharedPreferencesManager.putString(
                          'lastname',
                          lastNameTextEditingController.text,
                        );
                        _sharedPreferencesManager.putString(
                          'email',
                          emailTextEditingController.text,
                        );
                        _sharedPreferencesManager.putString(
                          'telephone',
                          telephoneTextEditingController.text,
                        );
                        widget._scaffoldKey.currentState.showSnackBar(
                          new SnackBar(
                            content: Container(
                                height: 30,
                                child: new Text(
                                  "Profile successfully updated",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300),
                                )),
                          ),
                        );
                        /*    Navigator.of(context).push(
                              MaterialPageRoute(builder: (_) => ProfileScreen()));*/
                      } else {
                        this.emailValidator = '';
                        this.firstNameValidator = '';
                        this.telephoneValidator = '';
                        this.lastNameValidator = '';
                      }

                      _formKey.currentState.save();
                      // if all are valid then go to success screen
                      //Navigator.of(context).push(MaterialPageRoute(builder:(_)=>ConfirmOtpPage()));

                    }
                  }),
            ),
          ]),
        ],
      ),
    );
  }
}

class Input extends StatefulWidget {
  Input(
      {Key key,
      @required this.controller,
      @required this.hintText,
      @required this.validator,
      this.secure,
      this.icon,
      this.keyboardType})
      : super(key: key);

  TextEditingController controller;
  final String hintText;
  Function validator;
  final bool secure;
  final IconData icon;
  TextInputType keyboardType;

  @override
  _InputState createState() => _InputState();
}

class _InputState extends State<Input> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: widget.hintText,
        errorMaxLines: 2,
        prefixIcon: Icon(
          widget.icon,
          color: kPrimaryColor,
        ),
      ),
      textInputAction: TextInputAction.next,
      controller: widget.controller,
      obscureText: widget.secure,
      style: TextStyle(fontSize: 16.0),
      validator: widget.validator,
      keyboardType: widget.keyboardType,
    );
  }
}
