// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refresh_token_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RefreshTokenResponseModel _$RefreshTokenResponseModelFromJson(
    Map<String, dynamic> json) {
  return RefreshTokenResponseModel(
    language: json['language'] as String,
    success: json['success'] as String,
    apiToken: json['api_token'] as String,
  );
}

Map<String, dynamic> _$RefreshTokenResponseModelToJson(
        RefreshTokenResponseModel instance) =>
    <String, dynamic>{
      'language': instance.language,
      'success': instance.success,
      'api_token': instance.apiToken,
    };
