// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voucher_themes_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VoucherThemesModel _$VoucherThemesModelFromJson(Map<String, dynamic> json) {
  return VoucherThemesModel(
    voucherThemes: (json['voucher_themes'] as List)
        ?.map((e) => e == null
            ? null
            : VoucherThemeModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$VoucherThemesModelToJson(VoucherThemesModel instance) =>
    <String, dynamic>{
      'voucher_themes': instance.voucherThemes,
    };

VoucherThemeModel _$VoucherThemeModelFromJson(Map<String, dynamic> json) {
  return VoucherThemeModel(
    name: json['name'] as String,
    voucherThemeId: json['voucher_theme_id'] as String,
  );
}

Map<String, dynamic> _$VoucherThemeModelToJson(VoucherThemeModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'voucher_theme_id': instance.voucherThemeId,
    };
