import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:australia_garden/core/constants.dart';

class DatePickerOption extends StatefulWidget {
  /* The name of the option */
  final String optionName;
  /* A function that will be executed whenever one of the check boxes is selected */
  final Callback onOptionSelected;
  /* The ID for the product option  */
  final String optionId;

  DatePickerOption({
    @required this.optionName,
    @required this.onOptionSelected,
    @required this.optionId,
  });

  @override
  _DatePickerOptionState createState() => _DatePickerOptionState();
}

class _DatePickerOptionState extends State<DatePickerOption> {
  DateTime _selectedDate;

  final DateFormat formatter = DateFormat('dd-MM-yyyy');

  void _startDatePicker() {
    !isIOS
        ? showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime.now(),
                lastDate: DateTime(2030),
                errorFormatText: 'Enter valid date',
                errorInvalidText: 'Enter date in valid range')
            .then((pickedDate) {
            if (pickedDate == null) return;
            setState(() {
              widget.onOptionSelected(
                widget.optionId,
                formatter.format(pickedDate),
              );
              _selectedDate = pickedDate;
            });
          })
        : buildCupertinoDatePicker(context);
  }

  buildCupertinoDatePicker(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (picked) {
                if (picked != null && picked != _selectedDate) {
                  widget.onOptionSelected(
                    widget.optionId,
                    formatter.format(picked),
                  );

                  setState(() {
                    _selectedDate = picked;
                  });
                }
              },
              initialDateTime: DateTime.now(),
              minimumYear: DateTime.now().year,
              maximumYear: 2030,
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(widget.optionName),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  _selectedDate == null
                      ? 'Select The ${widget.optionName}'
                      : formatter.format(_selectedDate),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.calendar_today_outlined,
                  color: kPrimaryColor,
                  size: 16,
                ),
                onPressed: _startDatePicker,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
