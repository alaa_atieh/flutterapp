import 'package:json_annotation/json_annotation.dart';

part 'voucher.g.dart';

@JsonSerializable(explicitToJson: true)
class Voucher {
  int code;
  String description;
  @JsonKey(name: 'from_name')
  String fromName;
  @JsonKey(name: 'from_email')
  String fromEmail;
  @JsonKey(name: 'to_name')
  String toName;
  @JsonKey(name: 'to_email')
  String toEmail;
  @JsonKey(name: 'voucher_theme_id')
  String voucherThemeId;
  String message;
  String price;
  double amount;

  Voucher({
    this.code,
    this.description,
    this.fromName,
    this.fromEmail,
    this.toName,
    this.toEmail,
    this.voucherThemeId,
    this.message,
    this.price,
    this.amount,
  });

  factory Voucher.fromJson(Map<String, dynamic> json) =>
      _$VoucherFromJson(json);
  Map<String, dynamic> toJson() => _$VoucherToJson(this);

  @override
  String toString() {
    return 'Vouchers(code: $code, description: $description, fromName: $fromName, fromEmail: $fromEmail, toName: $toName, toEmail: $toEmail, voucherThemeId: $voucherThemeId, message: $message, price: $price, amount: $amount)';
  }
}
