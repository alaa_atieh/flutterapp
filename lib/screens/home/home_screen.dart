import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:australia_garden/core/size_config.dart';

import '../../core/constants.dart';
import '../../core/enums.dart';
import '../../core/widgets/press_twice_to_exit.dart';
import '../cart/presentation/pages/check_out_page.dart';
import '../gift/presentaion/pages/send_voucher_gift.dart';
import '../search/presentaion/pages/search_bar.dart';
import 'components/body.dart';
import 'components/custom_bottom_nav_bar.dart';

class HomeScreen extends StatefulWidget {
  static String routeName = "/home";

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with TickerProviderStateMixin<HomeScreen> {
  TabController bottomTabController;

  void initState() {
    bottomTabController = TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return PressTwiceToExit(
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            children: [
              Padding(
                padding: EdgeInsets.only(right: SizeConfig.screenWidth * 0.02),
                child: SvgPicture.asset(
                  'assets/australia_garden_logo.svg',
                  height: SizeConfig.screenWidth * 0.08,
                  width: SizeConfig.screenWidth * 0.08,
                ),
              ),
              FittedBox(
                child: Text(
                  'Australia Garden',
                  style: TextStyle(
                    fontSize: SizeConfig.screenWidth * 0.035,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'ALCHEVROLA',
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
          actions: [
            IconButton(
              icon: Icon(
                Icons.shopping_cart_outlined,
                color: kPrimaryColor,
              ),
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => CheckOutPage(),
                ),
              ),
            ),
            IconButton(
              tooltip: 'Send a voucher gift',
              icon: Icon(
                Icons.card_giftcard_outlined,
                color: kPrimaryColor,
              ),
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => SendVoucherGift(),
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.search,
                color: kPrimaryColor,
              ),
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => SearchBar(),
                ),
              ),
            ),
          ],
        ),
        body: Container(
          child: Body(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/backgroundimage.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
      ),
    );
  }
}
