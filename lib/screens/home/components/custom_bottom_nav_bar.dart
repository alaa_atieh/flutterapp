import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/enums.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/auth/sign_in/sign_in_screen.dart';

import 'package:australia_garden/screens/category/category_list_page.dart';
import 'package:australia_garden/screens/favourites/presentaion/pages/favourite_list.dart';
import 'package:australia_garden/screens/home/home_screen.dart';
import 'package:australia_garden/screens/profile/profile_screen.dart';
import 'package:flutter/material.dart';

class CustomBottomNavBar extends StatelessWidget {
  CustomBottomNavBar({
    Key key,
    @required this.selectedMenu,
  }) : super(key: key);

  final MenuState selectedMenu;

  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();
  bool _isLoggedIn;
  @override
  Widget build(BuildContext context) {
    final Color inActiveIconColor = Color(0xFFB6B6B6);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 14),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          ),
        ],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
          topRight: Radius.circular(40),
        ),
      ),
      child: SafeArea(
        top: false,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Icon(
                Icons.home_outlined,
                color: MenuState.home == selectedMenu
                    ? kPrimaryColor
                    : inActiveIconColor,
              ),
              onPressed: () {
                if (MenuState.home != selectedMenu) {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (_) => HomeScreen(),
                    ),
                  );
                }
              },
            ),
            IconButton(
              icon: Icon(
                Icons.category_outlined,
                color: MenuState.category == selectedMenu
                    ? kPrimaryColor
                    : inActiveIconColor,
              ),
              onPressed: () {
                if (MenuState.category != selectedMenu) {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (_) => CategoryListPage(),
                    ),
                  );
                }
              },
            ),
            IconButton(
              icon: Icon(
                Icons.favorite_border_outlined,
                color: MenuState.favourite == selectedMenu
                    ? kPrimaryColor
                    : inActiveIconColor,
              ),
              onPressed: () {
                _isLoggedIn = _sharedPreferencesManager.getBool('is_logged_in');
                if (MenuState.favourite != selectedMenu && _isLoggedIn) {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (_) => FavouriteListView(true),
                    ),
                  );
                } else if (!_isLoggedIn) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => SignInScreen(),
                    ),
                  );
                }
              },
            ),
            IconButton(
              icon: Icon(
                Icons.person_outline,
                color: MenuState.profile == selectedMenu
                    ? kPrimaryColor
                    : inActiveIconColor,
              ),
              onPressed: () {
                _isLoggedIn = _sharedPreferencesManager.getBool('is_logged_in');
                if (MenuState.profile != selectedMenu && _isLoggedIn) {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (_) => ProfileScreen(),
                    ),
                  );
                } else if (!_isLoggedIn) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => SignInScreen(),
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
