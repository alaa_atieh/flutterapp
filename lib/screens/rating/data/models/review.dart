class Review {
  String author;
  String text;
  int rating;
  String dateAdded;

  Review({this.author, this.text, this.rating, this.dateAdded});

  factory Review.fromJson(var json) {
    return Review(
        author: json['author'],
        text: json['text'],
        rating: json['rating'],
        dateAdded: json['date_added']);
  }
  @override
  String toString() {
    return 'auther: {$author} , text: {$text} , rating: {$rating} , dateAdded: {$dateAdded}';
  }
}
