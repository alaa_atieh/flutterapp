import 'package:meta/meta.dart';

class RelatedProduct {
  int productId;
  String thumb;
  String name;
  String description;
  String price;
  String special;
  String tax;
  String minimum;
  int rating;
  int percent;
  bool isFavorite;
  bool isInCompareList;
  bool optionsIsRequired;

  RelatedProduct(
      {@required this.productId,
      @required this.thumb,
      @required this.name,
      @required this.description,
      @required this.price,
      @required this.special,
      @required this.tax,
      @required this.minimum,
      @required this.rating,
      @required this.percent,
      @required this.isFavorite,
      @required this.isInCompareList,
      @required this.optionsIsRequired});

  @override
  String toString() {
    return 'RelatedProduct(productId: $productId, thumb: $thumb, name: $name, description: $description, price: $price, special: $special, tax: $tax, minimum: $minimum, rating: $rating, percent: $percent, isFavorite: $isFavorite, isInCompareList: $isInCompareList, optionsIsRequired: $optionsIsRequired)';
  }
}
