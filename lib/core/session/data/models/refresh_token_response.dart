import 'package:json_annotation/json_annotation.dart';

part 'refresh_token_response.g.dart';

@JsonSerializable()
class RefreshTokenResponseModel {
  String language;
  String success;
  @JsonKey(name: 'api_token')
  String apiToken;

  RefreshTokenResponseModel({
    this.language,
    this.success,
    this.apiToken,
  });
  factory RefreshTokenResponseModel.fromJson(Map<String, dynamic> json) =>
      _$RefreshTokenResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$RefreshTokenResponseModelToJson(this);
}
