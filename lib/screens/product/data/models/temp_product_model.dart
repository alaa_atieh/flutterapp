import 'package:australia_garden/core/models/image.dart';
import 'package:australia_garden/screens/product/data/models/product_attribute_model.dart';
import 'package:australia_garden/screens/product/data/models/product_option_model.dart';
import 'package:australia_garden/screens/product/data/models/related_product_model.dart';
import 'package:australia_garden/screens/product/domin/entities/temp_product.dart';
import 'package:meta/meta.dart';

import 'discount_model.dart';

class TempProductModel extends Product {
  TempProductModel(
      {@required String productName,
      @required int productId,
      @required String manufacturer,
      @required String model,
      @required int reward,
      @required int points,
      @required String description,
      @required String stock,
      @required String thumb,
      @required List<ProductImage> images,
      @required String price,
      @required String special,
      @required int productPercent,
      @required String tax,
      @required List<DiscountModel> discounts,
      @required List<ProductOptionModel> options,
      @required String minimum,
      @required String reviewStatus,
      @required bool reviewGuest,
      @required String customerName,
      @required String reviews,
      @required int rating,
      @required List<AttributeGroupModel> attributeGroups,
      @required List<RelatedProductModel> relatedProducts,
      @required List tags,
      @required bool isFavorite,
      @required bool isInCompareList,
      @required String tapReview,
      @required int quantity})
      : super(
            productName: productName,
            productId: productId,
            manufacturer: manufacturer,
            model: model,
            reward: reward,
            points: points,
            description: description,
            stock: stock,
            thumb: thumb,
            images: images,
            price: price,
            special: special,
            productPercent: productPercent,
            tax: tax,
            discounts: discounts,
            options: options,
            minimum: minimum,
            reviewStatus: reviewStatus,
            reviewGuest: reviewGuest,
            customerName: customerName,
            reviews: reviews,
            rating: rating,
            attributeGroups: attributeGroups,
            relatedProducts: relatedProducts,
            tags: tags,
            isFavorite: isFavorite,
            isInCompareList: isInCompareList,
            tapReview: tapReview,
            quantity: quantity);

  factory TempProductModel.fromJson(var json) {
    var discountList = json['discounts'] as List;
    var optionsList = json['options'] as List;
    var relatedProductsList = json['products'] as List;
    var images = json['images'] as List;
    var attributeGroupList = json['attribute_groups'] as List;
    return TempProductModel(
        productName: json['product_name'],
        productId: json['product_id'],
        manufacturer: json['manufacturer'],
        model: json['model'],
        reward: json['reward'],
        points: json['points'],
        description: json['description'],
        stock: json['stock'],
        thumb: json['thumb'],
        images: images.map((e) => ProductImage.fromJson(e)).toList(),
        price: json['price'],
        special: json['special'],
        productPercent: json['product_percent'],
        tax: json['tax'],
        discounts: discountList.map((e) => DiscountModel.fromJson(e)).toList(),
        options:
            optionsList.map((e) => ProductOptionModel.fromJson(e)).toList(),
        minimum: json['minimum'],
        reviewStatus: json['review_status'],
        reviewGuest: json['review_guest'],
        customerName: json['customer_name'],
        reviews: json['reviews'],
        rating: json['rating'],
        attributeGroups: attributeGroupList
            .map(
              (attributeGroup) => AttributeGroupModel.fromJson(attributeGroup),
            )
            .toList(),
        relatedProducts: relatedProductsList
            .map((e) => RelatedProductModel.fromJson(e))
            .toList(),
        tags: json['tags'],
        isFavorite: json['isFavorite'],
        isInCompareList: json['isInCompareList'],
        tapReview: json['tab_review'],
        quantity: json['quantity']);
  }
}
