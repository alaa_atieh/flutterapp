import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/checkout/data/datasources/checkout_requests.dart';
import 'package:australia_garden/screens/gift/data/datasources/gift_requests.dart';
import 'package:australia_garden/screens/search/data/datasources/search_requests.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  SharedPreferencesManager sharedPreferencesManager =
      await SharedPreferencesManager.getInstance();

  locator.registerSingleton<SharedPreferencesManager>(sharedPreferencesManager);

  locator.registerLazySingleton<GiftRequest>(
    () => GiftRequestImpl(),
  );
  // locator.registerSingleton<GiftRequest>(GiftRequestImpl(), signalsReady: true);
  locator.registerSingleton<SessionRequest>(SessionRequestImpl(),
      signalsReady: true);

  locator.registerLazySingleton<SearchRequests>(
    () => SearchRequestsImbl(),
  );

  locator.registerLazySingleton<CheckoutRequests>(
    () => CheckoutRequestsImpl(),
  );
}
