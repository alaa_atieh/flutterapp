import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/models/currency.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class SettingsApi {
  /* static final baseURL =
      'http://192.168.1.108:8087/deluxe/index.php?route=apiCustomize'; */
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  Future<List<Currency>> listCurrencies() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    try {
      r = await http.get(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/currency',
              'api_token': apiToken,
            },
          ).toString(),
        ),
      );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          List add = decoded['currencies'] as List;

          List<Currency> currencies =
              add.map((e) => Currency.fromJson(e)).toList();
          return currencies;
        } else if (r.statusCode == 403) {
          await _sessionRequest.refreshClientToken(false);
          return await listCurrencies();
        } else {
          // If the server did not return a 200 OK response,
          // then throw an exception.
          throw Exception('Failed to List Currencies');
        }
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<bool> changeCurrency(String code) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    try {
      r = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/currency/currency',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'code': code,
        },
      );
      /* r = await http
          .post('$baseURL/currency/currency&api_token=$apiToken', body: {
        'code': code,
      }); */

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        return true;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await changeCurrency(code);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Change Currency');
      }

      //timeout(Duration(seconds: 10));
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'Can\'t change currency no internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
      return false;
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<dynamic> changePassword(
    String oldPassword,
    String newPassword,
    String confirmPassword,
  ) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    try {
      r = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/customer/password',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'password': newPassword,
          'confirm': confirmPassword,
          'old': oldPassword
        },
      );
      /* r = await http.post('$baseURL/customer/password&api_token=$apiToken',
          body: {
            'password': newPassword,
            'confirm': confirmPassword,
            'old': oldPassword
          },); */

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        return decoded;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await listCurrencies();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Change Password');
      }

      //timeout(Duration(seconds: 10));
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'Can\'t change Password no internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
      return null;
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return false;
    }
  }
}
