import 'package:australia_garden/core/app_properties.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/theme.dart';
import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:australia_garden/screens/product/domin/entities/temp_product.dart';
import 'package:australia_garden/screens/rating/data/models/review.dart';
import 'package:australia_garden/screens/rating/presentation/pages/rating_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:australia_garden/core/size_config.dart';
import '../../data/datasources/rating_requests.dart';

class RatingPage extends StatefulWidget {
  final Product product;

  RatingPage({
    @required this.product,
  });

  @override
  _RatingPageState createState() => _RatingPageState();
}

class _RatingPageState extends State<RatingPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  double rating = 0.0;
  List<int> ratings = [2, 1, 5, 2];

  List<Review> reviews;

  final ratingRequests = RatingRequests();

  Future<List<Review>> _getReviews(int productId) async {
    return await ratingRequests.getReviews(productId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        brightness: Brightness.light,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0.0,
        actions: <Widget>[
          widget.product.reviewGuest
              ? IconButton(
                  icon: Icon(Icons.comment, color: kPrimaryColor),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => Dialog(
                        shape: BeveledRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                        child: RatingDialog(
                          product: widget.product,
                          scaffoldKey: _scaffoldKey,
                        ),
                      ),
                    );
                  },
                  color: Colors.black,
                )
              : FittedBox(),
        ],
      ),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (b, constraints) => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: SingleChildScrollView(
              child: FutureBuilder(
                future: _getReviews(
                  widget.product.productId,
                ),
                builder: (context, snapshot) {
                  if (snapshot.connectionState != ConnectionState.done) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (snapshot.hasError) {
                    return RetryButton(
                      action: () => setState(
                        () {},
                      ),
                    );
                  }
                  if (snapshot.hasData) {
                    return Column(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              padding:
                                  EdgeInsets.all(SizeConfig.screenWidth * 0.04),
                              height: 92,
                              width: 92,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                                boxShadow: shadow,
                                border: Border.all(
                                    width: 5.0, color: Colors.yellow.shade700),
                              ),
                              child: Image.network(
                                widget.product.thumb,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 72.0, vertical: 16.0),
                              // here where the product name will go
                              child: Text(
                                widget.product.productName,
                                style: TextStyle(fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            )
                          ],
                        ),
                        // Divider(),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 40),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                  padding: const EdgeInsets.only(right: 16.0),
                                  // here where the average rating get calculated
                                  child: snapshot.hasData
                                      ? snapshot.data.isEmpty
                                          ? Text('No reviews yet')
                                          : Text(
                                              ((snapshot.data as List<Review>).fold<
                                                              int>(
                                                          0,
                                                          (int sumSoFar,
                                                                  Review
                                                                      currentReviw) =>
                                                              (sumSoFar +
                                                                  currentReviw
                                                                      .rating)) /
                                                      snapshot.data.length)
                                                  .ceil()
                                                  .toString(),
                                              style: TextStyle(fontSize: 48),
                                            )
                                      : Text('No reviews yet')),
                              Column(
                                children: <Widget>[
                                  FlutterRatingBar(
//                      borderColor: Color(0xffFF8993),
//                      fillColor: Color(0xffFF8993),
                                    ignoreGestures: true,
                                    itemSize: 20,
                                    allowHalfRating: true,
                                    // here is the rating on the RatingBar
                                    initialRating: snapshot.hasData
                                        ? snapshot.data.isEmpty
                                            ? 0.0
                                            : ((snapshot.data as List<Review>).fold<
                                                            int>(
                                                        0,
                                                        (int sumSoFar,
                                                                Review
                                                                    currentReviw) =>
                                                            (sumSoFar +
                                                                currentReviw
                                                                    .rating)) /
                                                    snapshot.data.length)
                                                .ceil()
                                                .toDouble()
                                        : 0.0,
                                    itemPadding:
                                        EdgeInsets.symmetric(horizontal: 4.0),
                                    fullRatingWidget: Icon(
                                      Icons.favorite,
                                      color: Colors.red,
                                      size: 20,
                                    ),
                                    noRatingWidget: Icon(Icons.favorite_border,
                                        color: Colors.red, size: 20),
                                    onRatingUpdate: (value) {
                                      setState(() {
                                        rating = value;
                                      });
                                    },
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    // here is the total amount of ratings
                                    child: snapshot.hasData
                                        ? Text(
                                            "from ${snapshot.data.length} people")
                                        : Text('No reviews Yet'),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 16.0),
                          child: Align(
                              alignment: Alignment(-1, 0),
                              child: Text('Recent Reviews')),
                        ),
                        Column(
                          children: snapshot.hasData
                              ? <Widget>[
                                  ...(snapshot.data as List<Review>)
                                      .map((val) => Container(
                                          margin: const EdgeInsets.symmetric(
                                              vertical: 4.0),
                                          padding: const EdgeInsets.all(8.0),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5.0))),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 16.0),
                                                child: CircleAvatar(
                                                  maxRadius: 14,
                                                  backgroundImage: AssetImage(
                                                      'assets/background.jpg'),
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        //here where the author name will go
                                                        Text(
                                                          val.author,
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                        //here where the date will go
                                                        Text(
                                                          val.dateAdded,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.grey,
                                                              fontSize: 10.0),
                                                        )
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          vertical: 8.0),
                                                      child: FlutterRatingBar(
//                                borderColor: Color(0xffFF8993),
//                                fillColor: Color(0xffFF8993),
                                                        ignoreGestures: true,
                                                        itemSize: 20,
                                                        allowHalfRating: false,
                                                        // here where the rating will go
                                                        initialRating: val
                                                            .rating
                                                            .toDouble(),
                                                        itemPadding: EdgeInsets
                                                            .symmetric(
                                                                horizontal:
                                                                    4.0),
                                                        fullRatingWidget: Icon(
                                                          Icons.favorite,
                                                          color: Colors.red,
                                                          size: 14,
                                                        ),
                                                        noRatingWidget: Icon(
                                                            Icons
                                                                .favorite_border,
                                                            color: Colors.red,
                                                            size: 14),
                                                        onRatingUpdate:
                                                            (value) {
                                                          setState(() {
                                                            rating = value;
                                                          });
                                                        },
                                                      ),
                                                    ),
                                                    // here where the rating text will go
                                                    Text(
                                                      val.text,
                                                      style: TextStyle(
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          )))
                                      .toList()
                                ]
                              : <Widget>[Text('No reviews Yet')],
                        )
                      ],
                    );
                  }
                  return RetryButton(
                    action: () => setState(
                      () {},
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
