import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/category/sort_rules.dart';
import 'package:australia_garden/screens/product/data/models/related_product_model.dart';
import 'package:australia_garden/screens/product/domin/entities/related_product.dart';
import 'package:australia_garden/screens/search/data/models/search_query.dart';

abstract class SearchRequests {
  Future<List<RelatedProduct>> search(SearchQueryRequest searchQueryRequest);
}

class SearchRequestsImbl implements SearchRequests {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  @override
  Future<List<RelatedProduct>> search(
    SearchQueryRequest searchQueryRequest,
  ) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');

    try {
      final response = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/product/search',
                  'api_token': apiToken,
                  'search': searchQueryRequest.query,
                  'description': searchQueryRequest.description?.toString() ??
                      true.toString(),
                  'attributes': searchQueryRequest.attributes?.toString() ??
                      true.toString(),
                  'category_id': searchQueryRequest.categoryId != -1
                      ? searchQueryRequest.categoryId?.toString() ?? ''
                      : '',
                  'order': searchQueryRequest.order?.toString() ??
                      SortOrder.FromHighestToLowest.toString(),
                  'sort': searchQueryRequest.sort?.toString() ??
                      SortType.Default.toString(),
                  'limit':
                      searchQueryRequest.limit?.toString() ?? 10.toString(),
                  'page': searchQueryRequest.page?.toString() ?? 1.toString(),
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      print(response.body);

      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        final List<dynamic> data = json.decode(response.body)['products'];
        final temp = data
            .map(
              (e) => RelatedProductModel.fromJson(e),
            )
            .toList();
        return temp;
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await search(searchQueryRequest);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Execute Search Query');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
    }
    return null;
  }
}
