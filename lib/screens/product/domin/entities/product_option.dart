import 'package:meta/meta.dart';

import 'product_option_value.dart';

class ProductOption {
  String productOptionId;
  List<ProductOptionValue> productOptionValues;
  String optionId;
  String name;
  String type;
  String value;
  bool requiredField;

  ProductOption(
      {@required this.productOptionId,
      @required this.productOptionValues,
      @required this.optionId,
      @required this.name,
      @required this.type,
      @required this.value,
      @required this.requiredField});
}
