import 'dart:async';

import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/theme.dart';
import 'package:australia_garden/screens/category/product_view.dart';
import 'package:australia_garden/screens/product/domin/entities/related_product.dart';
import 'package:australia_garden/screens/product/presentation/details/details_screen.dart';
import 'package:australia_garden/screens/search/data/models/search_query.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class SearchResults extends StatefulWidget {
  final List<RelatedProduct> searchResult;
  final Future<bool> Function(SearchQueryRequest, bool) reSearch;
  final Function(SearchQueryRequest) changeSearchRequestOptions;
  final SearchQueryRequest searchQueryRequest;
  final bool hasNewResult;
  final StreamController<List<RelatedProduct>> searchStreamController;

  const SearchResults({
    Key key,
    this.searchResult,
    this.reSearch,
    this.changeSearchRequestOptions,
    this.searchQueryRequest,
    this.hasNewResult,
    this.searchStreamController,
  }) : super(key: key);

  @override
  _SearchResultsState createState() => _SearchResultsState();
}

class _SearchResultsState extends State<SearchResults> {
  final _scrollController = ScrollController();
  bool _isLoading = false;

  void handleScroll() async {
    setState(
      () {
        _isLoading = true;
      },
    );
    await widget.reSearch(widget.searchQueryRequest, false);
    setState(
      () {
        _isLoading = false;
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final fsb = FloatingSearchBar.of(context);

    return NotificationListener<ScrollNotification>(
      onNotification: (notification) {
        if (widget.hasNewResult &&
            notification is ScrollEndNotification &&
            _scrollController.position.extentAfter == 0) {
          handleScroll();
        }
        return false;
      },
      child: FloatingSearchBarScrollNotifier(
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder(
                stream: widget.searchStreamController.stream,
                builder: (
                  BuildContext context,
                  AsyncSnapshot<List<RelatedProduct>> snapshot,
                ) {
                  if (snapshot.hasError) {
                    return Text(snapshot.error);
                  } else if (snapshot.hasData && snapshot.data.isNotEmpty) {
                    return ListView.builder(
                      padding: EdgeInsets.only(
                        top: fsb.height + fsb.margins.vertical,
                      ),
                      shrinkWrap: true,
                      controller: _scrollController,
                      itemCount: _isLoading
                          ? snapshot.data.length + 1
                          : snapshot.data.length,
                      itemBuilder: (context, index) {
                        if (index < snapshot.data.length) {
                          return ListTile(
                            title: Text(
                              snapshot.data[index].name,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline2
                                  .copyWith(
                                    color: AppColors.black,
                                    fontSize: 18,
                                    fontFamily: 'Metropolis',
                                    fontWeight: FontWeight.w400,
                                  ),
                            ),
                            leading: Image.network(
                              snapshot.data[index].thumb,
                              fit: BoxFit.contain,
                              height: SizeConfig.screenHeight * 0.068,
                              width: SizeConfig.screenHeight * 0.068,
                            ),
                            subtitle: buildPrice(
                              Theme.of(context),
                              snapshot.data[index],
                            ),
                            onTap: () async {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return DetailsScreen(
                                      snapshot.data[index].productId,
                                      'module_title',
                                    );
                                  },
                                ),
                              );
                            },
                          );
                        } else {
                          return _isLoading
                              ? Container(
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                )
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                );
                        }
                      },
                    );
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return Center(
                      child: SvgPicture.asset(
                        'assets/search_failed.svg',
                        height: SizeConfig.screenHeight,
                        width: SizeConfig.screenWidth,
                      ),
                    );
                  }
                  // }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
