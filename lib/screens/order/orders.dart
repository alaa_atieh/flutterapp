import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/theme.dart';
import 'package:australia_garden/core/widgets/RetryButton.dart';

import 'order.dart';
import 'order_api.dart';
import 'order_details.dart';
import 'order_tile.dart';

class OrderList extends StatefulWidget {
  final Function changeView;
  OrderApi o = new OrderApi();

  OrderList({Key key, this.changeView}) : super(key: key);

  @override
  _OrderListState createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  final List<Widget> tabs = <Widget>[
    Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppSizes.sidePadding),
      child: Tab(
        child: FittedBox(
          child: Text('Delivered'),
        ),
      ),
    ),
    Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppSizes.sidePadding),
      child: Tab(
        child: FittedBox(
          child: Text('Processing'),
        ),
      ),
    ),
    Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppSizes.sidePadding),
      child: Tab(
        child: FittedBox(
          child: Text('Cancelled'),
        ),
      ),
    ),
  ];

  ListView buildOrderList(List<Order> orders) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: orders.length,
      itemBuilder: (context, index) {
        return OpenFlutterOrderTile(
          order: orders[index],
          onClick: ((String orderId) => {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => OrderDetails(
                          order: orders[index],
                        )))
              }),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: FutureBuilder(
        future: widget.o.orderList(),
        builder: (BuildContext context,
            AsyncSnapshot<Map<String, List<Order>>> snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError) {
            return RetryButton(
              action: () => setState(
                () {},
              ),
            );
          }
          if (snapshot.hasData) {
            return SafeArea(
              child: DefaultTabController(
                length: tabs.length,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: AppSizes.sidePadding),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Your Orders", style: headingStyle),
                            Padding(
                                padding: EdgeInsets.only(
                                    bottom: AppSizes.sidePadding)),
                            TabBar(
                              indicatorSize: TabBarIndicatorSize.tab,
                              labelColor: AppColors.white,
                              labelPadding: EdgeInsets.symmetric(horizontal: 4),
                              unselectedLabelColor: AppColors.black,
                              indicator: BubbleTabIndicator(
                                indicatorHeight: 32,
                                indicatorColor: Colors.black,
                                tabBarIndicatorSize: TabBarIndicatorSize.tab,
                              ),
                              tabs: tabs,
                              unselectedLabelStyle: TextStyle(
                                  color: AppColors.lightGray,
                                  fontSize: 14,
                                  fontFamily: 'Metropolis',
                                  fontWeight: FontWeight.w400),
                              labelStyle: TextStyle(
                                      color: AppColors.lightGray,
                                      fontSize: 14,
                                      fontFamily: 'Metropolis',
                                      fontWeight: FontWeight.w400)
                                  .copyWith(color: AppColors.white),
                            ),
                          ]),
                    ),
                    Padding(
                        padding: EdgeInsets.only(bottom: AppSizes.sidePadding)),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: AppSizes.sidePadding),
                        child: TabBarView(
                          children: <Widget>[
                            snapshot.data['completed'] != null
                                ? buildOrderList(snapshot.data['completed'])
                                : Container(
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "assets/no_orders_yet.png"),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                            snapshot.data['processing'] != null
                                ? buildOrderList(snapshot.data['processing'])
                                : Container(
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "assets/no_orders_yet.png"),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                            snapshot.data['canceled'] != null
                                ? buildOrderList(snapshot.data['canceled'])
                                : Container(
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            'assets/no_orders_yet.png'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
          return RetryButton(
            action: () => setState(
              () {},
            ),
          );
        },
      ),
    );
  }
}
