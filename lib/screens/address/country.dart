class Country {
  String id;
  String name;
  bool postalCode;

  Country({this.id, this.name, this.postalCode});
  Country.fromJson(Map<String, dynamic> json) {
    id = json['country_id'];
    name = json['name'];
    if (json['postcode_required'] == "0") {
      postalCode = false;
    } else
      postalCode = true;
  }
}
