import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:australia_garden/screens/splash/splash_screen.dart';

import 'core/dependency_injection/injector.dart';
import 'core/routes.dart';
import 'core/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ],
  );
  await setupLocator();
  return runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Australia Garden',
      debugShowCheckedModeBanner: false,
      theme: theme(),
      home: SplashScreen(),
      routes: routes,
    );
  }
}
