import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/address/address_api.dart';
import 'package:flutter/material.dart';

import 'components/address_form.dart';

class AddAddressScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  Function() onAddAddress;

  AddAddressScreen(this.scaffoldKey, this.onAddAddress);

  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  bool postal = false;
  AddressApi a = new AddressApi();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add address"),
      ),
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: SizeConfig.screenHeight * 0.04),
                  Text(
                    "Add your new address",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: getProportionateScreenWidth(28),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: SizeConfig.screenHeight * 0.08),
                  AddressForm(
                    scaffoldKey: widget.scaffoldKey,
                    onAddAddress: widget.onAddAddress,
                    type: "add",
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Input extends StatefulWidget {
  Input(
      {Key key,
      @required this.controller,
      @required this.hintText,
      @required this.validator,
      this.secure,
      this.icon,
      this.keyboardType})
      : super(key: key);

  TextEditingController controller;
  final String hintText;
  Function validator;

  final bool secure;
  final Icon icon;
  TextInputType keyboardType;

  @override
  _InputState createState() => _InputState();
}

class _InputState extends State<Input> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          labelText: widget.hintText,
          errorMaxLines: 2,
          prefixIcon: widget.icon),
      controller: widget.controller,
      obscureText: widget.secure,
      style: TextStyle(fontSize: 16.0),
      validator: widget.validator,
      keyboardType: widget.keyboardType,
    );
  }
}
