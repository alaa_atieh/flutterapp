import 'package:australia_garden/screens/product/data/models/related_product_model.dart';
import 'package:australia_garden/screens/product/domin/entities/related_product.dart';

class Module {
  String title;
  List<RelatedProduct> products;

  Module(this.title, this.products);
  Module.fromJson(Map<String, dynamic> json) {
    var productslist = json['products'] as List;
    title = json['title'];
    products =
        productslist.map((e) => RelatedProductModel.fromJson(e)).toList();
  }

  @override
  String toString() => 'Module(title: $title, products: $products)';
}
