import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import 'order.dart';
import 'order_detail_model.dart';

class OrderApi {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  Future<DetailedOrder> orderInfo(String orderId) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    DetailedOrder d;
    try {
      r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/order/details',
                  'api_token': apiToken,
                  'order_id': orderId,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          d = DetailedOrder.fromJson(decoded);
          return d;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await orderInfo(orderId);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Add to Cart');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<Map<String, List<Order>>> orderList() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');

    Map<String, List<Order>> orders = new Map<String, List<Order>>();

    var r;
    try {
      r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/order',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          if (decoded['orders']['processing'] != null) {
            List pro = decoded['orders']['processing'] as List;
            List<Order> processing = pro.map((e) => Order.fromJson(e)).toList();
            orders['processing'] = processing;
          }

          if (decoded['orders']['completed'] != null) {
            List com = decoded['orders']['completed'] as List;

            List<Order> completed = com.map((e) => Order.fromJson(e)).toList();
            orders['completed'] = completed;
          }

          if (decoded['orders']['canceled'] != null) {
            List can = decoded['orders']['canceled'] as List;
            List<Order> canceled = can.map((e) => Order.fromJson(e)).toList();
            orders['canceled'] = canceled;
          }
          return orders;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await orderList();
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<Map<String, String>> reOrder(
    String orderId,
    String orderProductId,
  ) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    Map<String, String> results = new Map<String, String>();
    var r;
    try {
      r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/order/reorder',
                  'api_token': apiToken,
                  'order_id': orderId,
                  'order_product_id': orderProductId,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          results['success'] = decoded['success'];
        } else {
          results['error'] = decoded['error'];
        }
        return results;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await reOrder(orderId, orderProductId);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Add to Cart');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection try again later',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }
}
