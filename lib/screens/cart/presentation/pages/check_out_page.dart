import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:australia_garden/core/app_properties.dart';
import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/debouncer/debouncer.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:australia_garden/screens/cart/data/datasources/cart_requests.dart';
import 'package:australia_garden/screens/cart/domain/entities/cart.dart';
import 'package:australia_garden/screens/cart/presentation/widgets/cart_item_card.dart';
import 'package:australia_garden/screens/cart/presentation/widgets/text_field_dialog.dart';
import 'package:australia_garden/screens/cart/presentation/widgets/total_list.dart';
import 'package:australia_garden/screens/cart/presentation/widgets/voucher_card.dart';
import 'package:australia_garden/screens/checkout/payment_method_screen.dart';

import '../../../address/payment_address.dart';

class CheckOutPage extends StatefulWidget {
  @override
  _CheckOutPageState createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {
  CartRequestImpl cartRequestImpl;
  Map<String, String> updatedCartItems;
  Debouncer _debouncer;
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<Map<String, String>> applyVoucherCode(String voucherCode) async {
    var temp = await cartRequestImpl.applyVoucherGift(voucherCode);
    setState(() {});
    return temp;
  }

  Future<Map<String, String>> applyCouponCode(String couponCode) async {
    var temp = await cartRequestImpl.applyCoupon(couponCode);
    setState(() {});
    return temp;
  }

  Future<void> onCartItemQuantityUpdated(
      String cartItemId, String newQuantity) {
    _debouncer.run(() async {
      String temp =
          await cartRequestImpl.updateCartItemQuantity(cartItemId, newQuantity);
    });
    //! Removed this to prevent number picker from calling setState which redraw the progress indicator
/*     setState(() {
      updatedCartItems[cartItemId] = newQuantity;
    }); */
  }

  @override
  void initState() {
    super.initState();
    cartRequestImpl = CartRequestImpl();
    _debouncer = Debouncer();
    updatedCartItems = updatedCartItems == null ? {} : updatedCartItems;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          iconTheme: IconThemeData(color: darkGrey),
          title: Text(
            'Checkout',
            style: TextStyle(
              color: darkGrey,
              fontWeight: FontWeight.bold,
              fontFamily: 'ALCHEVROLA',
              fontSize: 18.0,
            ),
          ),
        ),
        body: FutureBuilder(
          // TODO add stock status in the cart product menu
          future: cartRequestImpl.getCart(),
          builder: (BuildContext context, AsyncSnapshot<Cart> snapshot) {
            if (snapshot.connectionState != ConnectionState.done) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (snapshot.hasError) {
              return RetryButton(
                action: () => setState(
                  () {},
                ),
              );
            }
            if (snapshot.hasData) {
              return snapshot.data.products.isEmpty &&
                      snapshot.data.vouchers.isEmpty
                  ? Container(
                      height: constraints.maxHeight,
                      child: Center(
                        child: Image.asset('assets/images/empty_cart.jpg'),
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/alaa-10.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: LayoutBuilder(
                        builder: (_, constraints) => SingleChildScrollView(
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              minHeight: constraints.maxHeight,
                            ),
                            child: Column(
                              children: <Widget>[
                                snapshot.data.products.isNotEmpty
                                    ? Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    topRight:
                                                        Radius.circular(20),
                                                    bottomRight:
                                                        Radius.circular(20),
                                                  ),
                                                  color: kPrimaryColor),
                                              child: Padding(
                                                padding: EdgeInsets.all(
                                                  SizeConfig.screenWidth * 0.03,
                                                ),
                                                child: Text(
                                                  'Products',
                                                  textAlign: TextAlign.right,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily: 'ALCHEVROLA',
                                                    fontSize: 18.0,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            ListView.builder(
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              shrinkWrap: true,
                                              itemCount:
                                                  snapshot.data.products.length,
                                              itemBuilder: (_, index) =>
                                                  CartItemsCard(
                                                cartItem: snapshot
                                                    .data.products[index],
                                                onRemove: () {
                                                  setState(
                                                    () {
                                                      cartRequestImpl
                                                          .deleteCartItem(
                                                              snapshot
                                                                  .data
                                                                  .products[
                                                                      index]
                                                                  .cartId);
                                                    },
                                                  );
                                                },
                                                onQuantityChanged:
                                                    onCartItemQuantityUpdated,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                snapshot.data.vouchers.isNotEmpty
                                    ? Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.only(
                                                top: 20,
                                              ),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topRight: Radius
                                                                .circular(20),
                                                            bottomRight:
                                                                Radius.circular(
                                                                    20)),
                                                    color: kPrimaryColor),
                                                child: Padding(
                                                  padding: EdgeInsets.all(
                                                    SizeConfig.screenWidth *
                                                        0.03,
                                                  ),
                                                  child: Text(
                                                    'Vouchers',
                                                    textAlign: TextAlign.right,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontFamily: 'ALCHEVROLA',
                                                      fontSize: 18.0,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            ListView.builder(
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              shrinkWrap: true,
                                              itemCount:
                                                  snapshot.data.vouchers.length,
                                              itemBuilder: (_, index) =>
                                                  VoucherCard(
                                                voucher: snapshot
                                                    .data.vouchers[index],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                Container(
                                  margin: const EdgeInsets.all(16.0),
                                  padding: const EdgeInsets.fromLTRB(
                                      16.0, 0, 16.0, 16.0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    boxShadow: shadow,
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10),
                                    ),
                                  ),
                                  // this where the list of totals will go
                                  child: TotalsList(
                                    totals: snapshot.data.totals,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(
                                      SizeConfig.screenWidth * 0.045),
                                  child: Container(
                                    // padding: EdgeInsets.only(left: 16.0),
                                    decoration: BoxDecoration(
                                      color: Colors.transparent,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                      //color: Colors.grey[200],
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        TextFieldDialog(
                                          title: 'Apply a Voucher',
                                          onTextFieldSubmitted:
                                              applyVoucherCode,
                                          scaffoldKey: _scaffoldKey,
                                        ),
                                        TextFieldDialog(
                                          title: 'Apply a Coupon',
                                          onTextFieldSubmitted: applyCouponCode,
                                          scaffoldKey: _scaffoldKey,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            SizeConfig.screenHeight * 0.045,
                                        vertical:
                                            SizeConfig.screenWidth * 0.045),
                                    child: DefaultButton(
                                      press: () {
                                        if (snapshot.data.products.every(
                                            (element) =>
                                                element.stock == true &&
                                                element.maximum != 0)) {
                                          if (snapshot.data.products.isEmpty) {
                                            Navigator.of(context).push(
                                              MaterialPageRoute(
                                                builder: (_) =>
                                                    PaymentMethodScreen(
                                                  hasProducts: false,
                                                ),
                                              ),
                                            );
                                          } else {
                                            Navigator.of(context).push(
                                              MaterialPageRoute(
                                                builder: (_) =>
                                                    PaymentAddressScreen(),
                                              ),
                                            );
                                          }
                                        } else {
                                          Fluttertoast.showToast(
                                            msg:
                                                'Some product are not available in stock',
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.black54,
                                            textColor: Colors.white,
                                          );
                                        }
                                      },
                                      text: "Checkout",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
            }
            return RetryButton(
              action: () => setState(
                () {},
              ),
            );
          },
        ),
      ),
    );
  }
}
