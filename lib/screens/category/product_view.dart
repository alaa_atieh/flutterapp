import 'dart:math';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/theme.dart';
import 'package:australia_garden/screens/category/category.dart';
import 'package:australia_garden/screens/product/domin/entities/temp_product.dart';
import 'package:australia_garden/screens/product/presentation/details/details_screen.dart';
import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:australia_garden/screens/product/data/datasources/product_requests.dart';
import 'base_product_list_item.dart';
import 'base_product_tile.dart';
import 'favourite.dart';
import '../product/data/models/related_product_model.dart';
import 'product_rating.dart';
import 'package:australia_garden/core/constants.dart';

Widget getListView(BuildContext context, Category category) {
  return SliverList(
    delegate: SliverChildBuilderDelegate(
      (BuildContext context, int index) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: BaseProductListItem(
            onClick: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => DetailsScreen(
                      category.products[index].productId, "module_title")));
            },
            bottomRoundButton: Favourite(product: category.products[index]),
            image: Image.network(category.products[index].thumb).image,
            specialMark: category.products[index].specialMark,
            mainContentBuilder: (context) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(category.products[index].name,
                      style: TextStyle(
                          color: AppColors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Metropolis')),

                  // Text(product.name, style: Theme.of(context).textTheme.body1),
                  buildRating(category.products[index]),
                  buildPrice(Theme.of(context), category.products[index]),
                ],
              );
            },
          ),
        );
      },
      childCount: category.products?.length ?? 20,
    ),
  );
}

Widget getTileView(context, Category category) {
  return SliverGrid(
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
      childAspectRatio: AppSizes.tile_width / AppSizes.tile_height,
    ),
    delegate: SliverChildBuilderDelegate(
      (BuildContext context, int index) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: BaseProductTile(
              onClick: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => DetailsScreen(
                        category.products[index].productId, "module_title")));
              },
              bottomRoundButton: Favourite(product: category.products[index]),
              image: Image.network(category.products[index].thumb).image,
              mainContentBuilder: (context) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    buildRating(category.products[index]),
                    Padding(
                        padding: EdgeInsets.symmetric(vertical: 4),
                        child: Text(category.products[index].name,
                            style: TextStyle(
                                color: AppColors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w800,
                                fontFamily: 'Metropolis'))),
                    Row(
                      children: <Widget>[
                        buildPrice(Theme.of(context), category.products[index]),
                      ],
                    )
                  ],
                );
              },
              specialMark: category.products[index].specialMark),
        );
      },
      childCount: category.products?.length ?? 20,
    ),
  );
}

Widget buildPrice(ThemeData _theme, RelatedProductModel product) {
  return Row(children: <Widget>[
    product.percent > 0 ? buildDiscountPrice(_theme, product) : Container(),
    SizedBox(
      width: 4.0,
    ),
    Text(
      product.price != null ? product.price : '',
      style: _theme.textTheme.headline2.copyWith(
        color: AppColors.black,
        fontSize: 18,
        fontFamily: 'Metropolis',
        fontWeight: FontWeight.w400,
        decoration: product.percent > 0
            ? TextDecoration.lineThrough
            : TextDecoration.none,
      ),
    ),
  ]);
}

Widget buildRating(RelatedProductModel product) {
  return Container(
      padding: EdgeInsets.only(top: 4, bottom: 4),
      child: OpenFlutterProductRating(
        rating: product.rating,
        ratingCount: product.rating,
        alignment: MainAxisAlignment.start,
        iconSize: 12,
        labelFontSize: 12,
      ));
}

Widget buildDiscountPrice(ThemeData _theme, RelatedProductModel product) {
  return Text(product.special.toString(),
      style: _theme.textTheme.headline5.copyWith(
          fontSize: 18,
          fontFamily: 'Metropolis',
          fontWeight: FontWeight.w400,
          color: kPrimaryColor));
}

Widget getListViewFav(BuildContext context, List<RelatedProductModel> products,
    Function onRemove) {
  final ProductRequestsImpl _productRequestsImpl = ProductRequestsImpl();
  return SliverList(
    delegate: SliverChildBuilderDelegate(
      (BuildContext context, int index) {
        return Padding(
          padding:
              EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth * 0.03),
          child: BaseProductListItem(
            onRemove: () async {
              ProductRequestsImpl p = new ProductRequestsImpl();
              p.removeProductFromFavourites(products[index].productId);
              onRemove();
            },
            onClick: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => DetailsScreen(
                      products[index].productId, "module_title")));
            },

            bottomRoundButton: Container(
              width: SizeConfig.screenWidth / 8,
              height: SizeConfig.screenWidth / 8,
              child: FloatingActionButton(
                heroTag:
                    'Remove from Cart' + Random().nextInt(1000000).toString(),
                backgroundColor: kPrimaryColor,
                child: IconButton(
                  icon: Icon(
                    FontAwesomeIcons.cartPlus,
                    color: AppColors.white,
                  ),
                  onPressed: () async {
                    Product product = await _productRequestsImpl
                        .getProductDetails(products[index].productId);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => OptionsBuilder(
                          productId: product.productId.toString(),
                          options: product.options,
                          minimum: int.parse(product.minimum),
                          maximumQuantity: product.quantity,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            image: Image.network(
              products[index].thumb,
            ).image,
            //onRemove: onRemoveFromFavorites,
            specialMark: products[index].specialMark,
            mainContentBuilder: (context) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(products[index].name,
                      style: TextStyle(
                          color: AppColors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Metropolis')),
                  Row(
                    children: <Widget>[
                      _buildColor(Theme.of(context)),
                      Padding(
                        padding: EdgeInsets.all(4),
                      ),
                      _buildSize(Theme.of(context)),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      buildPrice(Theme.of(context), products[index]),
                      Padding(
                        padding: EdgeInsets.only(left: 4),
                      ),
                    ],
                  ),
                  buildRating(products[index])
                ],
              );
            },
          ),
        );
      },
      childCount: products.length,
    ),
  );
}

Widget getTileViewFav(BuildContext context, Category category) {
  return SliverGrid(
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
        childAspectRatio: 1.589),
    delegate: SliverChildBuilderDelegate(
      (BuildContext context, int index) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: AppSizes.sidePadding),
          child: BaseProductTile(
              onClick: () {},
              bottomRoundButton: FloatingActionButton(
                heroTag: 'Add to Cart' + Random().nextInt(1000000).toString(),
                backgroundColor: kPrimaryColor,
                // onPressed: onAddToCart,
                child: Icon(
                  FontAwesomeIcons.cartPlus,
                  color: AppColors.white,
                ),
              ),
              image: Image.network(
                category.products[index].thumb,
              ).image,
              //onRemove: onRemoveFromFavorites,
              specialMark: category.products[index].specialMark,
              mainContentBuilder: (context) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      category.products[index].name,
                      style: TextStyle(
                          color: AppColors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Metropolis'),
                    ),
                    Row(
                      children: <Widget>[
                        _buildColor(Theme.of(context)),
                        Padding(
                          padding: EdgeInsets.all(4),
                        ),
                        _buildSize(Theme.of(context)),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        buildPrice(Theme.of(context), category.products[index]),
                        Padding(
                          padding: EdgeInsets.only(left: AppSizes.linePadding),
                        ),
                        buildRating(category.products[index])
                      ],
                    )
                  ],
                );
              }),
        );
      },
      childCount: category.products?.length ?? 20,
    ),
  );
}

Widget _buildColor(
  ThemeData _theme,
  /* HashMap<ProductAttribute, String> selectedAttributes*/
) {
  String colorValue = '';
  /*selectedAttributes?.forEach((attribute, value) {
      if ( attribute.name == 'Color') colorValue = value;
    });*/
  return colorValue.isNotEmpty
      ? Row(
          children: <Widget>[
            Text('Color:', style: _theme.textTheme.bodyText1.copyWith()),
            Padding(
              padding: EdgeInsets.only(left: 4),
            ),
            Text(colorValue,
                style:
                    _theme.textTheme.bodyText1.copyWith(color: AppColors.black))
          ],
        )
      : Row();
}

Row _buildSize(
  ThemeData _theme,
  /*  HashMap<ProductAttribute, String> selectedAttributes*/
) {
  String sizeValue = '';
  /*selectedAttributes?.forEach((attribute, value) {
      if ( attribute.name == 'Size') sizeValue = value;
    });*/
  return sizeValue.isNotEmpty
      ? Row(
          children: <Widget>[
            Text(
              'Size:',
              style: _theme.textTheme.bodyText1.copyWith(),
            ),
            Padding(
              padding: EdgeInsets.only(left: AppSizes.linePadding),
            ),
            Text(sizeValue,
                style: TextStyle(
                    color: AppColors.black,
                    fontSize: 11,
                    fontFamily: 'Metropolis',
                    fontWeight: FontWeight.w400))
          ],
        )
      : Row();
}
