import 'package:flutter/material.dart';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/enums.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:australia_garden/core/widgets/press_twice_to_exit.dart';
import 'package:australia_garden/screens/category/category.dart';
import 'package:australia_garden/screens/home/components/custom_bottom_nav_bar.dart';

import 'apicalls/categories_api.dart';
import 'components/staggered_category_card.dart';

class CategoryListPage extends StatefulWidget {
  @override
  _CategoryListPageState createState() => _CategoryListPageState();
}

class _CategoryListPageState extends State<CategoryListPage> {
  CategoriesApi c = new CategoriesApi();
  List<Category> categories = [];

  List<Category> searchResults;
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    searchResults = categories;
  }

  @override
  Widget build(BuildContext context) {
    return PressTwiceToExit(
      child: Scaffold(
        bottomNavigationBar:
            CustomBottomNavBar(selectedMenu: MenuState.category),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/backgroundimage.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          margin: const EdgeInsets.only(top: kToolbarHeight),
          child: FutureBuilder(
            future: c.getCategories(),
            builder: (context, snapshot) {
              if (snapshot.connectionState != ConnectionState.done) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (snapshot.hasError) {
                return RetryButton(
                  action: () => setState(
                    () {},
                  ),
                );
              }
              if (snapshot.hasData) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Align(
                      alignment: Alignment(-1, 0),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                            color: kPrimaryColor),
                        child: Padding(
                          padding: EdgeInsets.all(
                            SizeConfig.screenWidth * 0.03,
                          ),
                          child: Text(
                            'Category List',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'ALCHEVROLA',
                            ),
                          ), // color: Colors.white,
                        ),
                      ),
                    ),
                    Flexible(
                      child: ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (_, index) => Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: 8.0,
                          ),
                          child: StaggeredCardCard(
                            category: snapshot.data[index],
                            color: Colors.teal[100],

                            //snapshot.data[index].image,
                          ),
                        ),
                      ),
                    )
                  ],
                );
              }
              return RetryButton(
                action: () => setState(
                  () {},
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
