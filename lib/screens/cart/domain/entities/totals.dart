import 'package:json_annotation/json_annotation.dart';

part 'totals.g.dart';

@JsonSerializable(explicitToJson: true)
class Totals {
  String title;
  String text;

  Totals({
    this.title,
    this.text,
  });
  factory Totals.fromJson(Map<String, dynamic> json) => _$TotalsFromJson(json);
  Map<String, dynamic> toJson() => _$TotalsToJson(this);

  @override
  String toString() => 'Totals(title: $title, text: $text)';
}
