import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'shipping_method.dart';
import 'payment_method.dart';

class ShippingMethodApi {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  Future<List<ShippingMethod>> listShippingMethods() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;

    try {
      r = await http.get(
        Uri.decodeFull(
         Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/shipping/methods',
              'api_token': apiToken,
            },
          ).toString(),
        ),
      );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          List sh = decoded['shipping_methods'] as List;

          List<ShippingMethod> shippingMethods =
              sh.map((e) => ShippingMethod.fromJson(e)).toList();
          return shippingMethods;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await listShippingMethods();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to List Shipping Methods');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<bool> saveShippingMethod(String code, String comment) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http.post(
        Uri.decodeFull(
         Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/shipping/method',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {'shipping_method': code, 'comment': comment},
      );
      /* var r = await http.post(
        '$baseurl/shipping/method&api_token=$apiToken',
        body: {'shipping_method': code, 'comment': comment},
      ); */
      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          return true;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        await saveShippingMethod(code, comment);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('shipping method Failed');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<List<PaymentMethod>> listPaymentMethods() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    try {
      r = await http.get(
        Uri.decodeFull(
         Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/payment/methods',
              'api_token': apiToken,
            },
          ).toString(),
        ),
      );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          List pa = decoded['payment_methods'] as List;

          List<PaymentMethod> paymentMethods =
              pa.map((e) => PaymentMethod.fromJson(e)).toList();
          return paymentMethods;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await listPaymentMethods();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to List Payment Methods');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<bool> savePaymentMethod(String code, String comment) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http.post(
        Uri.decodeFull(
         Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/payment/method',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {'payment_method': code, 'comment': comment, 'agree': '1'},
      );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          return true;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        await savePaymentMethod(code, comment);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Payment method Failed');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return false;
    }
  }
}
