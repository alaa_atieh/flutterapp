import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:flutter/material.dart';
import 'components/body.dart';
import 'components/custom_app_bar.dart';
import 'package:australia_garden/screens/product/data/datasources/product_requests.dart';

class DetailsScreen extends StatefulWidget {
  static String routeName = "/details";

  int productId;
  String moduleTitle;

  DetailsScreen(
    this.productId,
    this.moduleTitle,
  );

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  ProductRequestsImpl p = new ProductRequestsImpl();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: p.getProductDetails(widget.productId),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              ),
            );
          }
          if (snapshot.hasError) {
            return Scaffold(
              body: RetryButton(
                action: () => setState(
                  () {},
                ),
              ),
            );
          }
          if (snapshot.hasData) {
            return Scaffold(
              appBar: CustomAppBar(
                product: snapshot.data,
              ),
              body: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/alaa-10.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Body(
                  product: snapshot.data,
                  moduleTitle: widget.moduleTitle,
                ),
              ),
            );
          }
          return Scaffold(
            body: RetryButton(
              action: () => setState(
                () {},
              ),
            ),
          );
        } /* => snapshot.hasData
          ? Scaffold(
              appBar: CustomAppBar(
                product: snapshot.data,
              ),
              body: Container(

                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/alaa-10.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Body(
                  product: snapshot.data,
                  moduleTitle: moduleTitle,
                ),
              ),
            )
          : Scaffold(
              body: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              ),
            ), */
        );
  }
}
