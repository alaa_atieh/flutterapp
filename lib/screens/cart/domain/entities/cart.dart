import 'package:australia_garden/screens/cart/domain/entities/totals.dart';
import 'package:australia_garden/screens/cart/domain/entities/voucher.dart';
import 'package:json_annotation/json_annotation.dart';

import 'cart_item.dart';

part 'cart.g.dart';

@JsonSerializable(explicitToJson: true)
class Cart {
  List<CartItem> products;
  List<Voucher> vouchers;
  List<Totals> totals;

  Cart({
    this.products,
    this.vouchers,
    this.totals,
  });

  factory Cart.fromJson(Map<String, dynamic> json) => _$CartFromJson(json);
  Map<String, dynamic> toJson() => _$CartToJson(this);

  @override
  String toString() =>
      'Cart(products: $products, vouchers: $vouchers, totals: $totals)';
}
