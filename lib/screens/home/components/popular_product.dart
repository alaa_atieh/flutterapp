import 'package:flutter/material.dart';
import 'product_card.dart';
import 'package:australia_garden/screens/product/data/models/related_product_model.dart';
import 'package:australia_garden/core/size_config.dart';
import 'section_title.dart';

class ProductModule extends StatelessWidget {
  String title;
  List<RelatedProductModel> products;

  ProductModule(this.title, this.products);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(
            // top: getProportionateScreenWidth(5),
            bottom: getProportionateScreenWidth(8),
          ),
          child: SectionTitle(
            title: title,
            press: () {},
          ),
        ),
        SizedBox(
          height: getProportionateScreenWidth(20),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...List.generate(
                products.length,
                (index) {
                  return ProductCard(
                    product: products[index],
                    moduleTitle: title,
                  );
                },
              ),
              SizedBox(
                width: getProportionateScreenWidth(20),
              ),
            ],
          ),
        )
      ],
    );
  }
}
