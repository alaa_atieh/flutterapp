class Currency {
  //int id;
  String title;
  String code;
  String symbolLeft;
  String symbolRight;

  Currency(this.title, this.code, this.symbolLeft, this.symbolRight);
  Currency.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    code = json['code'];
    symbolLeft = json['symbol_left'];
    symbolRight = json['symbol_right'];
  }

  Map<String, dynamic> toJson() => {
        'title': title,
        'code': code,
        'symbol_left': symbolLeft,
        'symbol_right': symbolRight,
      };

  @override
  String toString() {
    return 'Currency(title: $title, code: $code, symbolLeft: $symbolLeft, symbolRight: $symbolRight)';
  }
}
