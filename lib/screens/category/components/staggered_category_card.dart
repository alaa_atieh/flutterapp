import 'package:australia_garden/core/size_config.dart';
import 'package:flutter/material.dart';
import 'package:australia_garden/screens/category/apicalls/categories_api.dart';
import 'package:australia_garden/screens/category/products_list.dart';
import 'package:australia_garden/screens/category/category.dart';
import 'package:australia_garden/screens/category/sort_rules.dart';
import 'package:australia_garden/core/app_properties.dart';

class CategoryCard extends StatelessWidget {
  Category category;

  CategoryCard({Key key, this.controller, this.category})
      :

        // Each animation defined here transforms its value during the subset
        // of the controller's duration defined by the animation's interval.
        // For example the opacity animation transforms its value during
        // the first 10% of the controller's duration.

        height = Tween<double>(begin: 150, end: 250.0).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0,
              0.300,
              curve: Curves.ease,
            ),
          ),
        ),
        itemHeight = Tween<double>(begin: 0, end: 150.0).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0,
              0.300,
              curve: Curves.ease,
            ),
          ),
        ),
        super(key: key);

  final Animation<double> controller;
  final Animation<double> height;
  final Animation<double> itemHeight;

  // This function is called each time the controller "ticks" a new frame.
  // When it runs, all of the animation's values will have been
  // updated to reflect the controller's current value.
  Widget _buildAnimation(BuildContext context, Widget child) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth * 0.06),
      child: Container(
        height: height.value,
        width: SizeConfig.screenWidth,
/*        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.green.shade100, Colors.yellow.shade100],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight),
            borderRadius: BorderRadius.all(Radius.circular(10))),*/
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                "assets/alaa-10.jpg",
              ),
              fit: BoxFit.cover,
            ),
            boxShadow: shadow),
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Align(
              alignment: Alignment(-1, 0),
              child: Text(
                category.name,
                style: TextStyle(
                  fontSize: 22,
                  color: Colors.grey.shade800,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'ALCHEVROLA',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: _buildAnimation,
      animation: controller,
    );
  }
}

class StaggeredCardCard extends StatefulWidget {
  final Color color;

  final Category category;

  const StaggeredCardCard({
    Key key,
    this.category,
    this.color,
  }) : super(key: key);

  @override
  _StaggeredCardCardState createState() => _StaggeredCardCardState();
}

class _StaggeredCardCardState extends State<StaggeredCardCard>
    with TickerProviderStateMixin {
  AnimationController _controller;
  bool isActive = true;
  CategoriesApi c = new CategoriesApi();

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(
        milliseconds: 300,
      ),
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      //TODO move api call to the next page using future builder
      onTap: () async {
        // Category cat = await c.getCategory(widget.category.id, "", "", 15);

        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => ProductsListView(
                // category: cat,
                list: true,
                sortRules: new SortRules(),
                categoryName: widget.category.name,
                categoryId: widget.category.id),
          ),
        );
        if (isActive) {
          isActive = !isActive;
          //_reverseAnimation();
        } else {
          isActive = !isActive;
          // _playAnimation();
        }
      },
      child: CategoryCard(
        controller: _controller.view,
        category: widget.category,
      ),
    );
  }
}
