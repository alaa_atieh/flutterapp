import 'dart:convert';

import 'package:australia_garden/core/api_service.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/home/home_screen.dart';
import 'package:australia_garden/screens/profile/edit_profile/edit_profile.dart';
import 'package:australia_garden/screens/profile/settings/settings_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../order/orders.dart';
import 'profile_menu.dart';
import 'package:australia_garden/screens/address/address_list.dart';

class Body extends StatefulWidget {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Body(this._scaffoldKey);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.screenHeight * 0.06),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: SizeConfig.screenWidth * 0.08,
              bottom: SizeConfig.screenWidth * 0.03,
            ),
            child: SvgPicture.asset(
              'assets/australia_garden_logo.svg',
              height: SizeConfig.screenWidth * 0.2,
              width: SizeConfig.screenWidth * 0.2,
            ),
          ),
          SizedBox(height: 20),
          ProfileMenu(
            text: "My Account",
            icon: "assets/icons/User Icon.svg",
            press: () => {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => (EditProfileScreen())))
            },
          ),
          ProfileMenu(
            text: "My orders",
            icon: "assets/icons/Cart Icon.svg",
            press: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => (OrderList())));
            },
          ),
          ProfileMenu(
            text: "Settings",
            icon: "assets/icons/Settings.svg",
            press: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => (SettingsScreen()),
                ),
              );
            },
          ),
          ProfileMenu(
            text: "Addresses",
            icon: "assets/icons/location.svg",
            press: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => (AddressList()),
                ),
              );
            },
          ),
          _sharedPreferencesManager.getBool('is_logged_in')
              ? ProfileMenu(
                  text: "Log Out",
                  icon: "assets/icons/Log out.svg",
                  press: () async {
                    ApiService a = new ApiService();
                    var response = await a.logOut();
                    var decoded = await json.decode(response.body);
                    if (decoded['success'] != null) {
                      setState(() {});
                      widget._scaffoldKey.currentState.showSnackBar(
                        new SnackBar(
                          behavior: SnackBarBehavior.floating,
                          content: Container(
                            height: 30,
                            child: new Text(
                              "You have been logged out",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w300),
                            ),
                          ),
                        ),
                      );
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) => HomeScreen(),
                        ),
                      );
                    }
                  },
                )
              : FittedBox(),
        ],
      ),
    );
  }
}
