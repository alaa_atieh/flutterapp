import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'package:flutter/material.dart';

class TextFieldOptionInput extends StatefulWidget {
  /* The name of the option */
  final String optionName;
  /* A function that will be executed whenever one of the check boxes is selected */
  final Callback onOptionSubmitted;
  /* The ID for the product option  */
  final String optionId;
  /* The selected option name */
  final String optionType;

  TextFieldOptionInput({
    @required this.optionName,
    @required this.onOptionSubmitted,
    @required this.optionId,
    @required this.optionType,
  });
  @override
  _TextFieldOptionInputState createState() => _TextFieldOptionInputState();
}

class _TextFieldOptionInputState extends State<TextFieldOptionInput> {
  final _optionTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextFormField(
            controller: _optionTextController,
            maxLines: null, //widget.optionType == 'textarea' ? 5 : 1,
            onChanged: (value) => widget.onOptionSubmitted(
                widget.optionId, _optionTextController.text),
            onFieldSubmitted: (value) {
              widget.onOptionSubmitted(
                  widget.optionId, _optionTextController.text);
            },
            //textAlign: TextAlign.center,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              alignLabelWithHint: true,
              errorStyle: TextStyle(
                fontSize: 12,
                color: Colors.red,
              ),
              border: OutlineInputBorder(
                borderRadius: new BorderRadius.circular(5.0),
                borderSide: new BorderSide(),
              ),
              // contentPadding: EdgeInsets.zero,
              hintText: widget.optionName,
            ),
            style: TextStyle(fontSize: 12, color: Colors.grey[600]),
          ),
        ],
      ),
    );
  }
}
