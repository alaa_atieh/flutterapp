import 'package:australia_garden/screens/product/domin/entities/product_attribute.dart';

import 'package:meta/meta.dart';

class AttributeGroupModel extends AttributeGroup {
  AttributeGroupModel({
    @required String attributeGroupId,
    @required String attributeGroupName,
    @required List<AttributeModel> attributes,
  }) : super(
            attributeGroupId: attributeGroupId,
            attributeGroupName: attributeGroupName,
            attributes: attributes);

  factory AttributeGroupModel.fromJson(var json) {
    var attributeList = json['attribute'] as List;
    return AttributeGroupModel(
      attributeGroupId: json['attribute_group_id'],
      attributeGroupName: json['name'],
      attributes: attributeList
          .map(
            (attributeModel) => AttributeModel.fromJson(attributeModel),
          )
          .toList(),
    );
  }
}

class AttributeModel extends Attribute {
  AttributeModel({
    @required String attributeId,
    @required String attributeName,
    @required String attributeText,
  }) : super(
            attributeId: attributeId,
            attributeName: attributeName,
            attributeText: attributeText);

  factory AttributeModel.fromJson(var json) {
    return AttributeModel(
      attributeId: json['attribute_id'],
      attributeName: json['name'],
      attributeText: json['text'],
    );
  }
}
