class Zone {
  String id;
  String name;

  Zone({this.id, this.name});

  Zone.fromJson(Map<String, dynamic> json) {
    id = json['zone_id'];
    name = json['name'];
  }
}
