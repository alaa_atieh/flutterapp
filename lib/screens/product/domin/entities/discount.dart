import 'package:meta/meta.dart';

class Discount {
  String quantity;
  String price;

  Discount({@required this.quantity, @required this.price});
}
