import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/product/data/models/related_product_model.dart';
import 'package:australia_garden/screens/product/data/models/temp_product_model.dart';
import 'package:australia_garden/screens/product/domin/entities/temp_product.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

abstract class ProductRequests {
  Future<Product> getProductDetails(int productId);
}

class ProductRequestsImpl implements ProductRequests {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  @override
  Future<Product> getProductDetails(int productId) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/product',
                  'api_token': apiToken,
                  'product_id': productId.toString(),
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        Map data = json.decode(response.body);

        return TempProductModel.fromJson(data);
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getProductDetails(productId);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load Product details');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<bool> addProductToFavourites(String productId) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/wishlist/add',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {"product_id": productId},
      ).timeout(
        Duration(seconds: 10),
      );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        final data = json.decode(response.body);

        if (data['success'] != null)
          return true;
        else
          return false;
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await addProductToFavourites(productId);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Add to Favorites');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<bool> removeProductFromFavourites(var productId) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/wishlist/delete/',
                  'product_id': productId.toString(),
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        final data = json.decode(response.body);

        if (data['success'] != null)
          return true;
        else
          return false;
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await removeProductFromFavourites(productId);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Remove From Favourites');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<List<RelatedProductModel>> getFavourites() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/wishlist',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        List products = decoded['products'];
        List<RelatedProductModel> favProducts =
            products.map((e) => RelatedProductModel.fromJson(e)).toList();
        return favProducts;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getFavourites();
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }
}
