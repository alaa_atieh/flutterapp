import 'package:flutter/material.dart';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'edit_form.dart';

class Body extends StatelessWidget {
  GlobalKey<ScaffoldState> _scaffoldKey;
  Body(this._scaffoldKey);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.04), // 4%
                Text("Edit Profile", style: headingStyle),
                Text(
                  "edit your profile with new information",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                EditForm(_scaffoldKey),
                SizedBox(height: SizeConfig.screenHeight * 0.08),

                SizedBox(height: getProportionateScreenHeight(20)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
