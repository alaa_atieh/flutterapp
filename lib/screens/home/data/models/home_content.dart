import 'package:australia_garden/screens/home/module.dart';

class HomeContent {
  final List<Module> modules;
  final List<dynamic> sliderImages;

  HomeContent({
    this.modules,
    this.sliderImages,
  });

  factory HomeContent.fromJson(var json) {
    var modulesList = json['modules'] as List;
    var sliderImagesList = json['slides'] as List;
    return HomeContent(
      modules: modulesList.map((module) => Module.fromJson(module)).toList(),
      sliderImages: sliderImagesList,
    );
  }
}
