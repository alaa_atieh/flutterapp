import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:australia_garden/core/api_service.dart';
import 'package:australia_garden/core/components/custom_surfix_icon.dart';
import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/components/form_error.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/auth/confirm_otp_page.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  final _formKey = GlobalKey<FormState>();
  ApiService api = new ApiService();
  String pre;

  GlobalKey prefixKey = GlobalKey();
  double prefixWidth = 0;
  TextEditingController email = TextEditingController(text: '');
  TextEditingController password = TextEditingController(text: '');
  TextEditingController telephone = TextEditingController(text: '');
  TextEditingController firstname = TextEditingController(text: '');
  TextEditingController lastname = TextEditingController(text: '');
  TextEditingController cmfPassword = TextEditingController(text: '');
  String telephoneValidator = '';
  String emailValidator = '';
  String firstNameValidator = '';
  String lastNameValidator = '';
  String passwordValidator = '';
  String confirmPasswordValidator = '';
  bool remember = false;
  bool agree = true;
  bool _isLoading;

  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  void initState() {
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Input(
              controller: firstname,
              hintText: ' First name',
              icon: "assets/icons/User.svg",
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }

                if (firstNameValidator != '') {
                  return firstNameValidator;
                }
                return null;
              },
              secure: false),

          // buildEmailFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          Input(
            controller: lastname,
            hintText: 'Last name',
            icon: "assets/icons/User.svg",
            keyboardType: TextInputType.text,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }

              if (lastNameValidator != '') {
                return lastNameValidator;
              }
              return null;
            },
            secure: false,
          ),

          //buildPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),

          Row(children: <Widget>[
            /* prefix(prefixKey, (value) {
              setState(() {
                pre = value;
              });
            }),*/
            Flexible(
              child: TextFormField(
                controller: telephone,

                style: TextStyle(fontSize: 16.0),
                keyboardType: TextInputType.phone,
                // inputFormatters: <TextInputFormatter>[ FilteringTextInputFormatter.allow('123456789/+')],

                decoration: InputDecoration(
                  hintText: ' Telephone',
                  prefixIcon: prefix(prefixKey, (value) {
                    setState(() {
                      pre = value;
                    });
                  }),
                  // errorMaxLines: 2,

                  suffixIcon:
                      CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg"),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  if (int.tryParse(value) == null) {
                    return 'Input needs to be digits only';
                  }
                  if (telephoneValidator != '') {
                    return telephoneValidator;
                  }
                  return null;
                },
              ),
            ),
          ]),
          SizedBox(height: getProportionateScreenHeight(30)),
          Input(
            controller: email,
            hintText: 'Email',
            icon: "assets/icons/Mail.svg",
            keyboardType: TextInputType.emailAddress,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }

              if (emailValidator != '') {
                return emailValidator;
              }
              return null;
            },
            secure: false,
          ),

          //buildConformPassFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          Input(
              controller: password,
              hintText: 'Password',
              icon: "assets/icons/Lock.svg",
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }
                if (passwordValidator != '') {
                  return passwordValidator;
                }
                return null;
              },
              secure: true),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          Input(
              controller: cmfPassword,
              hintText: 'Password confirmation',
              icon: "assets/icons/Lock.svg",
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }

                if (confirmPasswordValidator != '') {
                  return confirmPasswordValidator;
                }
                return null;
              },
              secure: true),

          SizedBox(height: getProportionateScreenHeight(20)),

          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                        text: TextSpan(children: <TextSpan>[
                      TextSpan(
                        text: 'By continuing you agree to our ',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontSize: 13,
                        ),
                      ),
                      TextSpan(
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              String terms = await api.getTermsAndConditions();
                              showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0))),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        SizeConfig.screenHeight * 0.02),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text(
                                          "Terms and conditions",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Html(
                                          data: terms,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          text: 'terms and conditions',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                          )),
                    ])),
                  )
                ]),
          ),
          SizedBox(height: getProportionateScreenHeight(20)),
          !_isLoading
              ? Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: DefaultButton(
                    text: "Continue",
                    press: () async {
                      if (_formKey.currentState.validate()) {
                        setState(() {
                          _isLoading = true;
                        });
                        var response = await api.signup(
                          firstname.text,
                          lastname.text,
                          email.text,
                          password.text,
                          cmfPassword.text,
                          pre + telephone.text,
                          agree,
                        );
                        setState(() {
                          _isLoading = false;
                        });
                        if (response != null) {
                          var decoded = json.decode(response.body);
                          if (decoded['error'] != null) {
                            setState(
                              () {
                                this.emailValidator = decoded['error']['email'];
                                this.firstNameValidator =
                                    decoded['error']['firstname'];
                                this.telephoneValidator =
                                    decoded['error']['telephone'];
                                this.lastNameValidator =
                                    decoded['error']['lastname'];
                                this.passwordValidator =
                                    decoded['error']['password'];
                                this.confirmPasswordValidator =
                                    decoded['error']['cpassword'];
                              },
                            );
                          }
                          final formv = _formKey.currentState;
                          if (formv.validate()) {
                            _sharedPreferencesManager.putString(
                              'firstname',
                              firstname.text,
                            );
                            _sharedPreferencesManager.putString(
                              'lastname',
                              lastname.text,
                            );
                            _sharedPreferencesManager.putString(
                              'email',
                              email.text,
                            );
                            _sharedPreferencesManager.putString(
                              'telephone',
                              telephone.text,
                            );
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (_) => ConfirmOtpPage(),
                              ),
                            );
                          } else {
                            this.emailValidator = '';
                            this.firstNameValidator = '';
                            this.telephoneValidator = '';
                            this.lastNameValidator = '';
                            this.passwordValidator = '';
                            this.confirmPasswordValidator = '';
                          }
                        }
                        // _formKey.currentState.save();
                        // if all are valid then go to success screen
                        // Navigator.of(context).push(MaterialPageRoute(builder:(_)=>ConfirmOtpPage()));

                      }
                    },
                  ),
                )
              : Center(
                  child: CircularProgressIndicator(),
                ),
        ],
      ),
    );
  }
}

class Input extends StatefulWidget {
  Input(
      {Key key,
      @required this.controller,
      @required this.hintText,
      @required this.validator,
      this.secure,
      this.icon,
      this.keyboardType})
      : super(key: key);

  TextEditingController controller;
  final String hintText;
  Function validator;
  final bool secure;
  final String icon;
  TextInputType keyboardType;

  @override
  _InputState createState() => _InputState();
}

class _InputState extends State<Input> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        hintText: widget.hintText,
        errorMaxLines: 2,
        suffixIcon: CustomSurffixIcon(svgIcon: widget.icon),
      ),
      controller: widget.controller,
      obscureText: widget.secure,
      style: TextStyle(fontSize: 16.0),
      validator: widget.validator,
      keyboardType: widget.keyboardType,
    );
  }
}

Widget prefix(GlobalKey prefixKey, changePrefix(value)) {
  return Container(
    height: 60,
    key: prefixKey,
    decoration: BoxDecoration(
      // border: Border.all(color: kTextColor),
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(28), bottomLeft: Radius.circular(28)),
    ),
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: CountryCodePicker(
        initialSelection: 'AU',
        onChanged: (value) {
          changePrefix(value.dialCode);
        },
        favorite: ['+61', 'AU'],
      ),
    ),
  );
}
