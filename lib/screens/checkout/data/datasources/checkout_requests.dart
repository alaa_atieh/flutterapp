import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

abstract class CheckoutRequests {
  Future<bool> addOrder();
}

class CheckoutRequestsImpl implements CheckoutRequests {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  @override
  Future<bool> addOrder() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');

    try {
      final response = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/order/add',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (response.statusCode == 200) {
        final _data = json.decode(response.body);
        String _tempSuccess = _data['success']?.toString() ?? '';
        String _tempError = _data['error']?.toString() ?? '';
        if (_tempSuccess.isNotEmpty && _tempError.isEmpty) {
          return true;
        } else {
          return false;
        }
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await addOrder();
      } else {
        throw Exception('Failed to Place Order');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
      return null;
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      return false;
    }
  }
}
