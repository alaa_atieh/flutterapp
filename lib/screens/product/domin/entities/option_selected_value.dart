import 'package:flutter/material.dart';

class OptionSelectedValue {
  String selectedProductOptionId;
  bool isRequired;
  OptionSelectedValue({
    @required this.selectedProductOptionId,
    @required this.isRequired,
  });

/*   @override
  String toString() {
    return 'selected Product Option ID : $selectedProductOptionId , required $isRequired';
  } */
}
