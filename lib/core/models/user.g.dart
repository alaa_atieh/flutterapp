// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    firstName: json['firstname'] as String,
    lastName: json['lastname'] as String,
    email: json['email'] as String,
    telephone: json['telephone'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'firstname': instance.firstName,
      'lastname': instance.lastName,
      'email': instance.email,
      'telephone': instance.telephone,
    };
