import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/product/domin/entities/discount.dart';
import 'package:australia_garden/screens/product/domin/entities/product_attribute.dart';
import 'package:australia_garden/screens/product/domin/entities/temp_product.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:australia_garden/core/theme.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/app_properties.dart';

class ProductDescription extends StatelessWidget {
  final Product product;

  const ProductDescription({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(20),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: ProductDiscounts(
              discounts: product.discounts,
            ),
          ),
          ProductInfo(
            infoName: 'Manufacturer : ',
            info: product.manufacturer != null ? product.manufacturer : 'N/A',
          ),
          ProductInfo(
            infoName: 'Model : ',
            info: product.model != null ? product.model : 'N/A',
          ),
          product.points.toInt() != 0
              ? ProductInfo(
                  infoName: 'Reward : ',
                  info: product.reward.toString(),
                )
              : SizedBox.shrink(),
          product.points.toInt() != 0
              ? ProductInfo(
                  infoName: 'Points : ',
                  info: product.points.toString(),
                )
              : SizedBox.shrink(),
          ProductInfo(
            infoName: 'Availability : ',
            info: product.stock != null ? product.stock : 'N/A',
          ),
          ProductInfo(
            infoName: 'Tax : ',
            info: product.tax != null ? product.tax : 'N/A',
          ),
          product.special != null && product.special.isNotEmpty
              ? ProductInfo(
                  infoName: 'Special Offer : ',
                  info: product.special,
                )
              : SizedBox.shrink(),
          product.productPercent != 0
              ? ProductInfo(
                  infoName: 'Discount Percentage : ',
                  info: product.productPercent.toString(),
                )
              : SizedBox.shrink(),
          product.attributeGroups != null && product.attributeGroups.isNotEmpty
              ? ExpansionTile(
                  title: Text(
                    'Specification',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  children: product.attributeGroups
                      .map(
                        (e) => ProductAttribute(
                          attributeGroup: e,
                        ),
                      )
                      .toList(),
                )
              : SizedBox(),
          product.description != null
              ? ExpansionTile(
                  title: Text(
                    'Description',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  children: [
                    Html(
                      data: product.description,
                    )
                  ],
                )
              : SizedBox(),
        ],
      ),
    );
  }
}

class ProductInfo extends StatelessWidget {
  const ProductInfo({
    Key key,
    this.infoName,
    @required this.info,
  }) : super(key: key);

  final String info;
  final infoName;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child:
          /*Text(
        infoName != null ? infoName + info : info,
        style:TextStyle(
          fontSize: 14,

        ),
      ),*/
          RichText(
              text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: infoName != null ? infoName : "",
            style: TextStyle(
                color: Colors.black,
                //AppColors.lightGray,
                fontWeight: FontWeight.w700,
                fontSize: 14,
                fontFamily: 'Metropolis'),
          ),
          TextSpan(
              text: info,
              style: TextStyle(
                  color: AppColors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Metropolis')),
        ],
      )),
    );
  }
}

class ProductDiscounts extends StatelessWidget {
  final List<Discount> discounts;

  const ProductDiscounts({Key key, this.discounts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return discounts.isNotEmpty
        ? Container(
            //color: Colors.red.shade800,
            decoration: BoxDecoration(
              border: Border.all(color: kPrimaryColor),
              color: Colors.grey.shade50,
              boxShadow: shadow3,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(bottom: SizeConfig.screenWidth * 0.02),
                    child: Text(
                      'Special offers : ',
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: discounts.length,
                    itemBuilder: (context, index) => Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              right: 20, left: 15, top: 5, bottom: 4),
                          child: Text(
                            'Buy',
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              right: SizeConfig.screenWidth * 0.045),
                          child: Text(
                            discounts[index].quantity,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                        ),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  right: SizeConfig.screenWidth * 0.045),
                              child: Text(
                                "for",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: SizeConfig.screenWidth * 0.045),
                              child: Text(
                                discounts[index].price,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        : SizedBox.shrink();
  }
}

class ProductAttribute extends StatelessWidget {
  final AttributeGroup attributeGroup;

  const ProductAttribute({Key key, this.attributeGroup}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 2, /* horizontal: 50 */
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: SizeConfig.screenWidth * 0.047),
            child: Text(
              attributeGroup.attributeGroupName,
              style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey.shade800,
                  fontWeight: FontWeight.w500),
            ),
          ),
          LayoutBuilder(
            builder: (context, constraints) => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: attributeGroup.attributes
                  .map(
                    (attribute) => Padding(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      child: Row(
                        children: [
                          Container(
                            width: constraints.maxWidth / 2,
                            child: Center(
                              child: Text(
                                attribute.attributeName,
                                style: TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: constraints.maxWidth / 2,
                            child: Center(
                              child: Text(
                                attribute.attributeText,
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildDiscountPrice(Product product) {
    return Text(product.special.toString(),
        style: TextStyle(
            fontSize: 18,
            fontFamily: 'Metropolis',
            fontWeight: FontWeight.w400,
            color: Colors.red));
  }

  Widget buildPrice(Product product) {
    return Row(children: <Widget>[
      Text(
        product.price != null ? product.price : '',
        style: TextStyle(
          color: AppColors.black,
          fontSize: 18,
          fontFamily: 'Metropolis',
          fontWeight: FontWeight.w400,
          decoration: product.productPercent > 0
              ? TextDecoration.lineThrough
              : TextDecoration.none,
        ),
      ),
      SizedBox(
        width: 4.0,
      ),
      product.productPercent > 0 ? buildDiscountPrice(product) : Container(),
    ]);
  }
}
