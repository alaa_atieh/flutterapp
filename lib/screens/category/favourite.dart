import 'package:australia_garden/core/theme.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../product/data/datasources/product_requests.dart';
import '../product/data/models/related_product_model.dart';

class Favourite extends StatefulWidget {
  @override
  _FavouriteState createState() => _FavouriteState();
  RelatedProductModel product;

  Favourite({this.product});
}

class _FavouriteState extends State<Favourite> {
  @override
  Widget build(BuildContext context) {
    return _getFavoritesButton(widget.product);
  }

  Widget _getFavoritesButton(RelatedProductModel product) {
    return FloatingActionButton(
      heroTag: product.productId,
      mini: true,
      backgroundColor: Colors.white,

      onPressed: () async {
        ProductRequestsImpl p = new ProductRequestsImpl();
        if (!product.isFavorite) {
          bool result =
              await p.addProductToFavourites(product.productId.toString());
          if (result)
            setState(() {
              product.isFavorite = true;
            });
          else {
            Fluttertoast.showToast(
                msg: 'couldn\'t add product to favorites ',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                backgroundColor: Colors.black54,
                textColor: Colors.white);
          }
        } else {
          bool result = await p.removeProductFromFavourites(
            product.productId.toString(),
          );
          if (result) {
            setState(() {
              product.isFavorite = false;
            });
          } else {
            Fluttertoast.showToast(
                msg: 'couldn\'t remove product from favorites ',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                backgroundColor: Colors.black54,
                textColor: Colors.white);
          }
        }
      },
      child: product.isFavorite
          ? Icon(
              FontAwesomeIcons.solidHeart,
              color: AppColors.red,
              size: 18.0,
            )
          : Icon(
              FontAwesomeIcons.heart,
              color: AppColors.red,
              size: 18.0,
            ),
    );
  }
}
