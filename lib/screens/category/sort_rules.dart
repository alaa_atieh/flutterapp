class SortRules {
  final SortOrder sortOrder;
  final SortType sortType;

  const SortRules(
      {this.sortOrder = SortOrder.FromHighestToLowest,
      this.sortType = SortType.Default});

  String get orderToApi =>
      sortOrder == SortOrder.FromLowestToHighest ? 'ASC' : 'DESC';

  static List<SortRules> get options => SortType.values.fold(
        [],
        (list, sortType) => list
          ..addAll(
            [
              SortRules(
                  sortType: sortType, sortOrder: SortOrder.FromHighestToLowest),
              SortRules(
                  sortType: sortType, sortOrder: SortOrder.FromLowestToHighest),
            ],
          ),
      );

  String getSortTitle() {
    switch (sortType) {
      case SortType.Default:
        if (sortOrder == SortOrder.FromHighestToLowest) {
          return 'Newest first';
        } else {
          return 'Oldest first';
        }
        break;
      case SortType.Reviewed:
        if (sortOrder == SortOrder.FromHighestToLowest) {
          return 'Reviewed first';
        } else {
          return 'Unreviewed first';
        }
        break;
      case SortType.Price:
        if (sortOrder == SortOrder.FromHighestToLowest) {
          return 'Expensive first';
        } else {
          return 'Cheapest first';
        }
        break;

      case SortType.Name:
        if (sortOrder == SortOrder.FromHighestToLowest) {
          return 'Z to A';
        } else {
          return 'A to Z';
        }
        break;
      default:
        return 'No order';
    }
  }

  String typeToApi() {
    switch (sortType) {
      case SortType.Default:
        return 'p.sort_order';

        break;
      case SortType.Reviewed:
        return 'rating';
        break;
      case SortType.Price:
        return 'p.price';

        break;
      case SortType.Model:
        return 'p.model';

        break;
      case SortType.Name:
        return 'pd.name';

        break;
      default:
        return 'No order';
    }
  }

  /*String orderToApi() {
    switch (sortOrder) {
      case SortOrder.FromHighestToLowest:

        return 'Desc';
        break;
      case SortOrder.FromLowestToHighest:
        return 'Asc';
        break;
    }
  }*/
  Map<SortType, String> get sortTextVariants => {
        SortType.Default: 'Date added',
        SortType.Name: 'Name',
        SortType.Reviewed: 'Reviews',
        SortType.Price: 'Price',
        SortType.Model: 'Model'
      };

  SortRules copyWithChangedOrder() {
    return SortRules(
        sortType: sortType,
        sortOrder: sortOrder == SortOrder.FromHighestToLowest
            ? SortOrder.FromLowestToHighest
            : SortOrder.FromHighestToLowest);
  }

  @override
  String toString() => 'SortRules(sortOrder: $sortOrder, sortType: $sortType)';
}

enum SortType { Default, Reviewed, Price, Model, Name }
enum SortOrder { FromLowestToHighest, FromHighestToLowest }
