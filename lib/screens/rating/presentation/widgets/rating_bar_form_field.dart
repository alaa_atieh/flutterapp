import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RatingBarFormField extends FormField<int> {
  RatingBarFormField(
      {FormFieldSetter<int> onSaved,
      FormFieldValidator<int> validator,
      int initialValue = 0,
      bool autovalidate = false})
      : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<int> state) {
              return Column(
                children: [
                  FlutterRatingBar(
                    itemSize: 32,
                    allowHalfRating: false,
                    initialRating: 0,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    fullRatingWidget:
                        Icon(Icons.favorite, color: Colors.red, size: 32),
                    noRatingWidget: Icon(Icons.favorite_border,
                        color: Colors.red, size: 32),
                    onRatingUpdate: (value) {
                      state.didChange(value.toInt());
                    },
                  ),
                  state.hasError
                      ? Text(
                          state.errorText,
                          style: TextStyle(color: Colors.red, fontSize: 12),
                        )
                      : Container()
                ],
              );
            });
}
