import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import 'package:australia_garden/core/models/user.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';

import 'constants.dart';
import 'dependency_injection/injector.dart';

class ApiService {
  final SessionRequest _sessionRequest = locator<SessionRequest>();
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  Future<Map> forgetPassword(String email) async {
    String apiToken = _sharedPreferencesManager.getString('api_token');

    try {
      var _response = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/forgotten',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'email': email,
        },
      ).timeout(
        Duration(seconds: 10),
      );

      if (_response.statusCode == 200) {
        final Map _result = json.decode(_response.body);
        return _result;
      } else if (_response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await forgetPassword(email);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Send Reset E-mail');
      }
    } on SocketException catch (e) {
      print(e.toString());
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
      throw e.message;
    } on TimeoutException catch (e) {
      print(e.toString());
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
      throw e.message;
    } on Exception catch (e) {
      print(e.toString());
      throw e.toString();
    }
  }

  Future<dynamic> logUser() async {
    try {
      var r = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/login',
            },
          ).toString(),
        ),
        body: {
          "key": apiKey,
          "username": 'Default',
        },
      ).timeout(
        Duration(seconds: 10),
      );
      if (r.statusCode == 200) {
        var decoded = await json.decode(r.body);
        _sharedPreferencesManager.putString('language', decoded['language']);
        _sharedPreferencesManager.putString('api_token', decoded['api_token']);

        return decoded;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Connect to server');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<http.Response> login(
    String email,
    String password,
  ) async {
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/customer/login',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          "email": email,
          "apitoken": apiToken,
          "password": password,
        },
      ).timeout(
        Duration(seconds: 10),
      );

      if (r.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return r;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(true);
        return await login(
          email,
          password,
        );
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Login');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<http.Response> signup(
    String firstName,
    String lastName,
    String email,
    String password,
    String confirmPassword,
    String telephone,
    bool agree,
  ) async {
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http
          .post(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/register',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
            body: jsonEncode(
              <String, String>{
                'firstname': firstName,
                'lastname': lastName,
                'email': email,
                'password': password,
                'confirm': confirmPassword,
                'telephone': telephone,
                'newsletter': '0',
                'agree': agree.toString(),
              },
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        return r;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await signup(
          firstName,
          lastName,
          email,
          password,
          confirmPassword,
          telephone,
          agree,
        );
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<http.Response> otp(
    String code1,
    String code2,
    String code3,
    String code4,
    String code,
  ) async {
    String apiToken = _sharedPreferencesManager.getString('api_token');
    //  final re = RetryOptions(maxAttempts: 2);
    try {
      var r = await
          //re.retry(() =>
          http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/register/confirmcode',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'code': code,
          'code1': code1,
          'code2': code2,
          'code3': code3,
          'code4': code4,
        },
      ).timeout(
        Duration(seconds: 10),
      );

      if (r.statusCode == 200) {
        return r;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await otp(
          code1,
          code2,
          code3,
          code4,
          code,
        );
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('OTP Failure');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<http.Response> resendCode() async {
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/register/send_code',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        return r;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        await resendCode();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('OTP Code Send Failure');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<http.Response> logOut() async {
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/customer/logout',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        _sharedPreferencesManager.putBool('is_logged_in', false);
        return r;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        logOut();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Logout failed');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<http.Response> changeNumber(String phone) async {
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/register/change_number',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'phone': phone,
        },
      ).timeout(
        Duration(seconds: 10),
      );

      if (r.statusCode == 200) {
        return r;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        await changeNumber(phone);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Change Number Failed');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<void> getUserInfo() async {
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/customer/edit',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        User _user = User.fromJson(
          json.decode(r.body),
        );
        _sharedPreferencesManager.putString('firstname', _user.firstName);
        _sharedPreferencesManager.putString('lastname', _user.lastName);
        _sharedPreferencesManager.putString('email', _user.email);
        _sharedPreferencesManager.putString('telephone', _user.telephone);
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        await getUserInfo();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Get User Details Failed');
      }
    } on SocketException {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  Future<String> getTermsAndConditions() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    try {
      r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/home/terms',
                  'api_token': apiToken,
                  'type': 'register',
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        if (decoded['error'] == null) {
          String terms = decoded['terms'];
          return terms;
        }
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getTermsAndConditions();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to get terms');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }
}
