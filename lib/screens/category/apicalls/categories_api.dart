import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/category/data/models/category_search.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:australia_garden/screens/category/category.dart';

class CategoriesApi {
  /* static final baseurl = 'http://192.168.1.108:8087/deluxe/'; */
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  Future<List<Category>> getCategories() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/category',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        List cat = decoded['categories'];
        List<Category> categories =
            cat.map((e) => Category.fromJson(e)).toList();
        return categories;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getCategories();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Logout failed');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<Category> getCategory(
    String path,
    String sort,
    String order,
    int limit,
  ) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      var r = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/category',
                  'api_token': apiToken,
                  'path': path,
                  'sort': sort,
                  'order': order,
                  'limit': limit.toString(),
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        var decoded = json.decode(r.body);
        Category c = Category.fromJson(decoded);
        return c;
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getCategory(path, sort, order, limit);
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<List<CategorySearch>> getCategoriesSearch() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/category/searchCategories',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        Map data = json.decode(response.body);
        List _list = data['categories'];
        return _list
            .map(
              (categorySearch) => CategorySearch.fromJson(categorySearch),
            )
            .toList();
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getCategoriesSearch();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Load Categories');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }
}
