import 'package:australia_garden/screens/checkout/shipping_method_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../core/components/default_button.dart';
import '../../../core/constants.dart';
import '../../../core/size_config.dart';
import '../address.dart';
import '../address_api.dart';
import '../country.dart';
import '../shipping_address.dart';
import '../zone.dart';

class AddressForm extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey;

  Function() onAddAddress;
  Function() onChooseCountry;
  bool existingAddress;
  Address chosenAddress;
  String type;

  AddressForm(
      {this.scaffoldKey,
      this.onAddAddress,
      this.chosenAddress,
      this.existingAddress,
      this.type});

  @override
  _AddressFormState createState() => _AddressFormState();
}

class _AddressFormState extends State<AddressForm> {
  @override
  void initState() {
    super.initState();
    getCountries(a);
  }

  final _formKey = GlobalKey<FormState>();

  AddressApi a = new AddressApi();
  bool postal = false, isDefault = false;

  TextEditingController firstName = TextEditingController(text: '');

  TextEditingController lastName = TextEditingController(text: '');

  TextEditingController city = TextEditingController(text: '');

  TextEditingController address1 = TextEditingController(text: '');

  TextEditingController address2 = TextEditingController(text: '');

  TextEditingController postalCode = TextEditingController(text: '');

  String cityValidator = '';

  String address1Validator = '';
  String postalCodeValidator = '';

  List<Country> countries = <Country>[];

  List<Zone> cities = <Zone>[];

  List<String> test = <String>[];

  String chosenCountry;

  String chosenCity;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Input(
            controller: firstName,
            hintText: "First Name",
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }

              return null;
            },
            secure: false,
            icon: Icon(
              FontAwesomeIcons.user,
              color: kPrimaryColor,
              size: 20,
            ),
          ),
          SizedBox(height: getProportionateScreenHeight(30)),
          Input(
            controller: lastName,
            hintText: "Last Name",
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }

              return null;
            },
            secure: false,
            icon: Icon(
              FontAwesomeIcons.user,
              color: kPrimaryColor,
              size: 20,
            ),
          ),
          SizedBox(height: getProportionateScreenHeight(30)),
          Input(
            controller: city,
            hintText: "City",
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }

              if (cityValidator != '') {
                return cityValidator;
              }
              return null;
            },
            secure: false,
            icon: Icon(
              FontAwesomeIcons.city,
              color: kPrimaryColor,
              size: 20,
            ),
          ),
          SizedBox(height: getProportionateScreenHeight(30)),
          Input(
            controller: address1,
            hintText: "Address 1",
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              if (address1Validator != '') return address1Validator;

              return null;
            },
            secure: false,
            icon: Icon(
              FontAwesomeIcons.mapMarkerAlt,
              color: kPrimaryColor,
              size: 20,
            ),
          ),
          SizedBox(height: getProportionateScreenHeight(30)),
          Input(
            controller: address2,
            hintText: "Address 2",
            secure: false,
            icon: Icon(
              FontAwesomeIcons.mapMarkerAlt,
              color: kPrimaryColor,
              size: 20,
            ),
          ),
          SizedBox(height: getProportionateScreenHeight(30)),
          DropdownButtonFormField(
              validator: (value) {
                if (value == null) {
                  return 'Please enter some text';
                }

                return null;
              },
              isExpanded: true,
              value: chosenCountry,
              onChanged: (value) async {
                chosenCity = null;
                chosenCountry = value;
                var tempCities = await a.getZonesByCountryId(chosenCountry);
                setState(() {
                  cities = tempCities;
                });
              },
              disabledHint: Text("Country"),
              hint: Text('Choose country'),
              items: countries != null
                  ? countries.map((val) {
                      return new DropdownMenuItem(
                        child: new Text(val.name),
                        value: val.id,
                        onTap: () {
                          setState(() {
                            postal = val.postalCode;
                          });
                        },
                      );
                    }).toList()
                  : countries),
          SizedBox(height: getProportionateScreenHeight(20)),
          DropdownButtonFormField(
            validator: (value) {
              if (value == null) {
                return 'Please enter some text';
              }

              return null;
            },
            isExpanded: true,
            value: chosenCity,
            onChanged: (value) {
              setState(() {
                chosenCity = value;
              });
            },
            disabledHint: Text("City"),
            hint: Text('Choose city'),
            items: cities.map((val) {
              return new DropdownMenuItem(
                child: new Text(val.name),
                value: val.id,
              );
            }).toList(),
          ),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          postal
              ? Input(
                  controller: postalCode,
                  hintText: "Postal Code",
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }

                    if (postalCodeValidator != '') {
                      return postalCodeValidator;
                    }
                    return null;
                  },
                  secure: false,
                  icon: Icon(
                    FontAwesomeIcons.mapPin,
                    color: kPrimaryColor,
                  ),
                )
              : SizedBox(),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          CheckboxListTile(
            activeColor: kPrimaryColor,
            dense: true,
            contentPadding: EdgeInsets.symmetric(
              horizontal: SizeConfig.screenWidth * 0.1,
            ),
            secondary: Text(
              'Make it the Default Address',
              style: TextStyle(
                  fontSize: SizeConfig.screenWidth * 0.04, color: Colors.black),
            ),
            checkColor: Colors.black,
            value: isDefault,
            onChanged: (value) => setState(
              () {
                isDefault = value;
              },
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          widget.onAddAddress != null
              ? Padding(
                  padding:
                      EdgeInsets.only(bottom: SizeConfig.screenHeight * 0.05),
                  child: Row(
                    children: [
                      Expanded(
                        child: DefaultButton(
                          text: "Cancel",
                          press: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      SizedBox(width: SizeConfig.screenWidth * 0.08),
                      Expanded(
                        child: DefaultButton(
                          text: "Add address",
                          press: () async {
                            if (_formKey.currentState.validate()) {
                              var result = await a.addAddress(
                                firstName.text,
                                lastName.text,
                                city.text,
                                address1.text,
                                address2.text,
                                "180",
                                "2846",
                                postalCode.text,
                                widget.type,
                                "1",
                                isDefault,
                              );
                              if (result['error'] != null) {
                                this.cityValidator = result['error']['city'];
                                this.address1Validator =
                                    result['error']['address_1'];
                              }
                              final formv = _formKey.currentState;
                              if (formv.validate()) {
                                widget.scaffoldKey.currentState.showSnackBar(
                                  new SnackBar(
                                    content: Container(
                                        height: 30,
                                        child: new Text(
                                          "Address added successfully",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w300),
                                        )),
                                  ),
                                );
                                widget.onAddAddress();
                                Navigator.of(context).pop();
                              } else {
                                this.cityValidator = "";
                                this.address1Validator = "";
                              }
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                        child: DefaultButton(text: "Prev", press: () async {})),
                    SizedBox(
                      width: 70,
                    ),
                    Expanded(
                      child: DefaultButton(
                        text: "Next step ",
                        press: () async {
                          if (_formKey.currentState.validate()) {
                            var result = await a.addAddress(
                              firstName.text,
                              lastName.text,
                              city.text,
                              address1.text,
                              address2.text,
                              "180",
                              "2846",
                              postalCode.text,
                              widget.type,
                              "1",
                              isDefault,
                            );
                            if (result['error'] != null) {
                              this.cityValidator = result['error']['city'];
                              this.address1Validator =
                                  result['error']['address_1'];
                            }
                            final formv = _formKey.currentState;
                            if (formv.validate()) {
                              if (widget.type == 'shippingNew') {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (_) => ShippingMethodScreen(),
                                  ),
                                );
                              } else {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (_) => ShippingAddressScreen(),
                                  ),
                                );
                              }
                            } else {
                              this.cityValidator = "";
                              this.address1Validator = '';
                            }
                          }
                        },
                      ),
                    ),
                  ],
                ),
        ],
      ),
    );
  }

  getCountries(AddressApi a) async {
    List<Country> c;
    c = await a.getCountries();
    setState(() {
      countries = c;
    });
  }
}

class Input extends StatefulWidget {
  Input(
      {Key key,
      @required this.controller,
      @required this.hintText,
      this.validator,
      this.secure,
      this.icon,
      this.keyboardType})
      : super(key: key);

  TextEditingController controller;
  final String hintText;
  Function validator;

  final bool secure;
  final Icon icon;
  TextInputType keyboardType;

  @override
  _InputState createState() => _InputState();
}

class _InputState extends State<Input> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        labelText: widget.hintText,
        errorMaxLines: 2,
        prefixIcon: widget.icon,
      ),
      controller: widget.controller,
      obscureText: widget.secure,
      style: TextStyle(fontSize: 16.0),
      validator: widget.validator,
      keyboardType: widget.keyboardType,
    );
  }
}
