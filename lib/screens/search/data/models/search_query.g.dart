// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_query.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchQueryRequest _$SearchQueryRequestFromJson(Map<String, dynamic> json) {
  return SearchQueryRequest(
    query: json['query'] as String,
    description: json['description'] as bool ?? true,
    attributes: json['attributes'] as bool ?? true,
    tag: json['tag'] as String,
    categoryId: json['category_id'] as int,
    subCategory: json['sub_category'] as int,
    sort: json['sort'] as String ?? 'p.price',
    order: json['order'] as String ?? 'DESC',
    page: json['page'] as int ?? 1,
    limit: json['limit'] as int ?? 10,
  );
}

Map<String, dynamic> _$SearchQueryRequestToJson(SearchQueryRequest instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('query', instance.query);
  writeNotNull('description', instance.description);
  writeNotNull('attributes', instance.attributes);
  writeNotNull('tag', instance.tag);
  writeNotNull('category_id', instance.categoryId);
  writeNotNull('sub_category', instance.subCategory);
  writeNotNull('sort', instance.sort);
  writeNotNull('order', instance.order);
  writeNotNull('page', instance.page);
  writeNotNull('limit', instance.limit);
  return val;
}
