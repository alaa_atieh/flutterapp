import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class RetryButton extends StatelessWidget {
  final VoidCallback action;
/*   final String text;
  final String svgPictureAssetName; */
  const RetryButton({
    Key key,
    @required this.action,
    /* @required this.text,
    @required this.svgPictureAssetName, */
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Flexible(
          flex: 3,
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: SvgPicture.asset(
              'assets/No internet-05.svg',
              fit: BoxFit.contain,
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('No Internet Connection'),
                TextButton(
                  onPressed: action,
                  child: Text('Retry'),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
