import 'package:json_annotation/json_annotation.dart';

part 'cart_item.g.dart';

@JsonSerializable(
  explicitToJson: true,
)
class CartItem {
  @JsonKey(name: 'product_id')
  final String productId;

  @JsonKey(name: 'quantity')
  final String quantity;

  @JsonKey(name: 'option')
  final List<IdValue> option;

  CartItem({
    this.productId,
    this.quantity,
    this.option,
  });

  factory CartItem.fromJson(Map<String, dynamic> json) =>
      _$CartItemFromJson(json);

  Map<String, dynamic> toJson() => _$CartItemToJson(this);

  @override
  String toString() =>
      'CartItem(productId: $productId, quantity: $quantity, option: $option)';
}

@JsonSerializable(
  explicitToJson: true,
)
class IdValue {
  @JsonKey(name: 'key')
  String key;
  @JsonKey(name: 'value')
  List<String> value;

  IdValue({
    this.key,
    this.value,
  });

  factory IdValue.fromJson(Map<String, dynamic> json) =>
      _$IdValueFromJson(json);

  Map<String, dynamic> toJson() => _$IdValueToJson(this);

  @override
  String toString() => 'IdValue(key: $key, value: $value)';
}
