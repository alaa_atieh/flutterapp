import 'package:json_annotation/json_annotation.dart';

part 'refresh_token_request_body.g.dart';

@JsonSerializable(includeIfNull: false)
class RefreshTokenRequestBody {
  String username;
  String key;
  @JsonKey(name: 'customer_id', nullable: true)
  String customerId;
  RefreshTokenRequestBody({
    this.username,
    this.key,
    this.customerId,
  });
  factory RefreshTokenRequestBody.fromJson(Map<String, dynamic> json) =>
      _$RefreshTokenRequestBodyFromJson(json);
  Map<String, dynamic> toJson() => _$RefreshTokenRequestBodyToJson(this);

  @override
  String toString() =>
      'RefreshTokenRequestBody(username: $username, key: $key, customerId: $customerId)';
}
