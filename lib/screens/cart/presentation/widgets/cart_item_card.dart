import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

import 'package:australia_garden/core/app_properties.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/cart/data/datasources/cart_requests.dart';
import 'package:australia_garden/screens/cart/domain/entities/cart_item.dart';

class CartItemsCard extends StatefulWidget {
  final CartItem cartItem;
  final Function onRemove;
  final Function(String, String) onQuantityChanged;

  CartItemsCard({
    Key key,
    this.cartItem,
    this.onRemove,
    this.onQuantityChanged,
  }) : super(key: key);

  @override
  _CartItemsCardState createState() => _CartItemsCardState();
}

class _CartItemsCardState extends State<CartItemsCard> {
  CartRequestImpl cartRequestImpl;

  String _total;
  String _quantity;

  @override
  void initState() {
    super.initState();
    cartRequestImpl = CartRequestImpl();
    _total = widget.cartItem.total;
    _quantity = widget.cartItem.quantity;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          // margin: EdgeInsets.only(top: 2),
          height: 130,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment(0, 0.8),
                child: Opacity(
                  opacity: widget.cartItem.stock ? 1 : 0.3,
                  child: Container(
                    height: 100,
                    margin: EdgeInsets.symmetric(horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: shadow,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                              top: SizeConfig.screenWidth * 0.04,
                              left: SizeConfig.screenWidth * 0.04),
                          width: 200,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              FittedBox(
                                child: Text(
                                  widget.cartItem.name,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0,
                                    color: darkGrey,
                                  ),
                                ),
                              ),
                              Container(
                                width: 160,
                                padding: const EdgeInsets.only(
                                  left: 2.0,
                                  top: 3.0,
                                  bottom: 8.0,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      _total,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: kPrimaryColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              !widget.cartItem.stock || widget.cartItem.maximum == 0
                                  ? FittedBox(
                                      child: Text(
                                        "This product is currently not available",
                                        // '${widget.cartItem.total}',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.red,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12.0,
                                        ),
                                      ),
                                    )
                                  : Container()
                            ],
                          ),
                        ),
                        widget.cartItem.maximum == 0 ||
                                widget.cartItem.maximum <
                                    widget.cartItem.minimum
                            ? SizedBox()
                            : NumberPicker.integer(
                                initialValue: widget.cartItem.stock
                                    ? int.parse(_quantity)
                                    : 1,
                                minValue: widget.cartItem.minimum,
                                maxValue: widget.cartItem.maximum ?? 1000,
                                onChanged: (value) {
                                  setState(
                                    () {
                                      _quantity = value.toString();
                                      _total = '\$' +
                                          (double.parse(
                                                    widget.cartItem.price
                                                        .replaceAll(',', '')
                                                        .substring(1),
                                                  ) *
                                                  value)
                                              .toStringAsFixed(2);
                                      widget.onQuantityChanged(
                                        widget.cartItem.cartId,
                                        value.toString(),
                                      );
                                    },
                                  );
                                },
                                itemExtent: 30,
                                selectedTextStyle: TextStyle(
                                    color: kPrimaryColor,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                                listViewWidth: 40,
                              ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                child: CartItemDisplay(
                  widget.cartItem,
                  onPressed: widget.onRemove,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class CartItemDisplay extends StatelessWidget {
  final CartItem cartItem;
  final Function onPressed;

  const CartItemDisplay(
    this.cartItem, {
    Key key,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      width: 200,
      child: Stack(
        children: <Widget>[
          Positioned(
            left: 40,
            top: 25,
            child: SizedBox(
              height: 80,
              width: 80,
              child: Opacity(
                opacity: cartItem.stock ? 1 : 0.6,
                child: Image.network(
                  '${cartItem.thumb}',
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Positioned(
            left: 2,
            top: 12,
            child: Align(
              child: IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.red,
                  size: 14,
                ),
                onPressed: onPressed,
              ),
            ),
          )
        ],
      ),
    );
  }
}
