import 'package:carousel_slider/carousel_slider.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:flutter/material.dart';

class HomeImageSlider extends StatelessWidget {
  final List<dynamic> imgList;

  const HomeImageSlider({Key key, this.imgList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: SizeConfig.screenWidth * 0.04),
      child: CarouselSlider(
        options: CarouselOptions(
          enlargeCenterPage: true,
          autoPlay: true,
        ),
        items: imgList
            .map(
              (item) => Container(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.screenWidth * 0.02),
                child: Center(
                  child: Image.network(
                    item,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
