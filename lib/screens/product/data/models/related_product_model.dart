import 'package:australia_garden/screens/product/domin/entities/related_product.dart';
import 'package:meta/meta.dart';

class RelatedProductModel extends RelatedProduct {
  RelatedProductModel(
      {@required int productId,
      @required String thumb,
      @required String name,
      @required String description,
      @required String price,
      @required String special,
      @required String tax,
      @required String minimum,
      @required int rating,
      @required int percent,
      @required bool isFavorite,
      @required bool isInCompareList,
      @required bool optionsIsRequired})
      : super(
            productId: productId,
            thumb: thumb,
            name: name,
            description: description,
            price: price,
            special: special,
            tax: tax,
            minimum: minimum,
            rating: rating,
            percent: percent,
            isFavorite: isFavorite,
            isInCompareList: isInCompareList,
            optionsIsRequired: optionsIsRequired);

  factory RelatedProductModel.fromJson(var json) {
    return RelatedProductModel(
        productId: json['product_id'],
        thumb: json['thumb'],
        name: json['name'],
        description: json['description'],
        price: json['price'],
        special: json['special'],
        tax: json['tax'],
        minimum: json['minimum'],
        rating: json['rating'],
        percent: json['percent'],
        isFavorite: json['isFavorite'],
        isInCompareList: json['isInCompareList'],
        optionsIsRequired: json['optionsIsRequired']);
  }
  String get specialMark {
    if (percent > 0) {
      return '-' + percent.round().toString() + '%';
    } /* else if (1 == 1) 
    {
      return 'New';
    } */
    else {
      return null;
    }
  }
}
