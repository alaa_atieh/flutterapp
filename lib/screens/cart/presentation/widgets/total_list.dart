import 'package:australia_garden/screens/cart/domain/entities/totals.dart';
import 'package:flutter/material.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/constants.dart';

class TotalsList extends StatelessWidget {
  final List<Totals> totals;

  const TotalsList({
    Key key,
    this.totals,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(bottom: SizeConfig.screenHeight * 0.02),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(15),
                      bottomLeft: Radius.circular(15))),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig.screenWidth * 0.04),
                child: Text(
                  'Totals',
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'ALCHEVROLA',
                    fontSize: 18.0,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: totals.length,
              itemBuilder: (context, index) => Padding(
                padding: EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                child: Card(
                  child: Padding(
                    padding: EdgeInsets.all(SizeConfig.screenWidth * 0.025),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(totals[index].title),
                        Text(totals[index].text),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
