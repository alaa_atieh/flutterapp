import 'package:australia_garden/screens/product/domin/entities/option_selected_value.dart';
import 'package:flutter/material.dart';

class OptionSelectedStringValue extends OptionSelectedValue {
  String selectedProductOptionValue;
  String value;
  OptionSelectedStringValue(
      {@required this.selectedProductOptionValue,
      @required String selectedProductOptionId,
      @required bool isRequired,
      this.value})
      : super(
          selectedProductOptionId: selectedProductOptionId,
          isRequired: isRequired,
        );
  @override
  String toString() {
    return super.toString() +
        ' And the Selected Product Option Value $selectedProductOptionValue';
  }
}
