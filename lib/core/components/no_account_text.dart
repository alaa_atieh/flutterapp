import 'package:flutter/material.dart';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';

class NoAccountText extends StatelessWidget {
  final String text;
  final String clickableText;
  final String route;
  NoAccountText({Key key, this.text, this.clickableText, this.route})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          text,
          style: TextStyle(fontSize: getProportionateScreenWidth(16)),
        ),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, route),
          child: Text(
            clickableText,
            style: TextStyle(
                fontSize: getProportionateScreenWidth(16),
                color: kPrimaryColor),
          ),
        ),
      ],
    );
  }
}
