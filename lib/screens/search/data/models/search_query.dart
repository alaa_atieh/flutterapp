import 'package:json_annotation/json_annotation.dart';

part 'search_query.g.dart';

@JsonSerializable(includeIfNull: false)
class SearchQueryRequest {
  String query;
  @JsonKey(nullable: true, defaultValue: true)
  bool description;
  @JsonKey(nullable: true, defaultValue: true)
  bool attributes;
  @JsonKey(nullable: true)
  String tag;
  @JsonKey(nullable: true, name: 'category_id')
  int categoryId;
  @JsonKey(nullable: true, name: 'sub_category')
  int subCategory;
  @JsonKey(nullable: true, defaultValue: 'p.price')
  String sort;
  @JsonKey(nullable: true, defaultValue: 'DESC')
  String order;
  @JsonKey(nullable: true, defaultValue: 1)
  int page;
  @JsonKey(nullable: true, defaultValue: 10)
  int limit;

  SearchQueryRequest({
    this.query,
    this.description,
    this.attributes,
    this.tag,
    this.categoryId,
    this.subCategory,
    this.sort,
    this.order,
    this.page,
    this.limit,
  });

  factory SearchQueryRequest.fromJson(Map<String, dynamic> json) =>
      _$SearchQueryRequestFromJson(json);
  Map<String, dynamic> toJson() => _$SearchQueryRequestToJson(this);

  @override
  String toString() {
    return 'SearchQueryRequest(query: $query, description: $description, attributes: $attributes, tag: $tag, categoryId: $categoryId, subCategory: $subCategory, sort: $sort, order: $order, page: $page, limit: $limit)';
  }
}
