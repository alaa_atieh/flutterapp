import 'dart:core';

class ProductOption {
  //int id;
  String name;
  String value;

  ProductOption(this.name, this.value);
  ProductOption.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }
}
