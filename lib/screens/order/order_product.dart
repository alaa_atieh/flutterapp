import 'dart:core';

import 'product_option.dart';
class OrderProduct{
  //int id;
  String orderProductId;
  int productId;
  String name;
  String model;
  String quantity;
  String price;
  String thumb;
  String total;
  List<ProductOption> options ;




  OrderProduct( this.productId,this.orderProductId, this.name, this.model,this.quantity,this.price,this.total,this.thumb);
  OrderProduct.fromJson(Map<String, dynamic> json) {
    orderProductId = json['order_product_id'];
    productId = json['product_id'];
    name = json['name'];
    thumb=json['thumb'];
    model = json['model'];
    quantity=json['quantity'];
    price=json['price'];
    total=json['total'];
    List opt=json['option'] as List;
   options =
    opt.map((e) => ProductOption.fromJson(e)).toList();


  }

}