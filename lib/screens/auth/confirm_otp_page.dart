import 'package:australia_garden/core/app_properties.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/auth/sign_up/components/change_number_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:australia_garden/core/api_service.dart';
import 'dart:convert';
import 'package:australia_garden/screens/home/home_screen.dart';

class ConfirmOtpPage extends StatefulWidget {
  @override
  _ConfirmOtpPageState createState() => _ConfirmOtpPageState();
}

class _ConfirmOtpPageState extends State<ConfirmOtpPage> {
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  TextEditingController otp1 = TextEditingController(text: '');
  TextEditingController otp2 = TextEditingController(text: '');
  TextEditingController otp3 = TextEditingController(text: '');
  TextEditingController otp4 = TextEditingController(text: '');
  TextEditingController otp5 = TextEditingController(text: '');
  TextEditingController pin = TextEditingController(text: '');
  bool error = false;
  double fontSize = 34;
  String wrong = "Confirm Your OTP";
  ApiService api = new ApiService();
  final widgetLists = <Widget>[];
  Color txtColor = Colors.white;
  String subtitleText = 'We\'ve sent you a code via SMS';

  Widget otpBox(TextEditingController otpController) {
    return Container(
      height: 48,
      width: 48,
      decoration: BoxDecoration(
          color: Color.fromRGBO(255, 255, 255, 0.8),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Center(
        child: SizedBox(
          width: 9,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: TextField(
              controller: otpController,
              decoration: InputDecoration(
                  border: InputBorder.none, contentPadding: EdgeInsets.zero),
              style: TextStyle(fontSize: 16.0),
              keyboardType: TextInputType.text,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget title = Text(
      wrong,
      style: TextStyle(
          color: txtColor,
          fontSize: fontSize,
          fontWeight: FontWeight.bold,
          shadows: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.15),
              offset: Offset(0, 5),
              blurRadius: 10.0,
            )
          ]),
    );

    Widget subTitle = Padding(
        padding: const EdgeInsets.only(right: 56.0),
        child: Text(
          subtitleText,
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          ),
        ));

    Widget verifyButton = Center(
      child: InkWell(
        onTap: () async {
          var response = await api.otp(
              otp1.text, otp2.text, otp3.text, otp4.text, pin.text);
          if (response.statusCode == 200) {
            var decoded = json.decode(response.body);
            if (decoded['error'] != null) {
              setState(() {
                wrong = "Wrong verification code";
                fontSize = 24.0;
                txtColor = Colors.red;
                error = true;
              });
            } else {
              _sharedPreferencesManager.putBool(
                'is_logged_in',
                true,
              );
              Navigator.of(context).push(
                MaterialPageRoute(builder: (_) => HomeScreen()),
              );
            }
          }

          //  Navigator.of(context)
          //    .push(MaterialPageRoute(builder: (_) => IntroPage()));
        },
        child: Container(
          width: SizeConfig.screenWidth / 2,
          height: 80,
          child: Center(
            child: new Text(
              "Verify",
              style: const TextStyle(
                color: const Color(0xfffefefe),
                fontWeight: FontWeight.w600,
                fontStyle: FontStyle.normal,
                fontSize: 20.0,
              ),
            ),
          ),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(236, 60, 3, 1),
                    Color.fromRGBO(234, 60, 3, 1),
                    Color.fromRGBO(216, 78, 16, 1),
                  ],
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.16),
                  offset: Offset(0, 5),
                  blurRadius: 10.0,
                )
              ],
              borderRadius: BorderRadius.circular(9.0)),
        ),
      ),
    );

    /*   Widget otpCode = Container(
      padding: const EdgeInsets.only(right: 28.0),
      height: 190,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          otpBox(otp1),
          otpBox(otp2),
          otpBox(otp3),
          otpBox(otp4),
          otpBox(otp5)
        ],
      ),
    );*/

    Widget resendText = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Didn\'t receive code ? ',
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Color.fromRGBO(255, 255, 255, 0.8),
            fontSize: 14.0,
          ),
        ),
        InkWell(
          onTap: () async {
            var response = await api.resendCode();
            if (response.statusCode == 200) {
              var decoded = json.decode(response.body);
              if (decoded['error'] != null) {
                setState(() {
                  subtitleText = decoded['error'];
                });
              } else {
                setState(() {
                  subtitleText = decoded['success'];
                });
              }
            }
          },
          child: Text(
            ' resend again ',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
        ),
      ],
    );
    Widget changeNumber = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Not your number ? ',
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Color.fromRGBO(255, 255, 255, 0.8),
            fontSize: 14.0,
          ),
        ),
        InkWell(
          onTap: () async {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ChangeNumberPage(),
              ),
            );
          },
          child: Text(
            ' change number ',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
        ),
      ],
    );

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/background.jpg'), fit: BoxFit.cover)),
        child: Container(
          decoration: BoxDecoration(color: transparentYellow),
          child: Scaffold(
            backgroundColor: Colors.orange,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0.0,
            ),
            body: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 28.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Spacer(flex: 3),
                      title,
                      Spacer(),
                      subTitle,
                      Spacer(flex: 1),
                      Padding(
                        padding: const EdgeInsets.only(right: 28.0),
                        child: Center(
                          child: PinCodeTextField(
                            controller: pin,
                            highlightColor: Colors.white,
                            highlightAnimation: true,
                            highlightAnimationBeginColor: Colors.white,
                            highlightAnimationEndColor:
                                Theme.of(context).primaryColor,
                            pinTextAnimatedSwitcherDuration:
                                Duration(milliseconds: 500),
                            wrapAlignment: WrapAlignment.center,
                            hasTextBorderColor: Colors.transparent,
                            highlightPinBoxColor: Colors.white,
                            hasError: error,
                            errorBorderColor: Colors.red,
                            autofocus: true,
                            pinBoxHeight: 60,
                            pinBoxWidth: 60,
                            pinBoxRadius: 5,
                            defaultBorderColor: Colors.transparent,
                            pinBoxColor: Color.fromRGBO(255, 255, 255, 0.8),
                            maxLength: 4,
                            onDone: (text) => text,
                            onTextChanged: (text) => text,
                            keyboardType: TextInputType.text,
                          ),
                        ),
                      ),
                      Spacer(flex: 1),
//                      otpCode,

                      /* Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Text(
                           wrong,
                          style: TextStyle(
                            color: Colors.red,

                          ),
                        ),
                      ),*/
                      Padding(
                        padding: const EdgeInsets.only(right: 28.0),
                        child: verifyButton,
                      ),
                      Spacer(flex: 2),
                      changeNumber,
                      resendText,
                      Spacer()
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
