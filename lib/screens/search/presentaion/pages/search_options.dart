import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/category/apicalls/categories_api.dart';
import 'package:australia_garden/screens/category/data/models/category_search.dart';
import 'package:australia_garden/screens/search/data/models/search_query.dart';
import 'package:flutter/material.dart';

class SearchOptions extends StatefulWidget {
  final SearchQueryRequest searchQueryRequest;
  final Function(SearchQueryRequest) changeSearchRequestOptions;

  const SearchOptions({
    Key key,
    this.searchQueryRequest,
    this.changeSearchRequestOptions,
  }) : super(key: key);

  @override
  _SearchOptionsState createState() => _SearchOptionsState();
}

class _SearchOptionsState extends State<SearchOptions> {
  String _selectedCategoryId;
  final CategoriesApi categoriesApi = CategoriesApi();

  List<CategorySearch> _categoriesList;

  Future<List<CategorySearch>> getCategories() async {
    if (_categoriesList?.isEmpty ?? true) {
      _categoriesList = await categoriesApi.getCategoriesSearch();
      //* Added this to the category list to search in all categories
      _categoriesList.insert(
        0,
        CategorySearch(categoryId: -1, name: 'All Categories'),
      );
    }
    return _categoriesList;
  }

  @override
  void initState() {
    _categoriesList = <CategorySearch>[];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListView(
        shrinkWrap: true,
        // physics: NeverScrollableScrollPhysics(),
        children: [
          CheckboxListTile(
            title: Text('Search In Products Description'),
            value: widget.searchQueryRequest.description,
            onChanged: (value) {
              widget.searchQueryRequest.description =
                  !widget.searchQueryRequest.description;
              widget.changeSearchRequestOptions(widget.searchQueryRequest);
            },
          ),
          CheckboxListTile(
            title: Text('Search In Products Attributes'),
            value: widget.searchQueryRequest.attributes,
            onChanged: (value) {
              widget.searchQueryRequest.attributes =
                  !widget.searchQueryRequest.attributes;
              widget.changeSearchRequestOptions(widget.searchQueryRequest);
            },
          ),
          FutureBuilder(
            future: getCategories(),
            builder: (BuildContext context,
                    AsyncSnapshot<List<CategorySearch>> snapshot) =>
                snapshot.hasData
                    ? Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.05),
                        child: DropdownButton<String>(
                          value: _selectedCategoryId,
                          hint: const Text(
                            'Select a Category To Search In',
                          ),
                          isExpanded: true,
                          items: snapshot.data.map<DropdownMenuItem<String>>(
                            (value) {
                              return DropdownMenuItem<String>(
                                value: value.categoryId.toString(),
                                child: Text(value.name),
                              );
                            },
                          ).toList(),
                          onChanged: (id) {
                            widget.searchQueryRequest.categoryId =
                                int.parse(id);
                            setState(
                              () {
                                _selectedCategoryId = snapshot.data
                                    .firstWhere(
                                      (category) =>
                                          category.categoryId == int.parse(id),
                                    )
                                    .categoryId
                                    .toString();
                              },
                            );
                          },
                        ),
                      )
                    : Center(
                        child: CircularProgressIndicator(),
                      ),
          ),
        ],
      ),
    );
  }
}
