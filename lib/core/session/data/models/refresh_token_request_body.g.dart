// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refresh_token_request_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RefreshTokenRequestBody _$RefreshTokenRequestBodyFromJson(
    Map<String, dynamic> json) {
  return RefreshTokenRequestBody(
    username: json['username'] as String,
    key: json['key'] as String,
    customerId: json['customer_id'] as String,
  );
}

Map<String, dynamic> _$RefreshTokenRequestBodyToJson(
    RefreshTokenRequestBody instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('username', instance.username);
  writeNotNull('key', instance.key);
  writeNotNull('customer_id', instance.customerId);
  return val;
}
