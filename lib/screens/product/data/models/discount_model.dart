import 'package:australia_garden/screens/product/domin/entities/discount.dart';
import 'package:meta/meta.dart';

class DiscountModel extends Discount {
  DiscountModel({@required String quantity, @required String price})
      : super(quantity: quantity, price: price);

  factory DiscountModel.fromJson(var json) {
    return DiscountModel(quantity: json['quantity'], price: json['price']);
  }
}
