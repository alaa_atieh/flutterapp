import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/address/add_address.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'settings_api.dart';

class Body extends StatefulWidget {
  GlobalKey<ScaffoldState> _scaffoldKey;

  Body(this._scaffoldKey);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  String currentCurrency;

  TextEditingController oldPassword = TextEditingController(text: '');

  TextEditingController newPassword = TextEditingController(text: '');

  TextEditingController confirmNewPassword = TextEditingController(text: '');

  String oldPasswordValidator = '';

  String newPasswordValidator = '';

  String confirmPasswordValidator = '';

  SettingsApi s = new SettingsApi();

  @override
  void initState() {
    super.initState();
    setState(() {
      currentCurrency = _sharedPreferencesManager.getString('currency');
    });
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.04), // 4%
                Text("Settings", style: headingStyle),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                FutureBuilder(
                  future: s.listCurrencies(),
                  builder: (context, snapshot) => snapshot.hasData
                      ? DropdownButtonFormField(
                          value: currentCurrency,
                          onChanged: (value) async {
                            bool result = await (s.changeCurrency(value));
                            if (result) {
                              _sharedPreferencesManager.putString(
                                  'currency', value);
                            } else {
                              setState(
                                () {
                                  currentCurrency = _sharedPreferencesManager
                                      .getString('currency');
                                },
                              );
                            }
                          },
                          hint: Row(
                            children: [
                              Icon(
                                FontAwesomeIcons.dollarSign,
                                color: kPrimaryColor,
                                size: 17,
                              ),
                              Text(
                                "Currency",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          items: snapshot.data.map<DropdownMenuItem<String>>(
                            (val) {
                              return DropdownMenuItem<String>(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      val.symbolLeft != null
                                          ? TextSpan(
                                              text: val.symbolLeft,
                                              style: TextStyle(
                                                color: kPrimaryColor,
                                              ),
                                            )
                                          : TextSpan(text: ""),
                                      TextSpan(
                                        text: "  " + val.title,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      val.symbolRight != null
                                          ? TextSpan(
                                              text: " " + val.symbolRight,
                                              style: TextStyle(
                                                color: kPrimaryColor,
                                              ),
                                            )
                                          : TextSpan(text: ""),
                                    ],
                                  ),
                                ),
                                value: val.code,
                                onTap: () {
                                  currentCurrency = val.code;
                                },
                              );
                            },
                          ).toList(),
                        )
                      : Center(
                          child: CircularProgressIndicator(),
                        ),
                ),

                SizedBox(height: SizeConfig.screenHeight * 0.02),
                Container(
                  color: Colors.white,
                  child: ExpansionTile(
                    title: Text(
                      "Change Password",
                      style: TextStyle(
                          color: kPrimaryColor, fontWeight: FontWeight.bold),
                    ),
                    childrenPadding: EdgeInsets.all(5),
                    children: [
                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Input(
                              controller: oldPassword,
                              hintText: "Old Password",
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }

                                if (oldPasswordValidator != '') {
                                  return oldPasswordValidator;
                                }
                                return null;
                              },
                              secure: true,
                              icon: Icon(
                                FontAwesomeIcons.lock,
                                color: kPrimaryColor,
                                size: 15,
                              ),
                            ),
                            SizedBox(
                              height: SizeConfig.screenWidth / 24,
                            ),
                            Input(
                              controller: newPassword,
                              hintText: "New Password",
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }

                                if (newPasswordValidator != '') {
                                  return newPasswordValidator;
                                }
                                return null;
                              },
                              secure: true,
                              icon: Icon(
                                FontAwesomeIcons.lock,
                                color: kPrimaryColor,
                                size: 15,
                              ),
                            ),
                            SizedBox(
                              height: SizeConfig.screenWidth / 24,
                            ),
                            Input(
                              controller: confirmNewPassword,
                              hintText: "Password Confirmation",
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }

                                if (confirmPasswordValidator != '') {
                                  return confirmPasswordValidator;
                                }
                                return null;
                              },
                              secure: true,
                              icon: Icon(
                                FontAwesomeIcons.lock,
                                color: kPrimaryColor,
                                size: 15,
                              ),
                            ),
                            SizedBox(
                              height: SizeConfig.screenWidth / 24,
                            ),
                            DefaultButton(
                              text: "Change",
                              press: () async {
                                if (_formKey.currentState.validate()) {
                                  var result = await s.changePassword(
                                      oldPassword.text,
                                      newPassword.text,
                                      confirmNewPassword.text);
                                  if (result != null) {
                                    if (result['error'] != null) {
                                      oldPasswordValidator =
                                          result['error']['old'];
                                      newPasswordValidator =
                                          result['error']['warning'];
                                    }
                                    final formv = _formKey.currentState;
                                    if (formv.validate()) {
                                      Scaffold.of(context).showSnackBar(
                                        new SnackBar(
                                          content: Row(
                                            children: [
                                              Icon(
                                                FontAwesomeIcons.checkCircle,
                                                color: kPrimaryColor,
                                                size: 17,
                                              ),
                                              Text(
                                                  "   Your password has been rest succefully"),
                                            ],
                                          ),
                                        ),
                                      );
                                    } else {
                                      oldPasswordValidator = "";
                                      newPasswordValidator = "";
                                    }
                                  }
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: getProportionateScreenHeight(20),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
