import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProfileApi {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  Future<dynamic> editProfile(
    String firstname,
    String lastname,
    String email,
    String telephone,
  ) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    var r;
    try {
      r = await http
          .post(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/customer/edit',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
            body: jsonEncode(
              <String, String>{
                'firstname': firstname,
                'lastname': lastname,
                'email': email,
                'telephone': telephone
              },
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (r.statusCode == 200) {
        return json.decode(r.body);
      } else if (r.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await editProfile(firstname, lastname, email, telephone);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed To Edit Profile');
      }
      //timeout(Duration(seconds: 10));
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }
}
