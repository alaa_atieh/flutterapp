import 'subcategory.dart';
import '../product/data/models/related_product_model.dart';

class Category {
  String id;
  String name;
  List subcategories;
  String image;
  String description;
  List<RelatedProductModel> products;

  Category(this.id, this.name, this.subcategories, this.image, this.description,
      this.products);

  Category.fromJson(Map<String, dynamic> json) {
    var subcategorieslist = json['children'] as List;
    id = json['category_id'];
    name = json['name'];
    image = json['thumb'] != null ? json['thumb'] : null;
    description = json['description'] != null ? json['description'] : null;
    if (json['products'] != null) {
      var productslist = json['products'] as List;

      products =
          productslist.map((e) => RelatedProductModel.fromJson(e)).toList();
    }
    subcategories =
        subcategorieslist.map((e) => Subcategory.fromJson(e)).toList();
  }
}
