import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:im_stepper/stepper.dart';

import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/checkout/order_success.dart';

import 'data/datasources/checkout_requests.dart';
import 'payment_method.dart';
import 'shipping_payment_api.dart';
import 'webview_payment.dart';

class PaymentMethodScreen extends StatefulWidget {
  bool hasProducts;
  PaymentMethodScreen({
    this.hasProducts,
  });

  @override
  _PaymentMethodScreenState createState() => _PaymentMethodScreenState();
}

class _PaymentMethodScreenState extends State<PaymentMethodScreen> {
  final _formKey2 = GlobalKey<FormState>();
  ShippingMethodApi s = new ShippingMethodApi();
  List<PaymentMethod> paymentMethods = <PaymentMethod>[];
  PaymentMethod chosenMethod;
  TextEditingController commentController = TextEditingController(text: '');
  final CheckoutRequests _checkoutRequests = locator<CheckoutRequests>();

  Future<bool> placeOrder() async {
    return await _checkoutRequests.addOrder();
  }

  @override
  void initState() {
    super.initState();
    getPaymentMethods(s);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Payment method"),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/backgroundimage.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: SafeArea(
            child: SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: SingleChildScrollView(
                  child: Column(children: [
                    Text(
                      "Payment Method",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getProportionateScreenWidth(28),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    widget.hasProducts
                        ? IconStepper(
                            lineColor: Colors.black,
                            icons: [
                              SvgPicture.asset(
                                "assets/icons/iconfinder_address-billing_4192087.svg",
                                width: SizeConfig.screenWidth * 0.08,
                                height: SizeConfig.screenWidth * 0.08,

                                color: kPrimaryColor,
                                //size: 8,
                              ),
                              SvgPicture.asset(
                                "assets/icons/iconfinder_address-shipping_4301321.svg",
                                width: SizeConfig.screenWidth * 0.08,
                                height: SizeConfig.screenWidth * 0.08,
                                //FontAwesomeIcons.shippingFast,
                                color: kPrimaryColor,
                                // size: 6,
                              ),
                              SvgPicture.asset(
                                "assets/icons/delivery.svg",
                                width: SizeConfig.screenWidth * 0.08,
                                height: SizeConfig.screenWidth * 0.08,
                                //FontAwesomeIcons.shippingFast,
                                color: kPrimaryColor,
                                //size: 6,
                              ),
                              SvgPicture.asset(
                                "assets/icons/paymentmethod.svg",
                                width: SizeConfig.screenWidth * 0.08,
                                height: SizeConfig.screenWidth * 0.08,

                                //FontAwesomeIcons.paypal,
                                //  size: 6,
                              ),
                            ],
                            enableNextPreviousButtons: false,
                            activeStep: 3,
                            activeStepColor: Colors.grey.shade100,
                            stepColor: Colors.grey.shade100,
                            activeStepBorderColor: Colors.yellow.shade700,
                            activeStepBorderWidth: 1.5,
                            enableStepTapping: false,
                          )
                        : IconStepper(
                            lineColor: Colors.black,
                            icons: [
                              SvgPicture.asset(
                                "assets/icons/paymentmethod.svg",
                                width: SizeConfig.screenWidth * 0.08,
                                height: SizeConfig.screenWidth * 0.08,

                                //FontAwesomeIcons.paypal,
                                //  size: 6,
                              ),
                              SvgPicture.asset(
                                "assets/icons/iconfinder_address-billing_4192087.svg",
                                height: 25,
                                width: 25,
                                //  Icons.flag
                              ),
                            ],
                            enableNextPreviousButtons: false,
                            activeStep: 1,
                            activeStepColor: Colors.grey.shade100,
                            stepColor: Colors.grey.shade100,
                            activeStepBorderColor: Colors.yellow.shade700,
                            activeStepBorderWidth: 1.5,
                            enableStepTapping: false,
                          ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    Column(
                      children: [
                        Form(
                          key: _formKey2,
                          child: Column(
                            children: [
                              SizedBox(height: SizeConfig.screenHeight * 0.03),
                              DropdownButtonFormField(
                                isExpanded: true,
                                validator: (value) {
                                  if (value == null) {
                                    return 'Please choose a payment method';
                                  }
                                  return null;
                                },
                                onChanged: (value) {},
                                hint: Text('Choose payment method'),
                                items: paymentMethods.map(
                                  (val) {
                                    return new DropdownMenuItem(
                                      child: new Text(val.title),
                                      value: val.code,
                                      onTap: () {
                                        chosenMethod = val;
                                      },
                                    );
                                  },
                                ).toList(),
                              ),
                              SizedBox(height: SizeConfig.screenHeight * 0.06),
                              TextFormField(
                                maxLines: 3,
                                decoration: InputDecoration(
                                  hintText: "Comments",
                                ),
                                controller: commentController,
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: SizeConfig.screenHeight * 0.04),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: DefaultButton(
                                text: "Prev",
                                press: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            SizedBox(width: SizeConfig.screenWidth * 0.08),
                            Expanded(
                              child: DefaultButton(
                                  text: "Next step ",
                                  press: () async {
                                    if (_formKey2.currentState.validate()) {
                                      await s.savePaymentMethod(
                                          chosenMethod.code,
                                          commentController.text);
                                      //TODO BANK ACCOUNT
                                      if (chosenMethod.title ==
                                          "Cash On Delivery") {
                                        if (await placeOrder()) {
                                          Navigator.of(context)
                                              .pushAndRemoveUntil(
                                            MaterialPageRoute(
                                              builder: (_) => OrderSuccess(),
                                            ),
                                            (Route<dynamic> route) => false,
                                          );
                                        } else {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            const SnackBar(
                                              content: const Text(
                                                  'Something went wrong'),
                                            ),
                                          );
                                        }
                                      } else {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (_) => WebViewExample(),
                                          ),
                                        );
                                      }
                                    }
                                  }),
                            ),
                          ],
                        )
                      ],
                    )
                  ]),
                ),
              ),
            ),
          ),
        ));
  }

  getPaymentMethods(ShippingMethodApi s) async {
    List<PaymentMethod> pa;
    pa = await s.listPaymentMethods();
    setState(
      () {
        paymentMethods = pa;
      },
    );
  }
}
