import 'package:json_annotation/json_annotation.dart';

import 'package:australia_garden/screens/category/data/models/sub_category_search.dart';

part 'category_search.g.dart';

@JsonSerializable()
class CategorySearch {
  String name;
  @JsonKey(name: 'category_id')
  int categoryId;
  List<SubCategorySearch> children;

  CategorySearch({
    this.name,
    this.categoryId,
    this.children,
  });

  factory CategorySearch.fromJson(Map<String, dynamic> json) =>
      _$CategorySearchFromJson(json);
  Map<String, dynamic> toJson() => _$CategorySearchToJson(this);

  @override
  String toString() =>
      'CategorySearch(name: $name, categoryId: $categoryId, children: $children)';
}
