import 'dart:io';

import 'package:flutter/material.dart';
import 'size_config.dart';

/* 

192.168.1.108:8087
/deluxe/index.php'
 */

/* 

australia-garden.com.au';
/index.php';
 */

const String authority = 'australia-garden.com.au';
const String basicRoute = '/index.php';
const String apiKey =
    'EaydwDjgTry3WDdWF8Ngtzw0TAzQiSQ0UiBq0cjS5ZuzyDbHZsOGcG0o7FEaHRNf2EfesG6iRHZvO7sGsccNH7onMVgoqN5BbPSZXM4QaJe9YNfTaF17Lm3RhSTHVtUxZnQdSrtZobmCgIGfwXLmr8XFoGSR2gOp1O9fq3Ey32KNmFQHqwvmCn4uVjdygMtEE2U1zPb5RgPri77dXOy9H4YC5gGbwBXp2pXdvOnEf1T7FbQKtSkEaMqZ6sSM4JCz';

const kPrimaryColor = //Colors.orange[100];
    Color(0xFF509953);
const kPrimaryLightColor = Color(0xFFFFECDF);
Color kYellowColor = Colors.yellow.shade700;
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: getProportionateScreenWidth(28),
  fontWeight: FontWeight.bold,
  fontFamily: 'ALCHEVROLA',
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNamelNullError = "Please Enter your name";
const String kPhoneNumberNullError = "Please Enter your phone number";
const String kAddressNullError = "Please Enter your address";
const String KAttemptsError =
    "You exceeded allowed number of login attempts.try again in 1 hour.";
final bool isIOS = Platform.isIOS;
final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: BorderSide(color: kTextColor),
  );
}
