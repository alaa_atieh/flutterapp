// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartItem _$CartItemFromJson(Map<String, dynamic> json) {
  return CartItem(
    cartId: json['cart_id'] as String,
    productId: json['product_id'] as int,
    name: json['name'] as String,
    model: json['model'] as String,
    option: (json['option'] as List)
        ?.map((e) => e == null
            ? null
            : CartItemOption.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    quantity: json['quantity'] as String,
    stock: json['stock'] as bool,
    shipping: json['shipping'] as String,
    price: json['price'] as String,
    total: json['total'] as String,
    reward: json['reward'] as int,
    thumb: json['thumb'] as String,
    minimum: json['minimum'] as int,
    maximum: json['maximum'] as int,
  );
}

Map<String, dynamic> _$CartItemToJson(CartItem instance) => <String, dynamic>{
      'cart_id': instance.cartId,
      'product_id': instance.productId,
      'name': instance.name,
      'model': instance.model,
      'option': instance.option?.map((e) => e?.toJson())?.toList(),
      'quantity': instance.quantity,
      'stock': instance.stock,
      'shipping': instance.shipping,
      'price': instance.price,
      'total': instance.total,
      'reward': instance.reward,
      'thumb': instance.thumb,
      'minimum': instance.minimum,
      'maximum': instance.maximum,
    };

CartItemOption _$CartItemOptionFromJson(Map<String, dynamic> json) {
  return CartItemOption(
    name: json['name'] as String,
    value: json['value'] as String,
  );
}

Map<String, dynamic> _$CartItemOptionToJson(CartItemOption instance) =>
    <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
    };
