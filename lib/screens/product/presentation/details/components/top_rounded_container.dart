import 'package:australia_garden/core/size_config.dart';
import 'package:flutter/material.dart';
import 'package:australia_garden/core/app_properties.dart';

class TopRoundedContainer extends StatelessWidget {
  const TopRoundedContainer(
      {Key key, @required this.color, @required this.child, this.borderColor})
      : super(key: key);

  final Color color;
  final Widget child;
  final Color borderColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: getProportionateScreenWidth(20)),
      padding: EdgeInsets.only(top: getProportionateScreenWidth(20)),
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: shadow2,
        border: Border.all(
          color: borderColor,
          width: 1.2,
        ),
        color: color,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
          topRight: Radius.circular(40),
        ),
      ),
      child: child,
    );
  }
}
