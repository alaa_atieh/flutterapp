import 'package:flutter/material.dart';
import 'package:australia_garden/core/theme.dart';
import 'order.dart';
import 'package:australia_garden/core/constants.dart';

class OpenFlutterOrderTile extends StatelessWidget {
  final Order order;
  final Function(String) onClick;

  const OpenFlutterOrderTile(
      {Key key, @required this.order, @required this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(AppSizes.imageRadius),
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: AppColors.black.withOpacity(0.3),
                blurRadius: AppSizes.imageRadius,
              )
            ],
            borderRadius: BorderRadius.circular(AppSizes.imageRadius),
            color: AppColors.white,
          ),
          child: Padding(
            padding: EdgeInsets.all(AppSizes.sidePadding),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                        text: TextSpan(children: <TextSpan>[
                      TextSpan(
                        text: 'Order: ',
                        style: TextStyle(
                            color: AppColors.lightGray,
                            fontWeight: FontWeight.normal,
                            fontSize: 16,
                            fontFamily: 'Metropolis'),
                      ),
                      TextSpan(
                        text: '#' + order.id.toString(),
                        style: TextStyle(
                                color: AppColors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Metropolis')
                            .copyWith(fontWeight: FontWeight.w700),
                      ),
                    ])),
                    Text(order.dateAdded,
                        style: TextStyle(
                                color: AppColors.lightGray,
                                fontSize: 14,
                                fontFamily: 'Metropolis',
                                fontWeight: FontWeight.w400)
                            .copyWith(color: AppColors.red))
                  ],
                ),
                SizedBox(
                  height: AppSizes.linePadding,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              'Quantity: ',
                              style: TextStyle(
                                      color: AppColors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: 'Metropolis')
                                  .copyWith(color: AppColors.lightGray),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: AppSizes.linePadding),
                              child: Text(
                                order.productsCount.toString(),
                                style: TextStyle(
                                    color: AppColors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Metropolis'),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Total Amount: ',
                          style: TextStyle(
                                  color: AppColors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: 'Metropolis')
                              .copyWith(color: AppColors.lightGray),
                        ),
                        Text(
                          order.totalPrice,
                          //total amount
                          style: TextStyle(
                              color: AppColors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontFamily: 'Metropolis'),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: AppSizes.linePadding,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RaisedButton(
                      padding: EdgeInsets.only(
                          left: 24, right: 24, top: 10, bottom: 10),
                      color: AppColors.white,
                      onPressed: () {
                        onClick(order.id);
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(AppSizes.imageRadius),
                          side: BorderSide(color: AppColors.black, width: 2)),
                      child: Text(
                        'Details',
                        style: TextStyle(
                            color: AppColors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Metropolis'),
                      ),
                    ),
                    Text(order.status,
                        style: TextStyle(
                                color: AppColors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Metropolis')
                            .copyWith(color: kPrimaryColor)),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
