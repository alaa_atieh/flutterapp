// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sub_category_search.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubCategorySearch _$SubCategorySearchFromJson(Map<String, dynamic> json) {
  return SubCategorySearch(
    name: json['name'] as String,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$SubCategorySearchToJson(SubCategorySearch instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
    };
