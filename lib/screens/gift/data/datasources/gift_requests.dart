import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/gift/data/models/voucher_gift_model.dart';
import 'package:australia_garden/screens/gift/data/models/voucher_themes_model.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

abstract class GiftRequest {
  Future<VoucherThemesModel> getVoucherThemes();
  Future<Map<String, String>> addVoucherToCart(VoucherGiftModel voucherGift);
}

class GiftRequestImpl implements GiftRequest {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  @override
  Future<VoucherThemesModel> getVoucherThemes() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/voucher/themes',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        Map data = json.decode(response.body);
        var temp = VoucherThemesModel.fromJson(data);
        return temp;
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getVoucherThemes();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Get Voucher Themes');
      }
    } on SocketException catch (e) {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
      return null;
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Future<Map<String, String>> addVoucherToCart(
    VoucherGiftModel voucherGift,
  ) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http
          .post(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/voucher/add',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
            body: voucherGift.toJson(),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        String _result;
        String _isError;
        Map _data = json.decode(response.body);
        _isError = _data['success'] == null ? '1' : '0';
        _result = _data['success'] ?? _data['error'];
        return {'isError': _isError, 'result': _result};
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await addVoucherToCart(voucherGift);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Add Voucher Gift');
      }
    } on SocketException catch (e) {
      Fluttertoast.showToast(
        msg: 'No internet connection',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
      return null;
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }
}
