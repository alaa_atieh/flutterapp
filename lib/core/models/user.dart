import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  @JsonKey(name: 'firstname')
  String firstName;
  @JsonKey(name: 'lastname')
  String lastName;
  String email;
  String telephone;

  User({
    this.firstName,
    this.lastName,
    this.email,
    this.telephone,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() {
    return 'User(firstName: $firstName, lastName: $lastName, email: $email, telephone: $telephone)';
  }
}
