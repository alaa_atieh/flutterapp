import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/address/address_api.dart';
import 'package:australia_garden/screens/checkout/shipping_method_screen.dart';
import 'package:flutter/material.dart';
import 'package:im_stepper/stepper.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'address.dart';
import 'components/address_form.dart';

class ShippingAddressScreen extends StatefulWidget {
  Function() onAddAddress;
  @override
  _ShippingAddressScreenState createState() => _ShippingAddressScreenState();
}

class _ShippingAddressScreenState extends State<ShippingAddressScreen> {
  final _formKey2 = GlobalKey<FormState>();
  List<Address> addresses = <Address>[];
  String chosenCountry;
  String chosenCity;
  bool existingAddress;
  bool newAddress;
  Address chosenAddress;
  bool postal = false;
  AddressApi a = new AddressApi();

  @override
  void initState() {
    super.initState();

    getAddresses(a);
    existingAddress = false;
    newAddress = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shipping address"),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/backgroundimage.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: SizedBox(
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Text(
                      "Shipping Address",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getProportionateScreenWidth(28),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(30),
                    ),
                    IconStepper(
                      lineColor: Colors.black,
                      icons: [
                        SvgPicture.asset(
                          "assets/icons/iconfinder_address-billing_4192087.svg",
                          width: SizeConfig.screenWidth * 0.08,
                          height: SizeConfig.screenWidth * 0.08,
                          color: kPrimaryColor,
                        ),
                        SvgPicture.asset(
                          "assets/icons/iconfinder_address-shipping_4301321.svg",
                          width: SizeConfig.screenWidth * 0.08,
                          height: SizeConfig.screenWidth * 0.08,
                          color: Colors.black,
                        ),
                        SvgPicture.asset(
                          "assets/icons/delivery.svg",
                          width: SizeConfig.screenWidth * 0.08,
                          height: SizeConfig.screenWidth * 0.08,
                        ),
                        SvgPicture.asset(
                          "assets/icons/paymentmethod.svg",
                          width: SizeConfig.screenWidth * 0.09,
                          height: SizeConfig.screenWidth * 0.09,
                        ),
                      ],
                      enableNextPreviousButtons: false,
                      activeStep: 1,
                      activeStepColor: Colors.grey.shade100,
                      stepColor: Colors.grey.shade100,
                      activeStepBorderColor: Colors.yellow.shade700,
                      activeStepBorderWidth: 1.5,
                      enableStepTapping: false,
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    Row(
                      children: [
                        Checkbox(
                          value: existingAddress,
                          onChanged: (value) {
                            setState(
                              () {
                                if (existingAddress) {
                                  existingAddress = false;
                                } else {
                                  existingAddress = true;
                                  newAddress = false;
                                }
                              },
                            );
                          },
                        ),
                        Text(
                          "choose existing address",
                          style: TextStyle(fontSize: 18),
                        ),
                      ],
                    ),
                    existingAddress
                        ? Column(
                            children: [
                              Form(
                                key: _formKey2,
                                child: DropdownButtonFormField(
                                  isExpanded: true,
                                  validator: (value) {
                                    if (value == null) {
                                      return 'Please choose an address';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {},
                                  hint: Text('Choose Address'),
                                  items: addresses.map(
                                    (val) {
                                      return new DropdownMenuItem(
                                        child: new Text(val.address1),
                                        value: val.id,
                                        onTap: () {
                                          chosenAddress = val;
                                        },
                                      );
                                    },
                                  ).toList(),
                                ),
                              ),
                              SizedBox(
                                height: SizeConfig.screenHeight / 30,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: DefaultButton(
                                      text: "Prev",
                                      press: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                      width: SizeConfig.screenWidth * 0.08),
                                  Expanded(
                                    child: DefaultButton(
                                      text: "Next step ",
                                      press: () async {
                                        if (_formKey2.currentState.validate()) {
                                          var result = await a.addAddress(
                                            chosenAddress.firstName,
                                            chosenAddress.lastName,
                                            chosenAddress.city,
                                            chosenAddress.address1,
                                            chosenAddress.address2,
                                            "180",
                                            "2846",
                                            chosenAddress.postCode,
                                            "shippingExist",
                                            chosenAddress.id,
                                            chosenAddress.isDefault,
                                          );
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (_) =>
                                                      ShippingMethodScreen()));
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              )
                            ],
                          )
                        : Container(),
                    SizedBox(height: SizeConfig.screenHeight * 0.04),
                    Row(
                      children: [
                        Checkbox(
                          value: newAddress,
                          onChanged: (value) {
                            setState(
                              () {
                                if (newAddress)
                                  newAddress = false;
                                else {
                                  newAddress = true;
                                  existingAddress = false;
                                }
                              },
                            );
                          },
                        ),
                        Text(
                          "Add new address",
                          style: TextStyle(fontSize: 18),
                        ),
                      ],
                    ),
                    newAddress
                        ? AddressForm(
                            chosenAddress: chosenAddress,
                            type: "shippingNew",
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  getAddresses(AddressApi a) async {
    List<Address> ad;
    ad = await a.listAddresses();
    setState(
      () {
        addresses = ad;
      },
    );
  }
}
