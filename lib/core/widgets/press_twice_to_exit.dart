import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PressTwiceToExit extends StatelessWidget {
  final Widget child;
  DateTime currentBackPressTime;

  PressTwiceToExit({Key key, this.child}) : super(key: key);

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(
        msg: 'Press again to exit',
        backgroundColor: Colors.black,
      );
      return Future.value(false);
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: child,
      onWillPop: onWillPop,
    );
  }
}
