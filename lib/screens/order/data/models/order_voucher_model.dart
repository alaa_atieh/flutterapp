import 'package:json_annotation/json_annotation.dart';

part 'order_voucher_model.g.dart';

@JsonSerializable()
class OrderVoucherModel {
  String description;
  String amount;

  OrderVoucherModel({
    this.description,
    this.amount,
  });

  factory OrderVoucherModel.fromJson(Map<String, dynamic> json) =>
      _$OrderVoucherModelFromJson(json);
  Map<String, dynamic> toJson() => _$OrderVoucherModelToJson(this);
}
