import 'package:australia_garden/screens/auth/forgot_password/forgot_password_screen.dart';
import 'package:australia_garden/screens/auth/otp/otp_screen.dart';
import 'package:australia_garden/screens/splash/splash_screen.dart';
import 'package:flutter/widgets.dart';
import 'package:australia_garden/screens/home/home_screen.dart';
import 'package:australia_garden/screens/auth//sign_in/sign_in_screen.dart';

import 'package:australia_garden/screens/auth//sign_up/sign_up_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  OtpScreen.routeName: (context) => OtpScreen(),
  // CartScreen.routeName: (context) => CartScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  // DetailsScreen.routeName: (context) => DetailsScreen(),
};
