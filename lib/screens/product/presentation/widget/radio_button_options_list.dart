import 'package:australia_garden/screens/product/domin/entities/product_option_value.dart';
import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'package:flutter/material.dart';

class RadioButtonOptionsList extends StatefulWidget {
  /* List of Options for the product  */
  final List<ProductOptionValue> productOptionValues;
  /* The name of the option */
  final String optionName;
  /* A function that will be executed whenever one of the radio button is selected */
  final Callback onOptionSelected;
  /* The ID for the product option  */
  final String optionId;

  RadioButtonOptionsList({
    @required this.productOptionValues,
    @required this.optionName,
    @required this.onOptionSelected,
    @required this.optionId,
  });

  @override
  _RadioButtonOptionsListState createState() => _RadioButtonOptionsListState();
}

class _RadioButtonOptionsListState extends State<RadioButtonOptionsList> {
  String _selected = '';
  int _index;
  ProductOptionValue temp;

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(widget.optionName),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: widget.productOptionValues
                .map(
                  (e) => RadioListTile<String>(
                    controlAffinity: ListTileControlAffinity.trailing,
                    title: Text(e.name),
                    secondary: e.image == null
                        ? null
                        : Image.network(
                            e.image,
                          ),
                    subtitle: Text(e.price == null ? '' : '${e.price}'),
                    value: e.productOptionValueId,
                    groupValue: _selected,
                    onChanged: (String value) {
                      setState(() {
                        _selected = value;
                        _index = widget.productOptionValues.indexWhere(
                            (element) =>
                                element.productOptionValueId == _selected);
                        widget.onOptionSelected(
                            widget.optionId,
                            widget.productOptionValues[_index]
                                .productOptionValueId);
                      });
                    },
                  ),
                )
                .toList(),
          ),
        ],
      ),
    );
  }
}
