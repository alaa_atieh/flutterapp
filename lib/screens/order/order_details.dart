import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/theme.dart';
import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:australia_garden/screens/cart/presentation/widgets/voucher_card.dart';
import 'package:australia_garden/screens/category/base_product_list_item.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:australia_garden/core/app_properties.dart';
import 'order.dart';
import 'order_api.dart';
import 'package:australia_garden/core/size_config.dart';

import 'order_detail_model.dart';

class OrderDetails extends StatefulWidget {
  OrderDetails({this.order});

  Order order;

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  OrderApi o = new OrderApi();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: o.orderInfo(widget.order.id),
        builder: (BuildContext context, AsyncSnapshot<DetailedOrder> snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError) {
            return RetryButton(
              action: () => setState(
                () {},
              ),
            );
          }
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Container(
                child: Column(
                  //  mainAxisAlignment: MainAxisAlignment.,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SafeArea(
                      child: Container(
                        color: Colors.grey.shade100,
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.screenWidth / 22),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              widget.order.status == 'Complete'
                                  ? Icon(
                                      FontAwesomeIcons.checkCircle,
                                      size: 20,
                                      color: kPrimaryColor,
                                    )
                                  : widget.order.status == 'Pending'
                                      ? Icon(
                                          FontAwesomeIcons.clock,
                                          size: 20,
                                          color: Colors.yellow.shade700,
                                        )
                                      : Icon(
                                          Icons.remove_circle_outline,
                                          size: 20,
                                          color: Colors.red,
                                        ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: SizeConfig.screenWidth * 0.02),
                                child: Text(
                                  widget.order.status,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(
                        SizeConfig.screenWidth / 22,
                      ),
                      child: Container(
                          width: double.infinity,
                          // color: Colors.grey.shade200,
                          decoration: BoxDecoration(
                              boxShadow: shadow, color: Colors.white),
                          child: Padding(
                            padding: EdgeInsets.all(15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    RichText(
                                        text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: 'Order: ',
                                          style: TextStyle(
                                              color: Colors.grey.shade700,
                                              //AppColors.lightGray,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 16,
                                              fontFamily: 'Metropolis'),
                                        ),
                                        TextSpan(
                                          text:
                                              '#' + widget.order.id.toString(),
                                          style: TextStyle(
                                                  color: AppColors.black,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'Metropolis')
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    )),
                                    Text(snapshot.data.dateAdded,
                                        style: TextStyle(
                                                color: AppColors.lightGray,
                                                fontSize: 14,
                                                fontFamily: 'Metropolis',
                                                fontWeight: FontWeight.w400)
                                            .copyWith(color: AppColors.red))
                                  ],
                                ),
                                RichText(
                                    text: TextSpan(children: <TextSpan>[
                                  TextSpan(
                                    text: 'Total price: ',
                                    style: TextStyle(
                                        color: Colors.grey.shade700,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 16,
                                        fontFamily: 'Metropolis'),
                                  ),
                                  TextSpan(
                                    text: widget.order.totalPrice,
                                    style: TextStyle(
                                            color: AppColors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: 'Metropolis')
                                        .copyWith(fontWeight: FontWeight.w700),
                                  ),
                                ])),
                              ],
                            ),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.all(
                        SizeConfig.screenWidth / 22,
                      ),
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            boxShadow: shadow, color: Colors.white),
                        child: ExpansionTile(
                            title: Text(
                              'Payment',
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                  left: SizeConfig.screenWidth / 24,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      FontAwesomeIcons.moneyCheck,
                                      size: 12,
                                      color: kPrimaryColor,
                                    ),
                                    Expanded(
                                      child: RichText(
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                            text: '  Payment method :',
                                            style: TextStyle(
                                                color: Colors.grey.shade700,
                                                fontWeight: FontWeight.normal,
                                                fontSize: 16,
                                                fontFamily: 'Metropolis'),
                                          ),
                                          TextSpan(
                                            text: snapshot.data.paymentMethod,
                                            style: TextStyle(
                                                    color: AppColors.black,
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: 'Metropolis')
                                                .copyWith(
                                                    fontWeight:
                                                        FontWeight.w700),
                                          ),
                                        ]),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: SizeConfig.screenWidth / 24,
                                  bottom: SizeConfig.screenWidth / 24,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      FontAwesomeIcons.mapMarkerAlt,
                                      size: 12,
                                      color: kPrimaryColor,
                                    ),
                                    Expanded(
                                      child: RichText(
                                          text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                          text: '  Payment address :',
                                          style: TextStyle(
                                              color: Colors.grey.shade700,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 16,
                                              fontFamily: 'Metropolis'),
                                        ),
                                        TextSpan(
                                          text: snapshot.data.paymentAddress,
                                          style: TextStyle(
                                                  color: AppColors.black,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'Metropolis')
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ])),
                                    ),
                                  ],
                                ),
                              )
                            ]),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(
                        SizeConfig.screenWidth / 24,
                      ),
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            boxShadow: shadow, color: Colors.white),
                        child: ExpansionTile(
                          title: Text(
                            'Shipping',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                left: SizeConfig.screenWidth / 24,
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    FontAwesomeIcons.truck,
                                    size: 12,
                                    color: kPrimaryColor,
                                  ),
                                  Expanded(
                                    child: RichText(
                                      text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                          text: '   Shipping method:',
                                          style: TextStyle(
                                              color: Colors.grey.shade700,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 16,
                                              fontFamily: 'Metropolis'),
                                        ),
                                        TextSpan(
                                          text: snapshot.data.shippingMethod,
                                          style: TextStyle(
                                                  color: AppColors.black,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'Metropolis')
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ]),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: SizeConfig.screenWidth / 36,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 15, bottom: 15),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    FontAwesomeIcons.mapMarkerAlt,
                                    size: 12,
                                    color: kPrimaryColor,
                                  ),
                                  Expanded(
                                    child: RichText(
                                        text: TextSpan(children: <TextSpan>[
                                      TextSpan(
                                        text: '   Shipping address:',
                                        style: TextStyle(
                                            color: Colors.grey.shade700,
                                            fontWeight: FontWeight.normal,
                                            fontSize: 16,
                                            fontFamily: 'Metropolis'),
                                      ),
                                      TextSpan(
                                        text: snapshot.data.shippingAddress,
                                        style: TextStyle(
                                                color: AppColors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: 'Metropolis')
                                            .copyWith(
                                                fontWeight: FontWeight.w700),
                                      ),
                                    ])),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          snapshot.data.products.isNotEmpty
                              ? Padding(
                                  padding: EdgeInsets.only(
                                    left: SizeConfig.screenWidth / 12,
                                  ),
                                  child: Text(
                                    "Products",
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(fontWeight: FontWeight.bold),
                                  ),
                                )
                              : SizedBox(),
                          ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: snapshot.data.products.length,
                            itemBuilder: (context, index) {
                              return BaseProductListItem(
                                bottomRoundButton: Container(
                                  width: SizeConfig.screenWidth * 0.12,
                                  height: SizeConfig.screenWidth * 0.12,
                                  child: FloatingActionButton(
                                    onPressed: () async {
                                      var result = await o.reOrder(
                                          snapshot.data.id,
                                          snapshot.data.products[index]
                                              .orderProductId);
                                      if (result != null) {
                                        if (result['success'] != null) {
                                          Scaffold.of(context)
                                              .showSnackBar(new SnackBar(
                                            content: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Icon(
                                                  FontAwesomeIcons.checkCircle,
                                                  color: kPrimaryColor,
                                                ),
                                                Expanded(
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                      left: SizeConfig
                                                              .screenWidth /
                                                          36,
                                                    ),
                                                    child:
                                                        Text(result['success']),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ));
                                        }
                                      } else {
                                        Fluttertoast.showToast(
                                            msg:
                                                'This product is currently not available',
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            backgroundColor: Colors.black54,
                                            textColor: Colors.white);
                                      }
                                    },
                                    heroTag:
                                        snapshot.data.products[index].productId,
                                    backgroundColor: kPrimaryColor,
                                    child: Icon(
                                      FontAwesomeIcons.cartPlus,
                                      color: AppColors.white,
                                    ),
                                  ),
                                ),
                                onClick: () {
                                  // Navigator.of(context).push(MaterialPageRoute()
                                },
                                image: Image.network(
                                        snapshot.data.products[index].thumb)
                                    .image,
                                mainContentBuilder: (context) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(snapshot.data.products[index].name,
                                          style: TextStyle(
                                              color: AppColors.black,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: 'Metropolis')),
                                      RichText(
                                          text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                          text: 'Model:',
                                          style: TextStyle(
                                              color: Colors.grey.shade700,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 12,
                                              fontFamily: 'Metropolis'),
                                        ),
                                        TextSpan(
                                          text: snapshot
                                              .data.products[index].model,
                                          style: TextStyle(
                                                  color: AppColors.black,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'Metropolis')
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ])),
                                      RichText(
                                          text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                          text: 'Price:',
                                          style: TextStyle(
                                              color: Colors.grey.shade700,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 12,
                                              fontFamily: 'Metropolis'),
                                        ),
                                        TextSpan(
                                          text: snapshot
                                              .data.products[index].price,
                                          style: TextStyle(
                                                  color: AppColors.black,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'Metropolis')
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ])),
                                      RichText(
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                            text: 'Quantity:',
                                            style: TextStyle(
                                                color: Colors.grey.shade700,
                                                fontWeight: FontWeight.normal,
                                                fontSize: 12,
                                                fontFamily: 'Metropolis'),
                                          ),
                                          TextSpan(
                                            text: snapshot
                                                .data.products[index].quantity,
                                            style: TextStyle(
                                                    color: AppColors.black,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: 'Metropolis')
                                                .copyWith(
                                                    fontWeight:
                                                        FontWeight.w700),
                                          ),
                                        ]),
                                      ),

                                      // Text(product.name, style: Theme.of(context).textTheme.body1),
                                      //buildPrice(Theme.of(context), category.products[index]),
                                      ...List.generate(
                                        snapshot.data.products[index].options
                                            .length,
                                        (ind) => RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: snapshot
                                                        .data
                                                        .products[index]
                                                        .options[ind]
                                                        .name +
                                                    ":",
                                                style: TextStyle(
                                                    color: Colors.grey.shade700,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    fontSize: 12,
                                                    fontFamily: 'Metropolis'),
                                              ),
                                              TextSpan(
                                                text: snapshot
                                                    .data
                                                    .products[index]
                                                    .options[ind]
                                                    .value,
                                                style: TextStyle(
                                                        color: AppColors.black,
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily:
                                                            'Metropolis')
                                                    .copyWith(
                                                        fontWeight:
                                                            FontWeight.w700),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                          snapshot.data.vouchers.isNotEmpty
                              ? Padding(
                                  padding: EdgeInsets.only(
                                    left: SizeConfig.screenWidth / 12,
                                  ),
                                  child: Text(
                                    "Vouchers",
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(fontWeight: FontWeight.bold),
                                  ),
                                )
                              : SizedBox(),
                          ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: snapshot.data.vouchers.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.screenHeight * 0.015,
                                  vertical: SizeConfig.screenWidth * 0.02,
                                ),
                                child: Container(
                                  child: Card(
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: 'Amount : ',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16,
                                                      fontFamily: 'Metropolis'),
                                                ),
                                                TextSpan(
                                                  text: snapshot.data
                                                      .vouchers[index].amount,
                                                  style: TextStyle(
                                                          color: Colors
                                                              .grey.shade700,
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.normal,
                                                          fontFamily:
                                                              'Metropolis')
                                                      .copyWith(
                                                          fontWeight:
                                                              FontWeight.w700),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: 'Description : ',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16,
                                                      fontFamily: 'Metropolis'),
                                                ),
                                                TextSpan(
                                                  text: snapshot
                                                      .data
                                                      .vouchers[index]
                                                      .description,
                                                  style: TextStyle(
                                                          color: Colors
                                                              .grey.shade700,
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.normal,
                                                          fontFamily:
                                                              'Metropolis')
                                                      .copyWith(
                                                          fontWeight:
                                                              FontWeight.w700),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                        ],
                                      ),
                                    ),
                                    // isThreeLine: true,
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          }
          return RetryButton(
            action: () => setState(
              () {},
            ),
          );
        },
      ),
    );
  }
}
