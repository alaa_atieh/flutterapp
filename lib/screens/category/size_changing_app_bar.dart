import 'package:flutter/material.dart';

import 'package:australia_garden/core/theme.dart';

import 'apicalls/categories_api.dart';
import 'category.dart';
import 'sort_rules.dart';
import 'view_options.dart';

class SizeChangingAppBar extends StatefulWidget {
  String categoryName;
  SortRules sortRules;
  String categoryId;
  CategoriesApi c = new CategoriesApi();
  bool isListView;
  final VoidCallback onViewChanged;
  Function(Category category, SortRules sortRules) onSortChanged;

  SizeChangingAppBar({
    this.categoryName,
    this.sortRules,
    this.categoryId,
    this.isListView,
    this.onViewChanged,
    this.onSortChanged,
  });

  @override
  _SizeChangingAppBarState createState() => _SizeChangingAppBarState();
}

class _SizeChangingAppBarState extends State<SizeChangingAppBar> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      expandedHeight: AppSizes.app_bar_expanded_size,
      floating: true,
      primary: true,
      snap: false,
      pinned: false,
      flexibleSpace: FlexibleSpaceBar(
        background: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Text(
                widget.categoryName ?? 'Loading...',
                style: TextStyle(
                  fontSize: 34,
                  color: AppColors.black,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'ALCHEVROLA',
                ),
              ),
            ),
            /*  Container(
              height: 30,
              child: VisualFilter(),
            ),*/
            OpenFlutterViewOptions(
              onChangeViewClicked: widget.onViewChanged,
              onSortChanged: (sortrules) async {
                /* Category cat = await widget.c.getCategory(
                  widget.categoryId,
                  sortrules.typeToApi(),
                  sortrules.orderToApi,
                  15,
                ); */
                widget.onSortChanged(null, sortrules);

                /*    _scaffoldKey.currentState.showSnackBar(new SnackBar(
                        duration: new Duration(seconds: 10),
                        content: new Row(
                          children: <Widget>[
                            new CircularProgressIndicator(),
                            new Text(" sorting...")
                          ],
                        ),
                      ));
 */
              },
              sortRules: widget.sortRules,
            ),
          ],
        ),
      ),
    );
  }
}
