import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/models/image.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:flutter/material.dart';
import 'package:australia_garden/screens/product/data/models/temp_product_model.dart';
import 'package:australia_garden/screens/product/domin/entities/temp_product.dart';

class ProductImages extends StatefulWidget {
  final TempProductModel product;
  final String moduleTitle;

  const ProductImages({Key key, @required this.product, this.moduleTitle})
      : super(key: key);

  @override
  _ProductImagesState createState() => _ProductImagesState();
}

class _ProductImagesState extends State<ProductImages> {
  int selectedImage = 0;

  @override
  void initState() {
    super.initState();
    if (widget.product.images.isNotEmpty) {
      if (widget.product.images[0].thumb != widget.product.thumb) {
        widget.product.images.insert(
          0,
          ProductImage(
            thumb: widget.product.thumb,
          ),
        );
      }
    } else {
      widget.product.images.insert(
        0,
        ProductImage(
          thumb: widget.product.thumb,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: getProportionateScreenWidth(238),
          child: AspectRatio(
            aspectRatio: 1,
            child: Hero(
              tag: widget.product.productId.toString() + widget.moduleTitle,
              child: Image.network(widget.product.images[selectedImage].thumb),
            ),
          ),
        ),
        // SizedBox(height: getProportionateScreenWidth(20)),
        /* Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ...List.generate(
              widget.product.images.length,
              (index) => buildSmallProductPreview(index),
            ),
          ],
        ) */
        Container(
          height: 50,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: widget.product.images.length,
            itemBuilder: (context, index) => buildSmallProductPreview(index),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Row(
            children: [
              Expanded(
                child: Text(widget.product.productName,
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.black)),
              ),
            ],
          ),
        ),
        buildPrice(widget.product),
      ],
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: defaultDuration,
        margin: EdgeInsets.only(right: 15),
        padding: EdgeInsets.all(10),
        height: getProportionateScreenWidth(48),
        width: getProportionateScreenWidth(48),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: kPrimaryColor.withOpacity(
              selectedImage == index ? 1 : 0,
            ),
          ),
        ),
        child: Image.network(widget.product.images[index].thumb),
      ),
    );
  }

  Widget buildDiscountPrice(Product product) {
    return Text(product.special.toString(),
        style: TextStyle(
            fontSize: 18,
            fontFamily: 'Metropolis',
            fontWeight: FontWeight.w600,
            color: kPrimaryColor));
  }

  Widget buildPrice(Product product) {
    return Row(children: <Widget>[
      product.productPercent > 0 ? buildDiscountPrice(product) : Container(),
      SizedBox(
        width: 4.0,
      ),
      Text(
        product.price != null ? product.price : '',
        style: TextStyle(
          color: Colors.black,
          fontSize: 14,
          fontFamily: 'Metropolis',
          fontWeight: FontWeight.w600,
          decoration: product.productPercent > 0
              ? TextDecoration.lineThrough
              : TextDecoration.none,
        ),
      ),
      SizedBox(
        width: 6.0,
      ),
      Container(
        color: Colors.black,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            "save " + product.productPercent.toString() + "%",
            style: TextStyle(color: Colors.white, fontSize: 12),
          ),
        ),
      )
    ]);
  }
}
