import 'package:meta/meta.dart';

class ProductOptionValue {
  @required
  String productOptionValueId;
  @required
  String optionValueId;
  @required
  String name;
  @required
  String image;
  @required
  String price;
  @required
  String pricePrefix;

  ProductOptionValue(
      {this.productOptionValueId,
      this.optionValueId,
      this.name,
      this.image,
      this.price,
      this.pricePrefix});
}
