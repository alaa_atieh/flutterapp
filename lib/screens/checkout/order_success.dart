import 'package:australia_garden/screens/profile/profile_screen.dart';
import 'package:flutter/material.dart';

import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/widgets/press_twice_to_exit.dart';
import 'package:australia_garden/screens/home/home_screen.dart';
import 'package:australia_garden/screens/order/orders.dart';

class OrderSuccess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PressTwiceToExit(
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.all(SizeConfig.screenWidth * 0.08),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: SizeConfig.screenHeight * 0.06),
              const Text(
                "Your order has been saved successfully!",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              Image.asset(
                "assets/success.jpg",
                height: SizeConfig.screenHeight * 0.66,
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: DefaultButton(
                        text: 'Orders page',
                        press: () {
                          Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                              builder: (context) => ProfileScreen(),
                            ),
                            (Route<dynamic> route) => false,
                          );
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => OrderList(),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      width: SizeConfig.screenWidth * 0.1,
                    ),
                    Expanded(
                      child: DefaultButton(
                        text: 'Home page',
                        press: () {
                          //? Added this
                          Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                              builder: (context) => HomeScreen(),
                            ),
                            (Route<dynamic> route) => false,
                          );
                          //?
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
