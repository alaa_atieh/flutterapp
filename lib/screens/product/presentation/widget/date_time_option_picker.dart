import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:australia_garden/core/constants.dart';

class DateTimePickerOption extends StatefulWidget {
  /* The name of the option */
  final String optionName;
  /* A function that will be executed whenever one of the check boxes is selected */
  final Callback onOptionSelected;
  /* The ID for the product option  */
  final String optionId;
  /* if this field is required */
  final bool isRequired;

  DateTimePickerOption({
    @required this.optionName,
    @required this.onOptionSelected,
    @required this.optionId,
    @required this.isRequired,
  });

  @override
  _DateTimePickerOptionState createState() => _DateTimePickerOptionState();
}

class _DateTimePickerOptionState extends State<DateTimePickerOption> {
  DateTime _selectedDate;
  TimeOfDay _selectedTime = TimeOfDay(hour: 0, minute: 0);
  DateTime _selectedDateTime;
  final DateFormat formatter = DateFormat('yyyy - MM - dd hh:mm');

  void _startDatePicker() {
    !isIOS
        ? showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime.now(),
                lastDate: DateTime(2030),
                errorFormatText: 'Enter valid date',
                errorInvalidText: 'Enter date in valid range')
            .then((pickedDate) {
            if (pickedDate == null) return;
            showTimePicker(
              context: context,
              initialTime: _selectedTime,
              initialEntryMode: TimePickerEntryMode.dial,
            ).then((pickedTime) {
              if (pickedTime == null) return;
              setState(() {
                _selectedDate = pickedDate;
                _selectedTime = pickedTime;
                _selectedDateTime = DateTime(
                  _selectedDate.year,
                  _selectedDate.month,
                  _selectedDate.day,
                  _selectedTime.hour,
                  _selectedTime.minute,
                );
                widget.onOptionSelected(
                  widget.optionId,
                  _selectedDateTime.toString(),
                );
              });
            });
          })
        : buildCupertinoDatePicker(context);
  }

  buildCupertinoDatePicker(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white,
            child: CupertinoDatePicker(
              initialDateTime: DateTime.now(),
              onDateTimeChanged: (DateTime newdate) {
                widget.onOptionSelected(
                  widget.optionId,
                  newdate.toString(),
                );
                setState(() {
                  _selectedDateTime = newdate;
                });
              },
              use24hFormat: true,
              maximumDate: new DateTime(2060, 12, 30),
              minimumYear: 2010,
              maximumYear: 2060,
              minuteInterval: 1,
              mode: CupertinoDatePickerMode.dateAndTime,
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(widget.optionName),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  _selectedDateTime == null
                      ? 'Select The ${widget.optionName}'
                      : DateFormat('yyyy-MM-dd – kk:mm')
                          .format(_selectedDateTime),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.date_range_outlined,
                  color: kPrimaryColor,
                  size: 16,
                ),
                onPressed: _startDatePicker,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
