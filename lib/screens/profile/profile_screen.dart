import 'package:australia_garden/core/widgets/press_twice_to_exit.dart';
import 'package:australia_garden/screens/home/components/custom_bottom_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:australia_garden/core/enums.dart';

import 'components/body.dart';

class ProfileScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  static String routeName = "/profile";
  @override
  Widget build(BuildContext context) {
    return PressTwiceToExit(
      child: Scaffold(
        key: _scaffoldKey,
        body: Container(
          height: double.infinity,
          child: Body(_scaffoldKey),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/backgroundimage.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        bottomNavigationBar: CustomBottomNavBar(
          selectedMenu: MenuState.profile,
        ),
      ),
    );
  }
}
