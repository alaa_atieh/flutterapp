import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CounterButton extends StatefulWidget {
  final int minimum;
  final int maximum;
  final Function(int) onQuantityUpdated;
  CounterButton({
    this.minimum,
    this.maximum,
    this.onQuantityUpdated,
  });
  @override
  _CounterButtonState createState() => _CounterButtonState();
}

class _CounterButtonState extends State<CounterButton> {
  int _currentValue;

  void add() {
    setState(() {
      if (_currentValue < widget.maximum) {
        _currentValue++;
        widget.onQuantityUpdated(_currentValue);
      } else {
        Fluttertoast.showToast(
          msg: 'there are only ${widget.maximum} items available',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white,
        );
      }
    });
  }

  void minus() {
    setState(() {
      if (_currentValue != widget.minimum && _currentValue != 0) {
        _currentValue--;
        widget.onQuantityUpdated(_currentValue);
      } else {
        String _temp = widget.minimum == 1 ? 'item' : 'items';
        Fluttertoast.showToast(
            msg: 'Can\'t Order Less Than ${widget.minimum} ' + _temp,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.black54,
            textColor: Colors.white);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _currentValue = widget.minimum;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: [
            Text('Select Quantity'),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  onPressed: minus,
                  child: Icon(Icons.remove, color: Colors.black),
                ),
                Text(
                  '$_currentValue',
                  style: Theme.of(context).textTheme.headline6,
                ),
                FlatButton(
                  onPressed: add,
                  child: Icon(
                    Icons.add,
                    color: Colors.black,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
