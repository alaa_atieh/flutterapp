import 'package:flutter/material.dart';

import 'package:australia_garden/core/components/no_account_text.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/auth//sign_up/sign_up_screen.dart';
import 'package:australia_garden/screens/auth/forgot_password/forgot_password_screen.dart';
import 'package:australia_garden/screens/home/home_screen.dart';

import 'sign_form.dart';

//TODO Skip entire login page after first skip
class Body extends StatelessWidget {
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: SizeConfig.screenHeight * 0.12),
              Text(
                "Welcome Back",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: getProportionateScreenWidth(28),
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Text(
                "Sign in with your email and password",
                textAlign: TextAlign.center,
              ),
              SizedBox(height: SizeConfig.screenHeight * 0.08),
              SignForm(),
              SizedBox(height: SizeConfig.screenHeight * 0.08),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              NoAccountText(
                text: "Forgot Password? ",
                clickableText: "Change Password",
                route: ForgotPasswordScreen.routeName,
              ),
              NoAccountText(
                text: "Don’t have an account? ",
                clickableText: "Sign Up",
                route: SignUpScreen.routeName,
              ),
              Padding(
                padding: EdgeInsets.only(top: SizeConfig.screenWidth * 0.02),
                child: GestureDetector(
                  onTap: () {
                    _sharedPreferencesManager.putBool(
                      'is_logged_in',
                      false,
                    );
                    Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                        builder: (context) => HomeScreen(),
                      ),
                      (Route<dynamic> route) => false,
                    );
                  },
                  child: Text(
                    'Skip Login',
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(16),
                        color: kPrimaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              //SizedBox(height: SizeConfig.screenHeight * 0.4),
            ],
          ),
        ),
      ),
    );
  }
}
