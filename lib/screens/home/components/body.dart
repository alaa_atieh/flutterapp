import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:flutter/material.dart';
import '../home_image_slider.dart';
import 'popular_product.dart';
import 'package:australia_garden/screens/main/mainpageapis/mainpageapi.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  GetModules g = new GetModules();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: g.getModules(),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return RetryButton(
            action: () => setState(
              () {},
            ),
          );
        }
        if (snapshot.hasData) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  HomeImageSlider(
                    imgList: snapshot.data.sliderImages,
                  ),
                  ...List.generate(
                    snapshot.data.modules.length,
                    (index) {
                      return Padding(
                        padding: EdgeInsets.only(right: 8.0, top: 8, bottom: 8),
                        child: Container(
                          color: Colors.transparent,
                          child: ProductModule(
                              snapshot.data.modules[index].title,
                              snapshot.data.modules[index].products),
                        ),
                      );
                    },
                  ),
                ],
              ), //
            ),
          );
        }
        return RetryButton(
          action: () => setState(
            () {},
          ),
        );
      },
    );
  }
}
