import 'package:australia_garden/core/components/custom_surfix_icon.dart';
import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/components/form_error.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/keyboard.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/home/home_screen.dart';
import 'package:flutter/material.dart';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/api_service.dart';
import 'dart:convert';

class SignForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();
  final _formKey = GlobalKey<FormState>();
  TextEditingController email = TextEditingController(text: '');
  ApiService api = ApiService();
  String emailvalidator = '';
  String attempt = '';
  TextEditingController password = TextEditingController(text: '');
  bool _isLoading;
  //String password;

  bool remember = false;
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  void initState() {
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildEmailFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          Row(
            children: [
              /* Checkbox(
                value: remember,
                activeColor: kPrimaryColor,
                onChanged: (value) {
                  setState(() {
                    remember = value;
                  });
                },
              ),
              Text("Remember me"),*/
              Spacer(),
              /* GestureDetector(
                onTap: () => Navigator.pushNamed(
                    context, ForgotPasswordScreen.routeName),
                child: Text(
                  "Forgot Password",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )*/
            ],
          ),
          FormError(errors: errors),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          !_isLoading
              ? DefaultButton(
                  text: "Continue",
                  press: () async {
                    removeError(error: emailvalidator);
                    removeError(error: kInvalidEmailError);
                    removeError(error: KAttemptsError);

                    emailvalidator = '';
                    if (_formKey.currentState.validate()) {
                      // _formKey.currentState.save();
                      setState(() {
                        _isLoading = true;
                      });
                      var response = await api.login(email.text, password.text);
                      setState(() {
                        _isLoading = false;
                      });
                      if (response != null) {
                        var decoded = json.decode(response.body);
                        if (decoded['error'] != null) {
                          setState(
                            () {
                              if (decoded['error']['warning'] != null)
                                addError(error: KAttemptsError);
                              else {
                                if (decoded['error']['credentials'] != null)
                                  this.emailvalidator =
                                      'Credentials don\'t match';
                                addError(error: emailvalidator);
                              }
                            },
                          );
                        }
                        // if all are valid then go to success screen
                        if (errors.isEmpty) {
                          KeyboardUtil.hideKeyboard(context);
                          _sharedPreferencesManager.putBool(
                            'is_logged_in',
                            true,
                          );
                          _sharedPreferencesManager.putString(
                            'customer_id',
                            decoded['customer_id'],
                          );
                          api.getUserInfo();
                          Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                              builder: (context) => HomeScreen(),
                            ),
                            (Route<dynamic> route) => false,
                          );
                        } else
                          //TODO ????
                          print("here");
                      }
                    } else
                      //TODO ????
                      print("not valid");
                  },
                )
              : Center(
                  child: CircularProgressIndicator(),
                ),
        ],
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      controller: password,
      obscureText: true,
      //onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        }
        /* else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }*/
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        }
        /*else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }*/
        return null;
      },
      decoration: InputDecoration(
        labelText: "Password",
        hintText: "Enter your password",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      //onSaved: (newValue) => email = newValue,
      controller: email,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }

        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Email",
        hintText: "Enter your email",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }
}
