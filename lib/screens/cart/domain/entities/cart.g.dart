// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cart _$CartFromJson(Map<String, dynamic> json) {
  return Cart(
    products: (json['products'] as List)
        ?.map((e) =>
            e == null ? null : CartItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    vouchers: (json['vouchers'] as List)
        ?.map((e) =>
            e == null ? null : Voucher.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    totals: (json['totals'] as List)
        ?.map((e) =>
            e == null ? null : Totals.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CartToJson(Cart instance) => <String, dynamic>{
      'products': instance.products?.map((e) => e?.toJson())?.toList(),
      'vouchers': instance.vouchers?.map((e) => e?.toJson())?.toList(),
      'totals': instance.totals?.map((e) => e?.toJson())?.toList(),
    };
