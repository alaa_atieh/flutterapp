import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/datasource/session_requests.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:australia_garden/screens/cart/data/models/added_to_cart_item.dart';
import 'package:australia_garden/screens/cart/domain/entities/cart.dart';

abstract class CartRequest {
  Future<dynamic> addProductToCart(AddedToCartItem cartItem);
  Future<Cart> getCart();
  Future<String> updateCartItemQuantity(String cartId, String newQuantity);
  Future<String> deleteCartItem(String cartId);
  Future<Map<String, String>> applyVoucherGift(String voucherCode);
  Future<Map<String, String>> applyCoupon(String couponCode);
}

class CartRequestImpl implements CartRequest {
  final SessionRequest _sessionRequest = locator<SessionRequest>();

  @override
  Future<dynamic> addProductToCart(AddedToCartItem cartItem) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http
          .post(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/cart/add',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
            body: json.encode(
              cartItem.toJson(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        final data = json.decode(response.body);
        return data;
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await addProductToCart(cartItem);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Add to Cart');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Future<Cart> getCart() async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http
          .get(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/cart',
                  'api_token': apiToken,
                },
              ).toString(),
            ),
          )
          .timeout(
            Duration(seconds: 10),
          );
      // TODO decode the error if one of cart items is not available in stock
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        Map data = json.decode(response.body);
        return Cart.fromJson(data);
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await getCart();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Load Cart');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Future<String> updateCartItemQuantity(
    String cartId,
    String newQuantity,
  ) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/cart/edit',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'key': cartId,
          'quantity': newQuantity,
        },
      ).timeout(
        Duration(seconds: 10),
      );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        Map data = json.decode(response.body);
        return data['success'];
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await updateCartItemQuantity(cartId, newQuantity);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Update Cart Item');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Future<String> deleteCartItem(String cartId) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/cart/remove',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'key': cartId,
        },
      ).timeout(
        Duration(seconds: 10),
      );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        Map data = json.decode(response.body);
        return data['success'];
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await deleteCartItem(cartId);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Delete Cart Item');
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Future<Map<String, String>> applyVoucherGift(String voucherCode) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/voucher',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'voucher': voucherCode,
        },
      ).timeout(
        Duration(seconds: 10),
      );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        String _result;
        String _isError;
        Map data = json.decode(response.body);
        _isError = data['success'] == null ? '1' : '0';
        _result = data['success'] ?? data['error'];
        return {'isError': _isError, 'result': _result};
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await applyVoucherGift(voucherCode);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception();
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Future<Map<String, String>> applyCoupon(String couponCode) async {
    SharedPreferencesManager _sharedPreferencesManager =
        locator<SharedPreferencesManager>();
    String apiToken = _sharedPreferencesManager.getString('api_token');
    try {
      final response = await http.post(
        Uri.decodeFull(
          Uri.https(
            authority,
            basicRoute,
            {
              'route': 'apiCustomize/coupon',
              'api_token': apiToken,
            },
          ).toString(),
        ),
        body: {
          'coupon': couponCode,
        },
      ).timeout(
        Duration(seconds: 10),
      );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        String _result;
        String _isError;
        Map data = json.decode(response.body);
        _isError = data['success'] == null ? '1' : '0';
        _result = data['success'] ?? data['error'];
        return {'isError': _isError, 'result': _result};
      } else if (response.statusCode == 403) {
        await _sessionRequest.refreshClientToken(false);
        return await applyCoupon(couponCode);
      } else {
        // If the server did not return a 200 OK response,z
        // then throw an exception.
        throw Exception();
      }
    } on SocketException {
      Fluttertoast.showToast(
          msg: 'No internet connection',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    } on TimeoutException {
      Fluttertoast.showToast(
        msg: 'Failed To Communicate With The Server',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }
}
