import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional_switch.dart';

import 'package:australia_garden/core/app_properties.dart';
import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/cart/data/datasources/cart_requests.dart';
import 'package:australia_garden/screens/cart/data/models/added_to_cart_item.dart';
import 'package:australia_garden/screens/cart/presentation/pages/check_out_page.dart';
import 'package:australia_garden/screens/product/data/datasources/product_requests.dart';
import 'package:australia_garden/screens/product/domin/entities/option_selected_list_value.dart';
import 'package:australia_garden/screens/product/domin/entities/product_option.dart';
import 'package:australia_garden/screens/product/presentation/widget/checkbox_options_list.dart';
import 'package:australia_garden/screens/product/presentation/widget/counter_button.dart';
import 'package:australia_garden/screens/product/presentation/widget/date_option_picker.dart';
import 'package:australia_garden/screens/product/presentation/widget/date_time_option_picker.dart';
import 'package:australia_garden/screens/product/presentation/widget/dropdown_options_list.dart';
import 'package:australia_garden/screens/product/presentation/widget/form_error.dart';
import 'package:australia_garden/screens/product/presentation/widget/radio_button_options_list.dart';
import 'package:australia_garden/screens/product/presentation/widget/text_field_option_input.dart';
import 'package:australia_garden/screens/product/presentation/widget/time_picker_option.dart';

typedef void Callback(
  String optionId,
  String optionSelectedValueId,
);

//TODO prevent adding to cart if the quantity is not available
class OptionsBuilder extends StatefulWidget {
  final List<ProductOption> options;
  final int minimum;
  final String productId;
  final int maximumQuantity;
  OptionsBuilder(
      {@required this.productId,
      @required this.options,
      @required this.minimum,
      @required this.maximumQuantity});

  @override
  _OptionsBuilderState createState() => _OptionsBuilderState();
}

class _OptionsBuilderState extends State<OptionsBuilder> {
  /// Holds All the Selected Options
  List<OptionSelectedValue> selectedOptions;
  int selectedQuantity;

  ProductRequestsImpl productRequestsImpl;
  CartRequestImpl cartRequestImpl;
  List<String> errors = [];
  ScrollController _controller;

  void setChosenOptionId(String optionId, String productOptionValueId) {
    int _index = selectedOptions
        .indexWhere((element) => element.selectedProductOptionId == optionId);
    int _optionIndex = widget.options
        .indexWhere((element) => element.productOptionId == optionId);
    setState(() {
      if (widget.options[_optionIndex].type != 'checkbox') {
        selectedOptions[_index].selectedProductOptionIdValuesIds.clear();
        selectedOptions[_index]
            .selectedProductOptionIdValuesIds
            .add(productOptionValueId);
      } else {
        /// check if the value passed is already in the selectedOptions
        /// if it's already there we removed it if not we add it
        if (!selectedOptions[_index]
            .selectedProductOptionIdValuesIds
            .contains(productOptionValueId)) {
          selectedOptions[_index]
              .selectedProductOptionIdValuesIds
              .add(productOptionValueId);
        } else {
          selectedOptions[_index]
              .selectedProductOptionIdValuesIds
              .remove(productOptionValueId);
        }
      }
    });
  }

  bool checkIfCheckBoxSelected(String optionId, String productOptionValueId) {
    int _i = selectedOptions
        .indexWhere((element) => element.selectedProductOptionId == optionId);
    return selectedOptions[_i]
        .selectedProductOptionIdValuesIds
        .contains(productOptionValueId);
  }

  void validateOptionsSelected() {
    setState(() {
      errors.clear();
    });
    selectedOptions.forEach((selectedOption) {
      if (selectedOption.isRequired &&
          selectedOption.selectedProductOptionIdValuesIds.isEmpty) {
        int _i = widget.options.indexWhere((element) =>
            element.productOptionId == selectedOption.selectedProductOptionId);
        setState(() {
          errors.add(' Please Enter The ' + widget.options[_i].name);
        });
      }
    });
  }

  void submitSelected() async {
    validateOptionsSelected();
    if (errors.isEmpty) {
      /// call the Add to cart Endpoint
      List<IdValue> _idValues = selectedOptions
          .map(
            (option) => IdValue(
                key: option.selectedProductOptionId,
                value: option.selectedProductOptionIdValuesIds),
          )
          .toList();
      AddedToCartItem _cartItem = AddedToCartItem(
        option: _idValues,
        quantity: selectedQuantity.toString(),
        productId: widget.productId,
      );
      dynamic _response = await cartRequestImpl.addProductToCart(_cartItem);
      String _successMessage = _response['success']?.toString() ?? '';
      List<dynamic> _errorMessage =
          _response['error'] != null ? _response['error']['options'] : [];
      if (_successMessage.isNotEmpty && _errorMessage.isEmpty) {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => CheckOutPage(),
          ),
        );
      } else {
        setState(
          () {
            _errorMessage.forEach(
              (element) {
                errors.add(element);
              },
            );
          },
        );
      }
    }
    // TODO scroll to the end of the screen when errors is found Using [ScrollController]
    _controller.animateTo(
      _controller.offset + SizeConfig.screenHeight * 0.2,
      duration: Duration(milliseconds: 500),
      curve: Curves.bounceIn,
    );
  }

  void clearSelected() {
    setState(() {
      selectedQuantity = widget.minimum;
      selectedOptions.map((selectedOption) =>
          selectedOption.selectedProductOptionIdValuesIds.clear());
    });
  }

  void setQuantity(int selectedQuantity) {
    setState(() {
      this.selectedQuantity = selectedQuantity;
    });
  }

  @override
  void initState() {
    super.initState();
    productRequestsImpl = ProductRequestsImpl();
    cartRequestImpl = CartRequestImpl();
    selectedQuantity = widget.minimum;
    selectedOptions = <OptionSelectedValue>[];
    for (int i = 0; i < widget.options.length; i++) {
      selectedOptions.add(
        OptionSelectedValue(
          selectedProductOptionId: widget.options[i].productOptionId,
          isRequired: widget.options[i].requiredField,
          selectedProductOptionIdValuesIds: [],
        ),
      );
    }
    _controller = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Select The Options for the Product',
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: ListView(
          controller: _controller,
          shrinkWrap: true,
          children: [
            CounterButton(
              maximum: widget.maximumQuantity,
              minimum: widget.minimum,
              onQuantityUpdated: setQuantity,
            ),
            ...widget.options.map(
              (option) => ConditionalSwitch.single<String>(
                context: context,
                valueBuilder: (context) => option.type,
                caseBuilders: {
                  'radio': (context) => Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.025),
                        child: Container(
                          padding:
                              EdgeInsets.all(SizeConfig.screenWidth * 0.025),
                          decoration: BoxDecoration(
                            boxShadow: shadow,
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: Column(
                            children: [
                              RadioButtonOptionsList(
                                productOptionValues: option.productOptionValues,
                                optionName: option.name,
                                onOptionSelected: setChosenOptionId,
                                optionId: option.productOptionId,
                              ),
                            ],
                          ),
                        ),
                      ),
                  'checkbox': (context) => Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.02),
                        child: Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            boxShadow: shadow,
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: Column(
                            children: [
                              CheckBoxOptionsList(
                                productOptionValues: option.productOptionValues,
                                optionName: option.name,
                                onOptionSelected: setChosenOptionId,
                                isSelected: checkIfCheckBoxSelected,
                                optionId: option.productOptionId,
                              ),
                            ],
                          ),
                        ),
                      ),
                  'text': (context) => Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                        child: Container(
                          child: TextFieldOptionInput(
                            optionName: option.name,
                            onOptionSubmitted: setChosenOptionId,
                            optionId: option.productOptionId,
                            optionType: option.type,
                          ),
                        ),
                      ),
                  'select': (context) => Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                        child: Container(
                          padding:
                              EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                          decoration: BoxDecoration(
                            boxShadow: shadow,
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: DropdownOptionList(
                            productOptionValues: option.productOptionValues,
                            optionName: option.name,
                            onOptionSelected: setChosenOptionId,
                            optionId: option.productOptionId,
                          ),
                        ),
                      ),
                  'textarea': (context) => Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                        child: Container(
                          child: TextFieldOptionInput(
                            optionName: option.name,
                            onOptionSubmitted: setChosenOptionId,
                            optionId: option.productOptionId,
                            optionType: option.type,
                          ),
                        ),
                      ),
                  'date': (context) => Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                        child: Container(
                          padding:
                              EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                          decoration: BoxDecoration(
                            boxShadow: shadow,
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: DatePickerOption(
                            optionName: option.name,
                            onOptionSelected: setChosenOptionId,
                            optionId: option.productOptionId,
                          ),
                        ),
                      ),
                  'time': (context) => Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                        child: Container(
                          padding:
                              EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                          decoration: BoxDecoration(
                            boxShadow: shadow,
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: TimePickerOption(
                            optionName: option.name,
                            onOptionSelected: setChosenOptionId,
                            optionId: option.productOptionId,
                          ),
                        ),
                      ),
                  'datetime': (context) => Padding(
                        padding: EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                        child: Container(
                          padding:
                              EdgeInsets.all(SizeConfig.screenWidth * 0.03),
                          decoration: BoxDecoration(
                            boxShadow: shadow,
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: DateTimePickerOption(
                            optionName: option.name,
                            onOptionSelected: setChosenOptionId,
                            optionId: option.productOptionId,
                            isRequired: option.requiredField,
                          ),
                        ),
                      ),
                },
                fallbackBuilder: (context) =>
                    Text('No options for this product'),
              ),
            ),
            FormError(errors: errors),
            LayoutBuilder(
              builder: (context, constraints) => Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: constraints.maxWidth / 3,
                      child: Center(
                        child: DefaultButton(
                          text: 'Submit',
                          press: submitSelected,
                        ),
                      ),
                    ),

                    // TODO add the logic to the clear button
                    /* Container(
                      width: constraints.maxWidth / 3,
                      child: Center(
                        child: DefaultButton(
                          text: 'Clear',
                          press: clearSelected,
                        ),
                      ),
                    ), */
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
