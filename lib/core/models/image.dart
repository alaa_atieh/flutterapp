import 'package:meta/meta.dart';

class ProductImage {
  String thumb;
  String type;
  ProductImage({@required this.thumb, this.type});
  ProductImage.fromJson(Map<String, dynamic> json) {
    thumb = json['thumb'];
    type = json['type'];
  }
}
