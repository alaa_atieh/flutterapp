import 'dart:async';

import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/theme.dart';
import 'package:australia_garden/screens/category/sort_rules.dart';
import 'package:australia_garden/screens/product/domin/entities/related_product.dart';
import 'package:australia_garden/screens/search/data/datasources/search_requests.dart';
import 'package:australia_garden/screens/search/data/models/search_query.dart';
import 'package:australia_garden/screens/search/presentaion/pages/search_options.dart';
import 'package:australia_garden/screens/search/presentaion/pages/search_results.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

import '../../../product/domin/entities/related_product.dart';

class SearchBar extends StatefulWidget {
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  FloatingSearchBarController controller;
  final SearchRequests _searchRequests = locator<SearchRequests>();
  List<RelatedProduct> _searchResult;
  SearchQueryRequest searchQueryRequest = SearchQueryRequest(
    attributes: true,
    description: true,
  );
  bool searchInDescription, searchInAttributes, hasNewResult = true;
  int pageCount, pageElementLimit = 18;
  StreamController<List<RelatedProduct>> _searchStreamController;
  SortRules _sortRules;

  void changeSearchRequestOptions(SearchQueryRequest searchQueryRequest) {
    setState(
      () {
        this.searchQueryRequest = searchQueryRequest;
      },
    );
  }

  void _showSortOptions(BuildContext context) {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(34.0),
          topRight: Radius.circular(34.0),
        ),
      ),
      backgroundColor: Colors.white,
      builder: (context) {
        return SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(AppSizes.sidePadding),
                child: Container(
                  width: 60,
                  height: 6,
                  decoration: BoxDecoration(
                    color: AppColors.darkGray,
                    borderRadius: BorderRadius.circular(AppSizes.imageRadius),
                  ),
                ),
              ),
              Text(
                'Sort by',
                style: Theme.of(context).textTheme.headline4,
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: AppSizes.sidePadding,
                ),
              ),
              ..._sortRules.sortTextVariants
                  .map(
                    (key, value) => MapEntry(
                      key,
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: AppSizes.sidePadding,
                            vertical: AppSizes.linePadding),
                        alignment: Alignment.centerLeft,
                        color: _sortRules.sortType == key
                            ? AppColors.red
                            : AppColors.white,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: InkWell(
                                child: Text(
                                  value,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .copyWith(
                                          fontWeight: FontWeight.normal,
                                          color: _sortRules.sortType == key
                                              ? AppColors.white
                                              : AppColors.black),
                                ),
                                onTap: () {
                                  setState(
                                    () {
                                      _sortRules = SortRules(
                                        sortOrder: _sortRules.sortType == key
                                            ? (_sortRules.sortOrder ==
                                                    SortOrder
                                                        .FromLowestToHighest
                                                ? SortOrder.FromHighestToLowest
                                                : SortOrder.FromLowestToHighest)
                                            : _sortRules.sortOrder,
                                        sortType: key,
                                      );
                                      searchQueryRequest.order =
                                          _sortRules.orderToApi;
                                      searchQueryRequest.sort =
                                          _sortRules.typeToApi();
                                      //* Added this reset page number to get the results right
                                      // searchQueryRequest.page = 1;
                                    },
                                  );
                                  makeSearch(searchQueryRequest, true);
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            IconButton(
                              icon: Icon(
                                _sortRules.sortOrder ==
                                        SortOrder.FromHighestToLowest
                                    ? FontAwesomeIcons.sortAlphaUp
                                    : FontAwesomeIcons.sortAlphaDown,
                              ),
                              color: _sortRules.sortType == key
                                  ? Theme.of(context).primaryColor
                                  : Theme.of(context).backgroundColor,
                              onPressed: () {
                                _sortRules.copyWithChangedOrder();
                                Navigator.pop(context);
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                  .values
                  .toList(growable: false),
            ],
          ),
        );
      },
    );
  }

  Future<bool> makeSearch(
    SearchQueryRequest searchQueryRequest,
    bool isNewRequest,
  ) async {
    if (isNewRequest) {
      //* if the call is with new query or to reorder/sort the results
      _searchResult = <RelatedProduct>[];
      hasNewResult = true;
      searchQueryRequest.page = 1;
      searchQueryRequest.limit = pageElementLimit;
      final _temp = await _searchRequests.search(
        searchQueryRequest,
      );
      if (_temp == null) {
        return false;
      }
      setState(
        () {
          if (_temp.length < pageElementLimit) {
            hasNewResult = false;
          }
          this.searchQueryRequest = searchQueryRequest;
          _searchResult.addAll(_temp);
          _searchStreamController.add(_searchResult);
        },
      );
      return true;
    } else {
      //* if the call is to get the next page
      searchQueryRequest.page++;
      final _temp = await _searchRequests.search(
        searchQueryRequest,
      );
      // * If no results are found decrease the page number to
      if (_temp == null) {
        searchQueryRequest.page--;
        return false;
      }
      setState(
        () {
          if (_temp.length < pageElementLimit) {
            hasNewResult = false;
          }
          this.searchQueryRequest = searchQueryRequest;
          _searchResult.addAll(_temp);
          _searchStreamController.add(_searchResult);
        },
      );
      return true;
    }
  }

  @override
  void initState() {
    super.initState();
    controller = FloatingSearchBarController();
    _searchStreamController = new StreamController();
    _sortRules = SortRules();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildSearchBar(),
    );
  }

  Widget buildSearchBar() {
    return FloatingSearchBar(
      hint: 'Type Your Query Here',
      clearQueryOnClose: false,
      controller: controller,
      transition: CircularFloatingSearchBarTransition(),
      physics: BouncingScrollPhysics(),
      title: Text(
        searchQueryRequest.query == null || searchQueryRequest.query.isEmpty
            ? 'Search In Our Products '
            : searchQueryRequest.query,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      actions: [
        FloatingSearchBarAction.searchToClear(
          showIfClosed: true,
        ),
        searchQueryRequest.query?.isNotEmpty ?? false
            ? FloatingSearchBarAction.icon(
                icon: Icon(Icons.sort),
                onTap: () => _showSortOptions(context),
                showIfClosed: true,
              )
            : SizedBox(),
      ],
      body: searchQueryRequest.query?.isNotEmpty ?? false
          ? SearchResults(
              searchResult: _searchResult,
              reSearch: makeSearch,
              changeSearchRequestOptions: changeSearchRequestOptions,
              searchQueryRequest: searchQueryRequest,
              hasNewResult: hasNewResult,
              searchStreamController: _searchStreamController,
            )
          : SvgPicture.asset(
              'assets/start_search.svg',
              height: SizeConfig.screenHeight,
              width: SizeConfig.screenWidth,
            ),
      onSubmitted: (query) {
        if (query?.isNotEmpty ?? false) {
          searchQueryRequest.query = query;
          makeSearch(searchQueryRequest, true);
        }
        controller.close();
      },
      builder: (context, transition) => SearchOptions(
        searchQueryRequest: searchQueryRequest,
        changeSearchRequestOptions: changeSearchRequestOptions,
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    _searchStreamController.close();
    super.dispose();
  }
}
