import 'dart:core';

class Order {
  //int id;
  String id;
  String customerName;
  String status;
  String dateAdded;
  int productsCount;
  String totalPrice;

  Order(this.id, this.customerName, this.status, this.dateAdded,
      this.productsCount, this.totalPrice);
  Order.fromJson(Map<String, dynamic> json) {
    id = json['order_id'];
    customerName = json['customer'];
    status = json['status'];
    dateAdded = json['date_added'];
    productsCount = json['products'];
    totalPrice = json['total'];
  }
}
