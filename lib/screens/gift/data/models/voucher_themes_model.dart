import 'package:json_annotation/json_annotation.dart';
part 'voucher_themes_model.g.dart';

@JsonSerializable()
class VoucherThemesModel {
  @JsonKey(name: 'voucher_themes')
  List<VoucherThemeModel> voucherThemes;
  VoucherThemesModel({
    this.voucherThemes,
  });

  @override
  String toString() => 'VoucherThemesModel(voucherThemes: $voucherThemes)';

  factory VoucherThemesModel.fromJson(Map<String, dynamic> json) =>
      _$VoucherThemesModelFromJson(json);
  Map<String, dynamic> toJson() => _$VoucherThemesModelToJson(this);
}

@JsonSerializable()
class VoucherThemeModel {
  String name;
  @JsonKey(name: 'voucher_theme_id')
  String voucherThemeId;
  VoucherThemeModel({
    this.name,
    this.voucherThemeId,
  });

  @override
  String toString() =>
      'VoucherThemeModel(name: $name, voucherThemeId: $voucherThemeId)';

  factory VoucherThemeModel.fromJson(Map<String, dynamic> json) =>
      _$VoucherThemeModelFromJson(json);
  Map<String, dynamic> toJson() => _$VoucherThemeModelToJson(this);
}
