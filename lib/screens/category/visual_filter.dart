import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:australia_garden/core/theme.dart';
import 'subcategory.dart';

class VisualFilter extends StatelessWidget {
  final List<Subcategory> subcategories;
  final HashMap<String, bool> selecteHashTags;

  const VisualFilter({
    this.subcategories,
    this.selecteHashTags,
  });

  @override
  Widget build(BuildContext context) {
    if (subcategories == null) {
      return ListView.builder(
          scrollDirection: Axis.horizontal, itemBuilder: _blankChip);
    } else {
      List<Widget> widgetList = subcategories
              ?.map((optionHashTag) => Padding(
                    padding: EdgeInsets.only(right: AppSizes.sidePadding / 2),
                    child: ChoiceChip(
                      selected: selecteHashTags != null
                          ? selecteHashTags[optionHashTag] ?? false
                          : false,
                      padding: EdgeInsets.all(
                        AppSizes.linePadding,
                      ),
                      backgroundColor: Color(0xFF222222),
                      selectedColor: Color(0xFFDB3022),
                      label: Text(
                        "text",
                        style: Theme.of(context).textTheme.button.copyWith(
                            fontSize: 14,
                            color: AppColors.white,
                            fontFamily: 'Metropolis',
                            fontWeight: FontWeight.w500),
                      ),
                      /*   onSelected: (value) {
                onFilterChanged(optionHashTag, value);
              },*/
                    ),
                  ))
              ?.toList(growable: false) ??
          [];

      return ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
              SizedBox(
                width: 16,
              )
            ] +
            widgetList +
            [
              SizedBox(
                width: 16,
              )
            ],
      );
    }
  }

  Widget _blankChip(BuildContext context, _) {
    return Padding(
        padding: EdgeInsets.only(right: AppSizes.sidePadding / 2),
        child: Chip(
          padding: EdgeInsets.all(
            AppSizes.linePadding,
          ),
          backgroundColor: Color(0xFF222222),
          label: Text(
            '        ',
            style: Theme.of(context).textTheme.button.copyWith(),
          ),
        ));
  }
}

typedef FilterChanged = Function(String attribute, bool isSelected);
