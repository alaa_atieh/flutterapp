import 'package:flutter/material.dart';
import 'package:australia_garden/core/constants.dart';

class TextFieldDialog extends StatefulWidget {
  final String title;
  final Function(String) onTextFieldSubmitted;
  final GlobalKey<ScaffoldState> scaffoldKey;

  const TextFieldDialog({
    Key key,
    @required this.onTextFieldSubmitted,
    @required this.title,
    @required this.scaffoldKey,
  }) : super(key: key);
  @override
  _TextFieldDialogState createState() => _TextFieldDialogState();
}

class _TextFieldDialogState extends State<TextFieldDialog> {
  final _formKey = GlobalKey<FormState>();
  String _temp;

  Map<String, String> _result = {};
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.grey.shade50,
      child: Row(
        children: [
          Text(widget.title),
        ],
      ),
      onPressed: () => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(widget.title),
          content: Form(
            key: _formKey,
            child: TextFormField(
              autofocus: true,
              cursorColor: Theme.of(context).cursorColor,
              onChanged: (value) {
                setState(() {
                  _temp = value;
                  _result.clear();
                });
              },
              decoration: InputDecoration(errorMaxLines: 3),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Can\'t Submit an Empty Code';
                } else if (_result['isError'] == '1') {
                  return _result['result'];
                } else if (_result['isError'] == '0') {
                  var snackbar = new SnackBar(
                    content: new Text(
                      _result['result'],
                    ),
                  );
                  widget.scaffoldKey.currentState.showSnackBar(snackbar);
                  Navigator.pop(context);
                }
                return null;
              },
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.grey,
              textColor: Colors.white,
              child: Text('Cancel'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              color: kPrimaryColor,
              textColor: Colors.white,
              child: Text('Apply'),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  var s = await widget.onTextFieldSubmitted(_temp);
                  setState(
                    () {
                      _result = s;
                    },
                  );
                }
                _formKey.currentState.validate();
              },
            ),
          ],
        ),
      ),
    );
  }
}
