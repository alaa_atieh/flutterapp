import 'package:australia_garden/screens/product/presentation/details/details_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:australia_garden/screens/product/data/models/related_product_model.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/app_properties.dart';
import 'package:australia_garden/screens/product/data/datasources/product_requests.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ProductCard extends StatefulWidget {
  final double width, aspectRatio;
  final RelatedProductModel product;
  final String moduleTitle;

  const ProductCard(
      {Key key,
      this.width = 140,
      this.aspectRatio = 1.02,
      @required this.product,
      this.moduleTitle})
      : super(key: key);

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: getProportionateScreenWidth(20),
      ),
      child: SizedBox(
        width: getProportionateScreenWidth(widget.width),
        child: GestureDetector(
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => DetailsScreen(
                widget.product.productId,
                widget.moduleTitle,
              ),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.02,
                child: Container(
                  padding: EdgeInsets.all(
                    getProportionateScreenWidth(20),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: shadow,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Hero(
                    tag: widget.product.productId.toString() +
                        widget.moduleTitle,
                    child: Image.network(widget.product.thumb),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.only(left: SizeConfig.screenWidth * 0.02),
                child: FittedBox(
                  child: Text(
                    widget.product.name,
                    style: TextStyle(color: Colors.black),
                    maxLines: 2,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: SizeConfig.screenWidth * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      widget.product.price,
                      style: TextStyle(
                        fontSize: getProportionateScreenWidth(18),
                        fontWeight: FontWeight.w600,
                        color: kPrimaryColor,
                      ),
                    ),
                    InkWell(
                      borderRadius: BorderRadius.circular(50),
                      onTap: () async {
                        ProductRequestsImpl p = new ProductRequestsImpl();
                        if (!widget.product.isFavorite) {
                          bool result = await p.addProductToFavourites(
                              widget.product.productId.toString());
                          if (result)
                            setState(() {
                              widget.product.isFavorite = true;
                            });
                          else {
                            Fluttertoast.showToast(
                                msg: 'couldn\'t add product to favorites ',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                backgroundColor: Colors.black54,
                                textColor: Colors.white);
                          }
                        } else {
                          bool result = await p.removeProductFromFavourites(
                              widget.product.productId.toString());
                          if (result) {
                            setState(() {
                              widget.product.isFavorite = false;
                            });
                          } else {
                            Fluttertoast.showToast(
                                msg: 'couldn\'t remove product from favorites ',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                backgroundColor: Colors.black54,
                                textColor: Colors.white);
                          }
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.all(getProportionateScreenWidth(8)),
                        height: getProportionateScreenWidth(28),
                        width: getProportionateScreenWidth(28),
                        decoration: BoxDecoration(
                          color: widget.product.isFavorite
                              ? kPrimaryColor.withOpacity(0.15)
                              : kSecondaryColor.withOpacity(0.1),
                          shape: BoxShape.circle,
                        ),
                        child: SvgPicture.asset(
                          "assets/icons/Heart Icon_2.svg",
                          color: widget.product.isFavorite
                              ? Color(0xFFFF4848)
                              : Color(0xFFDBDEE4),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
