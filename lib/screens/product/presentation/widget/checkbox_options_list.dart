import 'package:australia_garden/screens/product/domin/entities/product_option_value.dart';
import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'package:flutter/material.dart';

class CheckBoxOptionsList extends StatefulWidget {
  /* List of Options for the product  */
  final List<ProductOptionValue> productOptionValues;
  /* The name of the option */
  final String optionName;
  /* A function that will be executed whenever one of the check boxes is selected */
  final Callback onOptionSelected;
  /* A function that will check if the checkbox is selected or not */
  final Function(String, String) isSelected;
  /* The ID for the product option  */
  final String optionId;

  CheckBoxOptionsList({
    @required this.productOptionValues,
    @required this.optionName,
    @required this.onOptionSelected,
    @required this.isSelected,
    @required this.optionId,
  });

  @override
  _CheckBoxOptionsListState createState() => _CheckBoxOptionsListState();
}

class _CheckBoxOptionsListState extends State<CheckBoxOptionsList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(widget.optionName),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.productOptionValues.length,
            itemBuilder: (context, index) => CheckboxListTile(
              title: Text(widget.productOptionValues[index].name),
              secondary: widget.productOptionValues[index].image == null
                  ? null
                  : Image.network(
                      widget.productOptionValues[index].image,
                      fit: BoxFit.cover,
                    ),
              subtitle: Text(widget.productOptionValues[index].price == null
                  ? ''
                  : widget.productOptionValues[index].price),
              contentPadding: EdgeInsets.all(0),
              value: widget.isSelected(
                widget.optionId,
                widget.productOptionValues[index].productOptionValueId,
              ),
              onChanged: (checked) {
                setState(
                  () {
                    widget.onOptionSelected(widget.optionId,
                        widget.productOptionValues[index].productOptionValueId);
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
