import 'package:json_annotation/json_annotation.dart';

part 'cart_item.g.dart';

@JsonSerializable(explicitToJson: true)
class CartItem {
  @JsonKey(name: 'cart_id')
  String cartId;
  @JsonKey(name: 'product_id')
  int productId;
  String name;
  String model;
  List<CartItemOption> option;
  String quantity;
  bool stock;
  String shipping;
  String price;
  String total;
  int reward;
  String thumb;
  int minimum;
  int maximum;

  CartItem({
    this.cartId,
    this.productId,
    this.name,
    this.model,
    this.option,
    this.quantity,
    this.stock,
    this.shipping,
    this.price,
    this.total,
    this.reward,
    this.thumb,
    this.minimum,
    this.maximum,
  });

  factory CartItem.fromJson(Map<String, dynamic> json) =>
      _$CartItemFromJson(json);
  Map<String, dynamic> toJson() => _$CartItemToJson(this);

  @override
  String toString() {
    return 'CartItem(cartId: $cartId, productId: $productId, name: $name, model: $model, option: $option, quantity: $quantity, stock: $stock, shipping: $shipping, price: $price, total: $total, reward: $reward, thumb: $thumb, minimum: $minimum)';
  }
}

@JsonSerializable(explicitToJson: true)
class CartItemOption {
  String name;
  String value;

  CartItemOption({this.name, this.value});

  factory CartItemOption.fromJson(Map<String, dynamic> json) =>
      _$CartItemOptionFromJson(json);
  Map<String, dynamic> toJson() => _$CartItemOptionToJson(this);

  @override
  String toString() => 'CartItemOption(name: $name, value: $value)';
}
