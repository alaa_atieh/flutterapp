import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:australia_garden/core/constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TimePickerOption extends StatefulWidget {
  /* The name of the option */
  final String optionName;
  /* A function that will be executed whenever one of the check boxes is selected */
  final Callback onOptionSelected;
  /* The ID for the product option  */
  final String optionId;

  TimePickerOption({
    @required this.optionName,
    @required this.onOptionSelected,
    @required this.optionId,
  });

  @override
  _TimePickerOptionState createState() => _TimePickerOptionState();
}

class _TimePickerOptionState extends State<TimePickerOption> {
  TimeOfDay _selectedTime = TimeOfDay(hour: 0, minute: 0);
  Duration initialTimer = null;

  void _startTimePicker() {
    !isIOS
        ? showTimePicker(
            context: context,
            initialTime: _selectedTime,
            initialEntryMode: TimePickerEntryMode.dial,
          ).then((pickedTime) {
            if (pickedTime == null) return;
            setState(() {
              _selectedTime = pickedTime;
              widget.onOptionSelected(
                widget.optionId,
                pickedTime.format(context),
              );
            });
          })
        : buildCupertinoDatePicker(context);
  }

  buildCupertinoDatePicker(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white,
            child: CupertinoTimerPicker(

              mode: CupertinoTimerPickerMode.hm,
              minuteInterval: 1,
              secondInterval: 1,
              initialTimerDuration: Duration.zero,
              onTimerDurationChanged: (Duration changedtimer) {
                setState(() {
                  initialTimer = changedtimer;
                });
                widget.onOptionSelected(
                  widget.optionId,
                  initialTimer.inHours.toString() +
                      ':' +
                      (initialTimer.inMinutes % 60).toString(),
                );
              },
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(widget.optionName),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: isIOS
                    ? Text(
                        initialTimer == null
                            ? 'Select The ${widget.optionName}'
                            : initialTimer.inHours.toString() +
                                ':' +
                                (initialTimer.inMinutes % 60).toString(),
                      )
                    : Text(
                        _selectedTime == null
                            ? 'Select The ${widget.optionName}'
                            : _selectedTime.format(context),
                      ),
              ),
              IconButton(
                icon: Icon(
                  FontAwesomeIcons.clock,
                  color: kPrimaryColor,
                  size: 16,
                ),
                onPressed: _startTimePicker,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
