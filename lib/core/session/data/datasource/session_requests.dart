import 'dart:convert';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/session/data/models/refresh_token_request_body.dart';
import 'package:australia_garden/core/session/data/models/refresh_token_response.dart';
import 'package:australia_garden/core/shared_preference/shared_preferences_manager.dart';
import 'package:http/http.dart' as http;

abstract class SessionRequest {
  Future<void> refreshClientToken(bool isLoginRequest);
}

class SessionRequestImpl implements SessionRequest {
  final SharedPreferencesManager _sharedPreferencesManager =
      locator<SharedPreferencesManager>();
  @override
  Future<void> refreshClientToken(bool isLoginRequest) async {
    String _username = 'Default';
    String _key =
        'EaydwDjgTry3WDdWF8Ngtzw0TAzQiSQ0UiBq0cjS5ZuzyDbHZsOGcG0o7FEaHRNf2EfesG6iRHZvO7sGsccNH7onMVgoqN5BbPSZXM4QaJe9YNfTaF17Lm3RhSTHVtUxZnQdSrtZobmCgIGfwXLmr8XFoGSR2gOp1O9fq3Ey32KNmFQHqwvmCn4uVjdygMtEE2U1zPb5RgPri77dXOy9H4YC5gGbwBXp2pXdvOnEf1T7FbQKtSkEaMqZ6sSM4JCz';
    String _customerId = _sharedPreferencesManager.getString('customer_id');
    RefreshTokenRequestBody _refreshTokenRequestBody = isLoginRequest
        ? RefreshTokenRequestBody(username: _username, key: _key)
        : RefreshTokenRequestBody(
            username: _username, key: _key, customerId: _customerId);

    try {
      final response = await http
          .post(
            Uri.decodeFull(
              Uri.https(
                authority,
                basicRoute,
                {
                  'route': 'apiCustomize/login',
                  'lang': 'en-gb',
                },
              ).toString(),
            ),
            body: _refreshTokenRequestBody.toJson(),
          )
          .timeout(
            Duration(seconds: 10),
          );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        Map data = json.decode(response.body);
        await _sharedPreferencesManager.putString(
          'api_token',
          RefreshTokenResponseModel.fromJson(data).apiToken,
        );
        // return baseCall();
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to Refresh Token');
      }
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }
}
