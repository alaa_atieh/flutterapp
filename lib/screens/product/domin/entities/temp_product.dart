import 'package:meta/meta.dart';

import 'package:australia_garden/core/models/image.dart';
import 'package:australia_garden/screens/product/domin/entities/discount.dart';
import 'package:australia_garden/screens/product/domin/entities/product_attribute.dart';
import 'package:australia_garden/screens/product/domin/entities/product_option.dart';
import 'package:australia_garden/screens/product/domin/entities/related_product.dart';

class Product {
  String productName;
  int productId;
  String manufacturer;
  String model;
  int reward; /* the amount of points rewarded when buying this product  */
  int points; /* the amount of points needed to buy this product with point  */
  String description;
  String stock;
  String thumb;
  List<ProductImage> images;
  String price;
  String special;
  int productPercent;
  String tax;
  List<Discount> discounts;
  List<ProductOption> options;
  String minimum;
  String reviewStatus;
  bool
      reviewGuest; /* holds if the user is logged in to determine if he/she could leave a review*/
  String
      customerName; /* holds the customer name, to show it when he/she writes a review */
  String reviews;
  int rating;
  List<AttributeGroup> attributeGroups; /* hold a list of the product specs */
  List<RelatedProduct> relatedProducts;
  List tags;
  bool isFavorite;
  bool isInCompareList;
  String tapReview;
  int quantity;

  Product(
      {@required this.productName,
      @required this.productId,
      @required this.manufacturer,
      @required this.model,
      @required this.reward,
      @required this.points,
      @required this.description,
      @required this.stock,
      @required this.thumb,
      @required this.images,
      @required this.price,
      @required this.special,
      @required this.productPercent,
      @required this.tax,
      @required this.discounts,
      @required this.options,
      @required this.minimum,
      @required this.reviewStatus,
      @required this.reviewGuest,
      @required this.customerName,
      @required this.reviews,
      @required this.rating,
      @required this.attributeGroups,
      @required this.relatedProducts,
      @required this.tags,
      @required this.isFavorite,
      @required this.isInCompareList,
      @required this.tapReview,
      @required this.quantity});

  @override
  String toString() {
    return 'Product(productName: $productName, productId: $productId, manufacturer: $manufacturer, model: $model, reward: $reward, points: $points, description: $description, stock: $stock, thumb: $thumb, images: $images, price: $price, special: $special, productPercent: $productPercent, tax: $tax, discounts: $discounts, options: $options, minimum: $minimum, reviewStatus: $reviewStatus, reviewGuest: $reviewGuest, customerName: $customerName, reviews: $reviews, rating: $rating, attributeGroups: $attributeGroups, relatedProducts: $relatedProducts, tags: $tags, isFavorite: $isFavorite, isInCompareList: $isInCompareList, tapReview: $tapReview, quantity: $quantity)';
  }
}
