import 'package:australia_garden/screens/product/domin/entities/product_option_value.dart';
import 'package:meta/meta.dart';

class ProductOptionValueModel extends ProductOptionValue {
  @required
  String productOptionValueId;
  @required
  String optionValueId;
  @required
  String name;
  @required
  String image;
  @required
  String price;
  @required
  String pricePrefix;

  ProductOptionValueModel(
      {this.productOptionValueId,
      this.optionValueId,
      this.name,
      this.image,
      this.price,
      this.pricePrefix});

  factory ProductOptionValueModel.fromJson(var json) {
    return ProductOptionValueModel(
        productOptionValueId: json['product_option_value_id'],
        optionValueId: json['option_value_id'],
        name: json['name'],
        image: json['image'],
        price: json['price'],
        pricePrefix: json['price_prefix']);
  }
}
