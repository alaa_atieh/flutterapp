// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voucher_gift_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VoucherGiftModel _$VoucherGiftModelFromJson(Map<String, dynamic> json) {
  return VoucherGiftModel(
    toName: json['to_name'] as String,
    toEmail: json['to_email'] as String,
    fromName: json['from_name'] as String,
    fromEmail: json['from_email'] as String,
    voucherThemeId: json['voucher_theme_id'] as String,
    message: json['message'] as String,
    amount: json['amount'] as String,
  );
}

Map<String, dynamic> _$VoucherGiftModelToJson(VoucherGiftModel instance) =>
    <String, dynamic>{
      'to_name': instance.toName,
      'to_email': instance.toEmail,
      'from_name': instance.fromName,
      'from_email': instance.fromEmail,
      'voucher_theme_id': instance.voucherThemeId,
      'message': instance.message,
      'amount': instance.amount,
    };
