class Quote {
  String code;
  String title;
  String cost;
  String taxClassId;
  String text;

  Quote({this.code, this.title, this.cost, this.taxClassId, this.text});
  Quote.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    title = json['title'];
    cost = json['cost'];
    taxClassId = json['tax_class_id'];
    text = json['text'];
  }
}
