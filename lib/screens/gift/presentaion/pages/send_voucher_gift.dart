import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/dependency_injection/injector.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/screens/gift/data/datasources/gift_requests.dart';
import 'package:australia_garden/screens/gift/data/models/voucher_gift_model.dart';
import 'package:australia_garden/screens/gift/data/models/voucher_themes_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SendVoucherGift extends StatefulWidget {
  @override
  _SendVoucherGiftState createState() => _SendVoucherGiftState();
}

class _SendVoucherGiftState extends State<SendVoucherGift> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController _senderNameController = TextEditingController();
  TextEditingController _senderEmailController = TextEditingController();
  TextEditingController _receiverNameController = TextEditingController();
  TextEditingController _receiverEmailController = TextEditingController();
  TextEditingController _messageController = TextEditingController();
  TextEditingController _amountController = TextEditingController();

  List<DropdownMenuItem<String>> _themes;
  String _voucherThemeSelected;

  void _resetControllers() {
    setState(
      () {
        _senderNameController.clear();
        _senderEmailController.clear();
        _receiverNameController.clear();
        _receiverEmailController.clear();
        _messageController.clear();
        _amountController.clear();
        _voucherThemeSelected = null;
      },
    );
  }

  DropdownMenuItem<String> _getDropdownThemeItem(
      VoucherThemeModel voucherThemeModel) {
    return DropdownMenuItem<String>(
      child: Text(voucherThemeModel.name),
      value: voucherThemeModel.voucherThemeId,
    );
  }

  Future<Map<String, String>> _addVoucherToCart() async {
    return await locator.get<GiftRequest>().addVoucherToCart(
          VoucherGiftModel(
            toName: _receiverNameController.text,
            toEmail: _receiverEmailController.text,
            fromName: _senderNameController.text,
            fromEmail: _senderEmailController.text,
            message: _messageController.text,
            amount: _amountController.text,
            voucherThemeId: _voucherThemeSelected,
          ),
        );
  }

  @override
  void initState() {
    super.initState();
    locator.get<GiftRequest>().getVoucherThemes().then(
          (VoucherThemesModel voucherThemesModel) => setState(
            () {
              _themes = voucherThemesModel != null
                  ? voucherThemesModel.voucherThemes
                      .map(
                        (e) => _getDropdownThemeItem(e),
                      )
                      .toList()
                  : [];
            },
          ),
        );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Send a Voucher Gift'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: LayoutBuilder(
          builder: (context, constraints) => Container(
            child: Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.person_outline,
                              color: kPrimaryColor,
                            ),
                            labelText: 'Receiver\'s Name ',
                          ),
                          keyboardType: TextInputType.name,
                          controller: _receiverNameController,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Can\'t Leave Name Empty';
                            }
                            return null;
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Receiver\'s Email',
                            prefixIcon: Icon(
                              Icons.mail_outline_rounded,
                              color: kPrimaryColor,
                            ),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          controller: _receiverEmailController,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (!RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value)) {
                              return 'Please Enter a Valid Email';
                            }
                            return null;
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Sender\'s Name',
                            prefixIcon: Icon(
                              Icons.person_outline,
                              color: kPrimaryColor,
                            ),
                          ),
                          keyboardType: TextInputType.name,
                          controller: _senderNameController,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Can\'t Leave Name Empty';
                            }
                            return null;
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Sender\'s Email',
                            prefixIcon: Icon(
                              Icons.mail_outline_rounded,
                              color: kPrimaryColor,
                            ),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          controller: _senderEmailController,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (!RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value)) {
                              return 'Please Enter a Valid Email';
                            }
                            return null;
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Amount',
                            prefixIcon: Icon(
                              Icons.attach_money,
                              color: kPrimaryColor,
                            ),
                          ),
                          keyboardType: TextInputType.numberWithOptions(
                            decimal: false,
                          ),
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          controller: _amountController,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Can\'t Leave The Amount Empty';
                            } else {
                              if (double.parse(value) < 1.0 ||
                                  double.parse(value) > 1000.0) {
                                return 'Amount must be between \$1.00 and \$1,000.00';
                              }
                            }
                            return null;
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: DropdownButtonFormField(
                          hint: Text('Choose Theme'),
                          value: _voucherThemeSelected,
                          items: _themes,
                          onChanged: (value) {
                            setState(
                              () {
                                _voucherThemeSelected = value;
                              },
                            );
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          decoration: InputDecoration(
                            hintText: '(optional)',
                            labelText: 'Message',
                            prefixIcon: Icon(
                              Icons.message_outlined,
                              color: kPrimaryColor,
                            ),
                          ),
                          controller: _messageController,
                          textInputAction: TextInputAction.done,
                          // onFieldSubmitted: (value) => node.unfocus(),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 2,
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  color: kPrimaryColor,
                                  child: Text(
                                    'Clear',
                                    style: TextStyle(
                                      fontSize: getProportionateScreenWidth(18),
                                      color: Colors.white,
                                    ),
                                  ),
                                  onPressed: _resetControllers,
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Expanded(
                                flex: 2,
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  color: kPrimaryColor,
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _addVoucherToCart().then((value) {
                                        if (value['isError'] == '1') {
                                          var snackbar = new SnackBar(
                                            content: new Text(
                                              value['result'],
                                            ),
                                          );
                                          _scaffoldKey.currentState
                                              .showSnackBar(snackbar);
                                        } else if (value['isError'] == '0') {
                                          var snackbar = new SnackBar(
                                            content: new Text(
                                              value['result'],
                                            ),
                                          );
                                          _scaffoldKey.currentState
                                              .showSnackBar(snackbar);
                                        }
                                      });
                                    }
                                  },
                                  child: Text(
                                    'Send',
                                    style: TextStyle(
                                      fontSize: getProportionateScreenWidth(18),
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ), // we will work in here
                ],
              ),
            ),
          ),
        ),
      ), // We'll add this in a bit
    );
  }
}
