import 'package:meta/meta.dart';

class AttributeGroup {
  String attributeGroupId;
  String attributeGroupName;
  List<Attribute> attributes;

  AttributeGroup({
    @required this.attributeGroupId,
    @required this.attributeGroupName,
    @required this.attributes,
  });
}

class Attribute {
  String attributeId;
  String attributeName;
  String attributeText;

  Attribute({
    @required this.attributeId,
    @required this.attributeName,
    @required this.attributeText,
  });
}
