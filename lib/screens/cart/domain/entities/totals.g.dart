// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'totals.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Totals _$TotalsFromJson(Map<String, dynamic> json) {
  return Totals(
    title: json['title'] as String,
    text: json['text'] as String,
  );
}

Map<String, dynamic> _$TotalsToJson(Totals instance) => <String, dynamic>{
      'title': instance.title,
      'text': instance.text,
    };
