import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../core/components/default_button.dart';
import '../../core/constants.dart';
import '../../core/size_config.dart';
import 'address.dart';
import 'address_api.dart';
import 'country.dart';
import 'zone.dart';

class EditAddressScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  Function() onAddAddress;

  Address address;

  EditAddressScreen(this.scaffoldKey, this.onAddAddress, this.address);

  @override
  _EditAddressScreenState createState() => _EditAddressScreenState();
}

class _EditAddressScreenState extends State<EditAddressScreen> {
  _EditAddressScreenState();

  final _formKey = GlobalKey<FormState>();

  TextEditingController firstName = TextEditingController(text: '');

  TextEditingController lastName = TextEditingController(text: '');

  TextEditingController city = TextEditingController(text: '');

  TextEditingController address1 = TextEditingController(text: '');

  TextEditingController address2 = TextEditingController(text: '');

  TextEditingController postalCode = TextEditingController(text: '');

  String firstNameValidator = '';

  String lastNameValidator = '';

  String cityValidator = '';

  String postalCodeValidator = '';

  List<Country> countries = <Country>[];

  List<Zone> cities = <Zone>[];

  List<String> test = <String>[];

  String chosenCountry;

  String chosenCity;

  bool postal = false, isDefault;
  AddressApi a = new AddressApi();

  @override
  void initState() {
    super.initState();
    getCountries(a);
    firstName.text = widget.address.firstName;
    firstName.text = widget.address.firstName;
    lastName.text = widget.address.lastName;
    city.text = widget.address.city;
    chosenCountry = widget.address.countryId;
    chosenCity = widget.address.zoneId;
    address1.text = widget.address.address1;
    address2.text = widget.address.address2;
    isDefault = widget.address.isDefault;
    getCities(a);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit address"),
      ),
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: SizeConfig.screenHeight * 0.04),
                  Text(
                    "Edit your address",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: getProportionateScreenWidth(28),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: SizeConfig.screenHeight * 0.08),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Input(
                          controller: firstName,
                          hintText: "First Name",
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }

                            if (firstNameValidator != '') {
                              return firstNameValidator;
                            }
                            return null;
                          },
                          secure: false,
                          icon: Icon(FontAwesomeIcons.user,
                              color: kPrimaryColor,
                              size: SizeConfig.screenHeight * 0.025),
                        ),
                        SizedBox(height: getProportionateScreenHeight(30)),
                        Input(
                          controller: lastName,
                          hintText: "Last Name",
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }

                            if (lastNameValidator != '') {
                              return lastNameValidator;
                            }
                            return null;
                          },
                          secure: false,
                          icon: Icon(
                            FontAwesomeIcons.user,
                            color: kPrimaryColor,
                            size: SizeConfig.screenHeight * 0.025,
                          ),
                        ),
                        SizedBox(height: getProportionateScreenHeight(30)),
                        Input(
                          controller: city,
                          hintText: "City",
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }

                            if (cityValidator != '') {
                              return cityValidator;
                            }
                            return null;
                          },
                          secure: false,
                          icon: Icon(
                            FontAwesomeIcons.city,
                            color: kPrimaryColor,
                            size: SizeConfig.screenHeight * 0.025,
                          ),
                        ),
                        SizedBox(height: getProportionateScreenHeight(30)),
                        Input(
                          controller: address1,
                          hintText: "Address 1",
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }

                            return null;
                          },
                          secure: false,
                          icon: Icon(
                            FontAwesomeIcons.mapMarkerAlt,
                            color: kPrimaryColor,
                            size: SizeConfig.screenHeight * 0.025,
                          ),
                        ),
                        SizedBox(height: getProportionateScreenHeight(30)),
                        Input(
                          controller: address2,
                          hintText: "Address 2",
                          secure: false,
                          icon: Icon(
                            FontAwesomeIcons.mapMarkerAlt,
                            color: kPrimaryColor,
                            size: SizeConfig.screenHeight * 0.025,
                          ),
                        ),
                        SizedBox(height: getProportionateScreenHeight(30)),
                        DropdownButtonFormField(
                            validator: (value) {
                              if (value == null) {
                                return 'Please enter some text';
                              }

                              return null;
                            },
                            isExpanded: true,
                            value: chosenCountry,
                            onChanged: (value) async {
                              chosenCity = null;
                              chosenCountry = value;
                              var tempCities =
                                  await a.getZonesByCountryId(chosenCountry);
                              setState(() {
                                cities = tempCities;
                              });
                            },
                            disabledHint: Text("Country"),
                            hint: Text('Choose country'),
                            items: countries.map((val) {
                              return new DropdownMenuItem(
                                child: new Text(val.name),
                                value: val.id,
                                onTap: () {
                                  setState(() {
                                    postal = val.postalCode;
                                  });
                                },
                              );
                            }).toList()),
                        SizedBox(height: getProportionateScreenHeight(20)),
                        DropdownButtonFormField(
                          validator: (value) {
                            if (value == null) {
                              return 'Please enter some text';
                            }

                            return null;
                          },
                          isExpanded: true,
                          value: chosenCity,
                          onChanged: (value) {
                            setState(() {
                              chosenCity = value;
                            });
                          },
                          disabledHint: Text("City"),
                          hint: Text('Choose city'),
                          items: cities.map((val) {
                            return new DropdownMenuItem(
                              child: new Text(val.name),
                              value: val.id,
                              // value: val.toString(),
                            );
                          }).toList(),
                        ),
                        SizedBox(height: getProportionateScreenHeight(20)),
                        CheckboxListTile(
                          activeColor: kPrimaryColor,
                          dense: true,
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.screenWidth * 0.1,
                          ),
                          secondary: Text(
                            'Make it the Default Address',
                            style: TextStyle(
                                fontSize: SizeConfig.screenWidth * 0.04,
                                color: Colors.black),
                          ),
                          checkColor: Colors.black,
                          value: isDefault,
                          onChanged: (value) => setState(
                            () {
                              isDefault = value;
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  postal
                      ? Input(
                          controller: postalCode,
                          hintText: "Postal Code",
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }

                            if (postalCodeValidator != '') {
                              return postalCodeValidator;
                            }
                            return null;
                          },
                          secure: false,
                          icon: Icon(
                            FontAwesomeIcons.code,
                            color: kPrimaryColor,
                            size: 20,
                          ),
                        )
                      : SizedBox(),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  Padding(
                    padding:
                        EdgeInsets.only(bottom: SizeConfig.screenHeight * 0.02),
                    child: Row(
                      children: [
                        Expanded(
                          child: DefaultButton(
                            text: "Cancel",
                            press: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        SizedBox(
                          width: SizeConfig.screenHeight * 0.02,
                        ),
                        Expanded(
                          child: DefaultButton(
                            text: "Edit address",
                            press: () async {
                              if (_formKey.currentState.validate()) {
                                var result = await a.updateAddress(
                                  widget.address.id,
                                  firstName.text,
                                  lastName.text,
                                  city.text,
                                  address1.text,
                                  address2.text,
                                  "180",
                                  "2846",
                                  postalCode.text,
                                  isDefault,
                                );
                                if (result['error'] != null) {
                                  this.firstNameValidator =
                                      result['error']['firstname'];
                                  this.lastNameValidator =
                                      result['error']['lastname'];
                                  this.cityValidator = result['error']['city'];
                                }
                                final formv = _formKey.currentState;
                                if (formv.validate()) {
                                  widget.scaffoldKey.currentState.showSnackBar(
                                    new SnackBar(
                                      content: Container(
                                          height: 30,
                                          child: new Text(
                                            "Address edited successfully",
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300),
                                          )),
                                    ),
                                  );
                                  widget.onAddAddress();
                                  Navigator.of(context).pop();
                                } else {
                                  this.firstNameValidator = "";
                                  this.lastNameValidator = "";
                                  this.cityValidator = "";
                                }
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  getCountries(AddressApi a) async {
    List<Country> c;
    c = await a.getCountries();
    setState(() {
      countries = c;
    });
  }

  getCities(AddressApi a) async {
    List<Zone> c;
    c = await a.getZonesByCountryId(chosenCountry);
    setState(() {
      cities = c;
    });
  }
}

class Input extends StatefulWidget {
  Input(
      {Key key,
      @required this.controller,
      @required this.hintText,
      this.validator,
      this.secure,
      this.icon,
      this.keyboardType})
      : super(key: key);

  TextEditingController controller;
  final String hintText;
  Function validator;

  final bool secure;
  final Icon icon;
  TextInputType keyboardType;

  @override
  _InputState createState() => _InputState();
}

class _InputState extends State<Input> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        labelText: widget.hintText,
        // hintText: widget.hintText,
        errorMaxLines: 2, prefixIcon: widget.icon,
      ),
      controller: widget.controller,
      obscureText: widget.secure,
      style: TextStyle(fontSize: 16.0),
      validator: widget.validator,
      keyboardType: widget.keyboardType,
    );
  }
}
