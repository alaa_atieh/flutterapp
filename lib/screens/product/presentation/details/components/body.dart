import 'package:australia_garden/core/components/default_button.dart';
import 'package:australia_garden/screens/product/domin/entities/temp_product.dart';
import 'package:australia_garden/screens/product/presentation/pages/options_builder.dart';
import 'package:flutter/material.dart';
import 'package:australia_garden/core/size_config.dart';
import 'product_description.dart';
import 'top_rounded_container.dart';
import 'product_images.dart';

class Body extends StatelessWidget {
  final Product product;
  final String moduleTitle;

  const Body({Key key, @required this.product, this.moduleTitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              left: SizeConfig.screenWidth * 0.06,
              right: SizeConfig.screenWidth * 0.06,
            ),
            child: ProductImages(
              product: product,
              moduleTitle: moduleTitle,
            ),
          ),
          TopRoundedContainer(
            color: Colors.grey.shade50,
            borderColor: Colors.grey.shade50,
            child: Column(
              children: [
                ProductDescription(
                  product: product,
                ),
                Container(
                  color: Colors.grey.shade50,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: SizeConfig.screenWidth * 0.15,
                      right: SizeConfig.screenWidth * 0.15,
                      bottom: getProportionateScreenWidth(40),
                      top: getProportionateScreenWidth(15),
                    ),
                    child: DefaultButton(
                      text: "Add To Cart",
                      press: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => OptionsBuilder(
                            productId: product.productId.toString(),
                            options: product.options,
                            minimum: int.parse(product.minimum),
                            maximumQuantity: product.quantity,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
