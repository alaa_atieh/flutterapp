import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:australia_garden/core/constants.dart';
import 'package:australia_garden/core/enums.dart';
import 'package:australia_garden/core/size_config.dart';
import 'package:australia_garden/core/widgets/RetryButton.dart';
import 'package:australia_garden/screens/home/components/custom_bottom_nav_bar.dart';
import 'package:australia_garden/screens/product/data/models/related_product_model.dart';

import '../../../../core/theme.dart';
import '../../../../core/widgets/press_twice_to_exit.dart';
import '../../../category/product_view.dart';
import '../../../category/sort_rules.dart';
import '../../../product/data/datasources/product_requests.dart';

class FavouriteListView extends StatefulWidget {
  bool list;
  SortRules sortRules;

  FavouriteListView(
    this.list,
  );

  @override
  _ProductsListViewState createState() => _ProductsListViewState();
}

class _ProductsListViewState extends State<FavouriteListView> {
  @override
  Widget build(BuildContext context) {
    ProductRequestsImpl p = new ProductRequestsImpl();
    return PressTwiceToExit(
      child: Scaffold(
        bottomNavigationBar:
            CustomBottomNavBar(selectedMenu: MenuState.favourite),
        body: Container(
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/backgroundimage.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: FutureBuilder(
            future: p.getFavourites(),
            builder: (
              BuildContext context,
              AsyncSnapshot<List<RelatedProductModel>> snapshot,
            ) {
              if (snapshot.connectionState != ConnectionState.done) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (snapshot.hasError) {
                return RetryButton(
                  action: () => setState(() {}),
                );
              }
              if (snapshot.hasData) {
                return snapshot.data.isNotEmpty
                    ? CustomScrollView(
                        slivers: <Widget>[
                          SliverAppBar(
                            expandedHeight: 100,
                            actions: <Widget>[],
                            floating: true,
                            primary: true,
                            snap: false,
                            pinned: false,
                            flexibleSpace: FlexibleSpaceBar(
                              background: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                        color: kPrimaryColor,
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(20),
                                            bottomRight: Radius.circular(20))),
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        right: SizeConfig.screenHeight * 0.03,
                                        top: SizeConfig.screenHeight * 0.009,
                                        bottom: SizeConfig.screenHeight * 0.01,
                                        left: SizeConfig.screenHeight * 0.01,
                                      ),
                                      child: Text(
                                        'Favourites',
                                        style: TextStyle(
                                          fontSize: 22,
                                          color: AppColors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'ALCHEVROLA',
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: SizeConfig.screenHeight * 0.02,
                                  )
                                ],
                              ),
                            ),
                          ),
                          getListViewFav(
                            context,
                            snapshot.data,
                            () {
                              setState(() {});
                            },
                          )
                        ],
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Container(
                              child: Image.asset(
                                'assets/no_favourite_yet.png',
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ],
                      );
              }
              return Padding(
                padding: const EdgeInsets.only(top: kToolbarHeight),
                child: RetryButton(
                  action: () => setState(
                    () {},
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
