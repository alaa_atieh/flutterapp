import 'package:australia_garden/screens/product/domin/entities/product_option_value.dart';
import 'package:australia_garden/screens/product/domin/entities/product_option.dart';
import 'package:meta/meta.dart';

import 'Product_option_value_model.dart';

class ProductOptionModel extends ProductOption {
  ProductOptionModel(
      {@required String productOptionId,
      @required List<ProductOptionValue> productOptionValues,
      @required String optionId,
      @required String name,
      @required String type,
      @required String value,
      @required bool requiredField})
      : super(
            productOptionId: productOptionId,
            productOptionValues: productOptionValues,
            optionId: optionId,
            name: name,
            type: type,
            value: value,
            requiredField: requiredField);

  factory ProductOptionModel.fromJson(var json) {
    var productOptionValuesList = json['product_option_value'] as List;
    return ProductOptionModel(
        productOptionId: json['product_option_id'],
        productOptionValues: productOptionValuesList
            .map((e) => ProductOptionValueModel.fromJson(e))
            .toList(),
        optionId: json['option_id'],
        name: json['name'],
        type: json['type'],
        value: json['value'],
        requiredField: json['required']);
  }
}
