class OptionSelectedValue {
  String selectedProductOptionId;
  bool isRequired;
  List<String> selectedProductOptionIdValuesIds;

  OptionSelectedValue({
    this.selectedProductOptionId,
    this.isRequired,
    this.selectedProductOptionIdValuesIds,
  });

  @override
  String toString() {
    return this.selectedProductOptionId +
        ' | ' +
        this.isRequired.toString() +
        ' | ' +
        this.selectedProductOptionIdValuesIds.toString() +
        '\n';
  }
}